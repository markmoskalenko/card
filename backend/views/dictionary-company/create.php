<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\DictionaryCompany */

$this->title = Yii::t('app', 'Создать {modelClass}', [
    'modelClass' => 'словарь компаний',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Словарь компаний'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dictionary-company-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
