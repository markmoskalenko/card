<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DictionaryCompany */

$this->title = Yii::t('app', 'Обновить {modelClass}: ', [
    'modelClass' => 'словарь компаний',
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Словарь компаний'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Обновление');
?>
<div class="dictionary-company-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
