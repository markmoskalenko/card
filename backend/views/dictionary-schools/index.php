<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\DictionarySchoolsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Словарь образовавтельных учереждений');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dictionary-schools-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Создать', [
    'modelClass' => 'Dictionary Schools',
]), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'name',
            [
                'attribute' => 'is_public',
                'value'     => function ($value) {
                        return $value->is_public ? 'Да' : 'Нет';
                    }
            ],
            [
                'attribute' => 'language_id',
                'value'     => function ($value) {
                        return $value->language->name;
                    }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}{delete}'
            ],
        ],
    ]); ?>

</div>
