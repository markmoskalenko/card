<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\DictionarySchools */

$this->title = Yii::t('app', 'Создать {modelClass}', [
    'modelClass' => 'словарь образовавтельных учереждений',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Словарь образовавтельных учереждений'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dictionary-schools-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
