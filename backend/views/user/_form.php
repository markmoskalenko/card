<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'surname')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 's_status_title')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'role')->dropDownList(\common\models\User::getRoles()) ?>

    <?= $form->field($model, 'status')->dropDownList(\common\models\User::getStatus()) ?>

    <?= $form->field($model, 'balance')->textInput(['maxlength' => 10]) ?>

    <?= $form->field($model, 'subdomain')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => 50]) ?>

    <?= $form->field($model, 'phone_mob')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'skype')->textInput(['maxlength' => 50]) ?>

    <p>Социальные контакты</p>
    <hr/>
    <?= $form->field($model, 's_vk')->textInput(['maxlength' => 50]) ?>

    <?= $form->field($model, 's_gplus')->textInput(['maxlength' => 50]) ?>


    <?= $form->field($model, 's_fb')->textInput(['maxlength' => 50]) ?>

    <?= $form->field($model, 's_tw')->textInput(['maxlength' => 50]) ?>

    <?= $form->field($model, 's_mail')->textInput(['maxlength' => 50]) ?>

    <?= $form->field($model, 's_git')->textInput(['maxlength' => 50]) ?>

    <?= $form->field($model, 'flickr')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'linkedin')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'my_site')->textInput(['maxlength' => 255]) ?>


    <div class="form-group">
        <?= Html::submitButton('Сохраниь', ['class' =>  'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
