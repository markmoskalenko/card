<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пользователи';
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p style="color: green;">
        <?= Yii::$app->session->getFlash('messageSent'); ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'username',
            'email:email',
            'created_at',
            'role',
            [
                'attribute'=>'subdomain',
                'format'=>'html',
                'value'=>function($data){
                        if( empty( $data->subdomain ) ) return null;
                        $link = Yii::$app->params['scheme'].$data->subdomain.'.'.Yii::$app->params['baseDomain'];
                        return Html::a( $link, $link, ['target'=>'_blank'] );
                    }
            ],
            'balance',
            [
                'class' => 'yii\grid\ActionColumn',
                'buttons'=>[
                    'message'=>function ($url, $model) {
                            return Html::a( 'Написать', ['message/send', 'id' => $model->id] );

                        }
                ],
                'template'=>'{message} {update} {delete}',
            ],
        ],
    ]); ?>
</div>