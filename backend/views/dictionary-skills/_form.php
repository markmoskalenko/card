<?php

use common\models\DictionaryLanguage;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\DictionarySkills */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dictionary-skills-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'is_public')->checkbox() ?>

    <?= $form->field($model, 'language_id')->dropDownList(DictionaryLanguage::getTreeMap(), ['prompt'=> \Yii::t('app','Выберите язык')]) ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Создать') : Yii::t('app', 'Обновить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
