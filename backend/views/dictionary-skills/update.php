<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DictionarySkills */

$this->title = Yii::t('app', 'Обновить словарь навыков:', [
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Словарь навыков'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Обновление');
?>
<div class="dictionary-skills-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
