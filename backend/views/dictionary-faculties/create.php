<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\DictionaryFaculties */

$this->title = Yii::t('app', 'Создать {modelClass}', [
    'modelClass' => 'словарь факультетов',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Словарь факультетов'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dictionary-faculties-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
