<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DictionaryFaculties */

$this->title = Yii::t('app', 'Обновить {modelClass} ', [
    'modelClass' => 'Словарь факультетов',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Словарь факультетов'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Обновить');
?>
<div class="dictionary-faculties-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
