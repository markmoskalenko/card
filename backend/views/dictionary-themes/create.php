<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Themes */

$this->title = Yii::t('app', 'Создать {modelClass}', [
    'modelClass' => 'тему',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Темы'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="themes-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
