<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\DictionaryThemesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Темы');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="themes-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a(Yii::t('app', 'Создать {modelClass}', [
            'modelClass' => 'тему',
        ]), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'title',
            ],
            [
                'attribute' => 'system_name',
                'value'     => function ($value) {
                        return $value->system_name;
                    }
            ],
            [
                'attribute' => 'img',
                'format' => 'html',
                'value'     => function ($value){
                        if($value->img==null){
                            return Html::img('http://placehold.it/200x200');
                        }
                        return Html::img('/uploads/theme/200x200/'.$value->img, ['width'=>'200', 'height'=>'200' ]);
                    }
            ],
            [
                'attribute' => 'cost',
                'value'     => function ($value) {
                        return $value->cost;
                    }
            ],
            [
                'attribute' => 'is_published',
                'value'     => function ($value) {
                        return $value->is_published ? 'Да' : 'Нет';
                    }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}{delete}'
            ],
        ],
    ]); ?>

</div>
