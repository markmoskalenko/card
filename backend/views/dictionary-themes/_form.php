<?php

use kartik\widgets\ActiveForm;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Themes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="themes-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]);?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>

    <?php if($model->img==NULL){
        echo Html::img('http://placehold.it/200x200');
    }else{
        echo Html::img('/uploads/theme/200x200/'.$model->img);
    }?>

    <?= $form->field($model, 'img')->fileInput()->label('')?>

    <?= $form->field($model, 'cost')->textInput(['maxlength' => 10]) ?>

    <?= $form->field($model, 'system_name')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'is_published')->checkbox() ?>

    <?= $form->field($model, 'sort')->textInput() ?>

<!--    --><?//= $form->field($model, 'created_at')->textInput() ?>
<!---->
<!--    --><?//= $form->field($model, 'updated_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Создать') : Yii::t('app', 'Обновить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
