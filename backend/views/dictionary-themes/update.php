<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Themes */

$this->title = Yii::t('app', 'Обновить {modelClass}: ', [
    'modelClass' => 'темы',
]) . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Темы'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Обновить');
?>
<div class="themes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
