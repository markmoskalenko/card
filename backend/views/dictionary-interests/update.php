<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DictionaryInterests */

$this->title = Yii::t('app', 'Обнивить {modelClass}: ', [
    'modelClass' => 'словарь интересов',
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Словарь интересов'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Обновление');
?>
<div class="dictionary-interests-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
