<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\DictionaryInterests */

$this->title = Yii::t('app', 'Создать {modelClass}', [
    'modelClass' => 'словарь интересов',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Словарь интересов'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dictionary-interests-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
