<?php
namespace backend\controllers;
use backend\components\Controller;
use common\models\Invoice;
use common\models\InvoiceSearch;
use yii\web\BadRequestHttpException;
use Yii;

class PaymentController extends Controller
{
    /**
     * Lists all Invoice models.
     * @return mixed
     */
    public function actionList()
    {
        $model = new InvoiceSearch();
        $dataProvider = $model->search(Yii::$app->request->queryParams);

        return $this->render('list', [
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param integer $id
     * @return Invoice
     * @throws \yii\web\BadRequestHttpException
     */
    protected function loadModel($id) {
        $model = Invoice::find($id);
        if ($model === null) {
            throw new BadRequestHttpException;
        }
        return $model;
    }
}