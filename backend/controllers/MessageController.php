<?php
namespace backend\controllers;
use backend\components\Controller;
use common\models\Message;
use common\models\User;
use yii\web\BadRequestHttpException;
use Yii;

class MessageController extends Controller
{
    /**
     * Lists all Invoice models.
     * @return mixed
     */
    public function actionSend($id)
    {
        if ($id){
            $model = new Message();
            $model->date = date('Y-m-d H:i:s');
            $model->is_read = 0;
            $model->category = Message::CATEGORY_MYRES;
            $model->to_user_id = $id;
            $model->email = User::u()->email;
            $model->name = User::u()->username;

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                User::u()->sendCommingMessageEmail($model);
                Yii::$app->getSession()->setFlash('messageSent', \Yii::t('app','Ваше сообщение отправлено.'));
                return $this->redirect(['user/index']);
            } else {
                return $this->render('send', [
                    'model' => $model,
                ]);
            }
        } else {
            return $this->redirect(['user/index']);
        }
    }

    /**
     * @param integer $id
     * @return Invoice
     * @throws \yii\web\BadRequestHttpException
     */
    protected function loadModel($id) {
        $model = Invoice::find($id);
        if ($model === null) {
            throw new BadRequestHttpException;
        }
        return $model;
    }
}