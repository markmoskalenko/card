<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use Yii;

/**
 * DictionarySchoolsSearch represents the model behind the search form about `common\models\DictionarySchools`.
 */
class DictionarySchoolsSearch extends DictionarySchools
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'is_public', 'language_id'], 'integer'],
            [['name'], 'safe'],
            ['name', 'filter', 'filter' => function ($value) {
                    return \common\filters\HtmlPurifier::escape( $value );
                }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DictionarySchools::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'is_public' => $this->is_public,
            'language_id' => $this->language_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
