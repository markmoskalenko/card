<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "tbl_city".
 *
 * @property string $city_id
 * @property string $country_id
 * @property string $region_id
 * @property string $name
 *
 * @property Country $country
 * @property Region $region
 */
class City extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_city';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'region_id'], 'integer'],
            [['name'], 'string', 'max' => 128],
            ['name', 'filter', 'filter' => function ($value) {
                    return \common\filters\HtmlPurifier::escape( $value );
                }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'city_id' => 'City ID',
            'country_id' => 'Country ID',
            'region_id' => 'Region ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['country_id' => 'country_id']);
    }

    /**
     * Возвращает все города принадлежащие ID страны.
     *
     * @param $id - Страны
     *
     * @return array
     */

    public static function getMapByCountryId( $id ){
        $oModel = self::find()->where( ['country_id'=> $id] )->asArray()->all();
        return ArrayHelper::map( $oModel, 'city_id', 'name' );
    }


    /**
     * Возвращает массив key=>value
     * Нужнен для dropDown силектов
     *
     * @return array
     */
    public static function getMap(){
        $oModel = self::find()->asArray()->all();
        return ArrayHelper::map( $oModel, 'city_id', 'name' );
    }

//    /**
//     * @return \yii\db\ActiveQuery
//     */
//    public function getRegion()
//    {
//        return $this->hasOne(Region::className(), ['region_id' => 'region_id']);
//    }
}
