<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_pages_translate".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $content
 * @property string $meta_k
 * @property string $meta_d
 * @property integer $object_id
 * @property integer $language_id
 *
 * @property Pages $page
 */
class PagesTranslate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_pages_translate';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'object_id', 'language_id'], 'required'],
            [['description', 'content', 'meta_d'], 'string'],
            [['object_id', 'language_id'], 'integer'],
            [['title', 'meta_k'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'description' => 'Описание',
            'content' => 'Контент',
            'meta_k' => 'МЕТА Ключевые слова',
            'meta_d' => 'МЕТА Описание',
            'object_id' => 'Page ID',
            'language_id' => 'Language ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(Pages::className(), ['id' => 'object_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(DictionaryLanguage::className(), ['id' => 'language_id']);
    }
}
