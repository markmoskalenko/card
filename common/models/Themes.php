<?php

namespace common\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "tbl_themes".
 *
 * @property string $id
 * @property string $title
 * @property string $img
 * @property string $cost
 * @property string $system_name
 * @property integer $is_published
 * @property integer $sort
 * @property string $created_at
 * @property string $updated_at
 * @property UserTheme[] $userThemes
 */
class Themes extends \yii\db\ActiveRecord
{
    public $image;

    public $title;

    public $translateModelName = '\common\models\ThemesTranslate';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_themes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'system_name'], 'required'],
            [['cost'], 'number'],
            [['is_published', 'sort'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['title', 'img', 'system_name'], 'string', 'max' => 255],

            ['img','filter', 'filter' => function ($value) {
                    return \common\filters\HtmlPurifier::escape( $value );
                }],
            ['system_name','filter', 'filter' => function ($value) {
                    return \common\filters\HtmlPurifier::escape( $value );
                }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Название'),
            'img' => Yii::t('app', 'Изображение'),
            'cost' => Yii::t('app', 'Цена'),
            'system_name' => Yii::t('app', 'Системное имя'),
            'is_published' => Yii::t('app', 'Опубликовано'),
            'sort' => Yii::t('app', 'Сортировка'),
            'created_at' => Yii::t('app', 'Создана в'),
            'updated_at' => Yii::t('app', 'Обновлена в'),
        ];
    }

    public function behaviors()
    {
        return [
            'translate' => [
                'class' => \common\components\behaviors\TranslateBehavior::className(),
                'translateAttributes'=>array(
                    'title',
                ),
            ]
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserThemes()
    {
        return $this->hasOne(UserTheme::className(), ['theme_id' => 'id']);
    }



    public function getTranslate()
    {
        return $this->hasOne(ThemesTranslate::className(), ['object_id' => 'id'])
            ->andOnCondition(['language_id'=>DictionaryLanguage::getCurrent()->id]);
    }



    /**
     * Список языков для селекта
     * @return array
     */
    public static function getAllTemplates()
    {
        $oTemplate  = self::find()->all();

        return $oTemplate;
    }

    public function saveImage($model){

        $sUploadDirectory = Yii::getAlias('@backend/web/uploads/theme/original');
        $sSmallImageDirectory = Yii::getAlias('@backend/web/uploads/theme/200x200');
        $image = UploadedFile::getInstance( $model, 'img' );

        if(!$image==NULL){
            $aSourcePath = pathinfo($image);

            $model->img = md5(microtime().rand(1,10)) .'.'. $aSourcePath['extension'];
            $image->saveAs($sUploadDirectory.DIRECTORY_SEPARATOR.$model->img);

            $picture = new  \frontend\components\SimpleImage();
            $picture->load($sUploadDirectory.DIRECTORY_SEPARATOR.$model->img);
            $picture->resize(401, 188);

            $picture->save($sSmallImageDirectory.DIRECTORY_SEPARATOR.$model->img);
            return true;
        }
     return true;
    }

    /**
     * Покупка шаблона и снятие денег
     * @return bool
     */
    public function buy(){

        if( UserTheme::find()->where(['user_id'=>User::u()->id, 'theme_id'=>$this->id])->exists()){

            Yii::$app->session->setFlash('success', Yii::t('app', 'Тема уже куплена!'));

        }else{


            if($isBuy = Invoice::addInvoice($this->cost,'Покупка темы', Invoice::STATUS_SUCCESS, Invoice::TYPE_DECREMENT)){

                Yii::$app->session->setFlash('success', Yii::t('app', 'Денежные средства списаны!'));

                if( UserTheme::saveBuyTemplate($this->id) ){
                    Yii::$app->session->setFlash('success', Yii::t('app', 'Тема успешно куплена!'));
                }else{
                    Yii::$app->session->setFlash('error', Yii::t('app', 'Ошибка сохранения темы. Обратитесь к админестрации сайта.'));
                }

            }else{
                Yii::$app->session->setFlash('error', Yii::t('app', 'У вас недостаточно денежных средств. Пополните баланс.'));
            }

        }

        return;
    }


    public function add(){
        if( UserTheme::saveAddTemplate($this->id) ){
            Yii::$app->session->setFlash('success', Yii::t('app', 'Тема успешно добавлена!'));
        }else{
            Yii::$app->session->setFlash('error', Yii::t('app', 'Ошибка сохранения темы. Обратитесь к админестрации сайта.'));
        }
    }



}
