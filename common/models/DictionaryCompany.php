<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_dictionary_company".
 *
 * @property string $id
 * @property string $name
 * @property integer $is_public
 * @property string $language_id
 *
 * @property DictionaryLanguage $language
 * @property Experience[] $experiences
 */
class DictionaryCompany extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_dictionary_company';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'language_id'], 'required'],
            [['is_public', 'language_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'string', 'min' => 2],
            ['name', 'filter', 'filter' => function ($value) {
                    return \common\filters\HtmlPurifier::escape( $value );
                }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Значение'),
            'is_public' => Yii::t('app', 'Опубликовано'),
            'language_id' => Yii::t('app', 'Язык'),
        ];
    }

    public static function getIdByName($name, $create = true){

        $oCompany = parent::find()->andWhere(['name'=>$name])->one();

        if( !$oCompany && $create ){
            $oCompany = new self;

            $oCompany->name = $name;
            $oCompany->language_id = DictionaryLanguage::getCurrent()->id;
            $oCompany->is_public = 0;
            $oCompany->save();
        }

        return $oCompany->id;
    }


    public static function getByAutocomlete(){

        $query = DictionaryCompany::find();

        $query->andFilterWhere([
            'is_public' => 1,
            'language_id' => DictionaryLanguage::getCurrent()->id,
        ]);

        $r = $query->asArray()->all();

        $data = [];
        foreach( $r as $i ){
            $data[] = $i['name'];
        }
        return $data;

    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(DictionaryLanguage::className(), ['id' => 'language_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExperiences()
    {
        return $this->hasMany(Experience::className(), ['company_id' => 'id']);
    }
}
