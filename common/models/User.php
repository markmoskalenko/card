<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\web\IdentityInterface;
use yii\web\UploadedFile;

/**
 * User model
 *
 * @property integer $id
 * @property integer $balance
 * @property string $username
 * @property string $name
 * @property string $surname
 * @property string $date_of_birth_day
 * @property string $date_of_birth_month
 * @property string $date_of_birth_year
 * @property string $s_status_title
 * @property string $activate_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $counrty_id
 * @property integer $role
 * @property integer $status
 * @property integer $expire_date
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface
{
    public $password;
    public $passwordRepeat;
    public $validateScenario;
    public $image;
    public $oldPassword;

    // 10 дней
    const TEST_PERIOD = 864000;

    // Срок действие плана UnixTime
    const PAY_PERIOD_1 = 31536000;

    // Стоимость первого периода
    const PAY_PERIOD_1_PRICE = 100;

    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;
    const STATUS_TEST = 20;
    const STATUS_NOT_PAID = 30;

    const ROLE_USER = 'user';
    const ROLE_ADMINISTRATOR = 'administrator';

    public static function getRoles(){
        return [
            self::ROLE_USER => 'Пользователь',
            self::ROLE_ADMINISTRATOR => 'Администратор',
        ];
    }

    public static function getStatus(){
        return [
            self::STATUS_DELETED    => \Yii::t('app','Блокировать'),
            self::STATUS_ACTIVE     => \Yii::t('app','Активировать'),
            self::STATUS_TEST       => \Yii::t('app','Тестовый период'),
            self::STATUS_NOT_PAID   =>\Yii::t('app', 'Не оплачен'),
        ];
    }
    /**
     * @var array EAuth attributes
     */
    public $profile;

    public $x;
    public $y;
    public $width;
    public $height;
    public $filename;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_TEST],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED, self::STATUS_TEST, self::STATUS_NOT_PAID]],

            ['role', 'default', 'value' => self::ROLE_USER, 'on'=>['signup','signupEAuth']],
            ['role', 'in', 'range' => [self::ROLE_USER], 'on'=>['signup','signupEAuth']],

            ['role', 'default', 'value' => self::ROLE_USER, 'on'=>['administrator']],
            ['role', 'in', 'range' => [self::ROLE_USER, self::ROLE_ADMINISTRATOR], 'on'=>['administrator']],

            ['username', 'required'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['username', 'filter', 'filter' => function ($value) {
                return \common\filters\HtmlPurifier::escape( $value );
            }],

            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique', 'on'=>['signup', 'setEmail', 'profile']],
            ['email', 'filter', 'filter' => function ($value) {
                return \common\filters\HtmlPurifier::escape( $value );
            }],

            ['subdomain', 'required'],
            ['subdomain', 'match', 'pattern' => '/^[A-Za-z0-9\-]+$/u', 'message' => \Yii::t('app','Поле может содержать только тире, латинские буквы и цифры')],
            ['subdomain', 'unique'],
            ['subdomain', 'string', 'min' => 2, 'max' => 255],
            ['subdomain', 'filter', 'filter' => function ($value) {
                return \common\filters\HtmlPurifier::escape( $value );
            }],

            ['surname', 'required'],
            ['surname', 'string', 'min' => 2, 'max' => 255],
            ['surname', 'filter', 'filter' => function ($value) {
                return \common\filters\HtmlPurifier::escape( $value );
            }],

            ['date_of_birth_day', 'required'],
            ['date_of_birth_day', 'integer', 'min' => 1, 'max' => 31],
            ['date_of_birth_day', 'filter', 'filter' => 'trim'],

            ['date_of_birth_month', 'required'],
            ['date_of_birth_month', 'integer', 'min' => 1, 'max' => 12],
            ['date_of_birth_month', 'filter', 'filter' => 'trim'],

            ['date_of_birth_year', 'required'],
            ['date_of_birth_year', 'integer', 'min' => 1, 'max' => 2020],
            ['date_of_birth_year', 'filter', 'filter' => 'trim'],

            ['s_status_title', 'string', 'min' => 2, 'max' => 500],
            ['s_status_title', 'filter', 'filter' => function ($value) {
                return nl2br( \common\filters\HtmlPurifier::escape( $value, 'br') );
            }],

            ['s_fb', 'url'],
            ['s_fb', 'string', 'min' => 2, 'max' => 500],
            ['s_fb', 'filter', 'filter' => function ($value) {
                return \common\filters\HtmlPurifier::escape( $value );
            }],

            ['s_tw', 'url'],
            ['s_tw', 'string', 'min' => 2, 'max' => 500],
            ['s_tw', 'filter', 'filter' => function ($value) {
                return \common\filters\HtmlPurifier::escape( $value );
            }],

            ['flickr', 'url'],
            ['flickr', 'string', 'min' => 2, 'max' => 500],
            ['flickr', 'filter', 'filter' => function ($value) {
                return \common\filters\HtmlPurifier::escape( $value );
            }],

            ['s_gplus', 'url'],
            ['s_gplus', 'string', 'min' => 2, 'max' => 500],
            ['s_gplus', 'filter', 'filter' => function ($value) {
                return \common\filters\HtmlPurifier::escape( $value );
            }],

            ['s_vk', 'url'],
            ['s_vk', 'string', 'min' => 2, 'max' => 500],
            ['s_vk','filter', 'filter' => function ($value) {
                return \common\filters\HtmlPurifier::escape( $value );
            }],

            ['s_git', 'url'],
            ['s_git', 'string', 'min' => 2, 'max' => 500],
            ['s_git', 'filter', 'filter' => function ($value) {
                return \common\filters\HtmlPurifier::escape( $value );
            }],

            ['linkedin', 'url'],
            ['linkedin', 'string', 'min' => 2, 'max' => 500],
            ['linkedin', 'filter', 'filter' => function ($value) {
                return \common\filters\HtmlPurifier::escape( $value );
            }],

            ['s_mail', 'string', 'min' => 2, 'max' => 500],
            ['s_mail', 'filter', 'filter' => function ($value) {
                return \common\filters\HtmlPurifier::escape( $value );
            }],

            ['counrty_id', 'integer'],
            ['counrty_id', 'filter', 'filter' => 'trim'],

            ['city_id', 'integer'],
            ['city_id', 'filter', 'filter' => 'trim'],

            ['phone', 'string'],
            ['phone', 'filter', 'filter' => function ($value) {
                return \common\filters\HtmlPurifier::escape( $value );
            }],

            ['phone_mob', 'string'],
            ['phone_mob', 'filter', 'filter' => function ($value) {
                return \common\filters\HtmlPurifier::escape( $value );
            }],

            ['my_site', 'string'],
            ['my_site', 'filter', 'filter' => function ($value) {
                return \common\filters\HtmlPurifier::escape( $value );
            }],

            ['profession', 'string'],
            ['profession', 'filter', 'filter' => function ($value) {
                return \common\filters\HtmlPurifier::escape( $value );
            }],

            ['validateScenario', 'string'],

            ['activate_key', 'string'],
            ['activate_key', 'filter', 'filter' => function ($value) {
                return \common\filters\HtmlPurifier::escape( $value );
            }],

            ['image', 'safe'],

            ['balance', 'number'],

            ['expire_date', 'integer'],

            ['password', 'required', 'message' => \Yii::t('app', 'Поле пароль обязательное для заполнения.')],
            ['password', 'string', 'min' => 6, 'message' => \Yii::t('app', 'Пароль должен содержать больше 5 символов.')],
            ['passwordRepeat', 'compare', 'compareAttribute'=>'password', 'message'=>\Yii::t('app','Пароли не совпадают' )],
            [['oldPassword', 'password', 'passwordRepeat'], 'required', 'on'=>['changePassword']]
        ];
    }

    /**
     * Сценарии
     * @return array
     */
    public function scenarios()
    {
        return [
            'administrator'=>['role', 'username', 'email','s_mail', 'linkedin', 's_git', 's_gplus', 's_vk', 'flickr', 's_tw','s_fb', 'subdomain', 'surname', 'my_site', 'phone_mob', 'phone', 'balance', 'skype' ],
            'signup'                    => ['status', 'role', 'username', 'email', 'password', 'passwordRepeat'],
            'signupEAuth'               => ['status', 'role', 'username'],
            'setEmail'                  => ['email', 'password'],
            'activate'                  => ['activate_key', 'status'],
            'resetPassword'             => ['password'],
            'requestPasswordResetToken' => ['email', 'password_reset_token'],
            'changePassword'            => ['oldPassword', 'password', 'passwordRepeat'],

            'contact' => ['country_id', 'city_id', 'my_site', 'phone_mob', 'phone' ],

            'social' => ['s_mail', 'linkedin', 's_git', 's_gplus', 's_vk', 'flickr', 's_tw','s_fb' ],

            'profile' =>['username', 'profession', 'email', 'subdomain', 'surname', 'date_of_birth_day',
                'date_of_birth_month', 'date_of_birth_year', 's_status_title'],

            'upload' =>['image', 'avatar'],
            'period' =>['expire_date', 'status'],

            'buy' => ['balance'],



        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'subdomain'=> \Yii::t('app','Адрес сайта'),
            'username' => \Yii::t('app','Имя'),
            'email'    => 'E-Mail',
            'password' => \Yii::t('app','Пароль'),
            'name' => \Yii::t('app','Имя'),
            'surname' => \Yii::t('app','Фамилия'),
            'date_of_birth_day' => \Yii::t('app','День'),
            'date_of_birth_month' => \Yii::t('app','Месяц'),
            'date_of_birth_year' =>\Yii::t('app','Год') ,
            's_status_title' => \Yii::t('app','Моё кредо'),
            's_fb' => \Yii::t('app','Facebook'),
            's_tw' => \Yii::t('app','Twitter'),
            's_git' => \Yii::t('app','GitHub'),
            'linkedin' => \Yii::t('app','LinkedIn'),
            's_vk' => \Yii::t('app','Вконтакте'),
            's_mail' => \Yii::t('app','МойКруг@mail.ru'),
            's_gplus' => \Yii::t('app','Google+'),
            'flickr' => \Yii::t('app','Flickr'),
            'country_id' => \Yii::t('app','Страна'),
            'city_id' => \Yii::t('app','Город'),
            'phone' => \Yii::t('app','Телефон'),
            'phone_mob' => \Yii::t('app','Мобильный'),
            'my_site' => \Yii::t('app','Мой сайт (при наличии)'),
            'balance' => \Yii::t('app','Баланс'),
            'created_at' => \Yii::t('app','Дата регистрации'),
            'role' => \Yii::t('app','Роль'),
            'validateScenario' => '',
            'passwordRepeat' => \Yii::t('app', 'Введите пароль еще раз'),
            'oldPassword' => \Yii::t('app', 'Введите старый пароль'),
            'profession' => \Yii::t('app', 'Профессия'),
        ];
    }



    /**
     * Регистрация
     * @return User|null
     */
    public function signup()
    {
        /** @var User $user */
        $this->generateAuthKey();
        $this->validate();
        $this->generateExpireDate();
        $this->generateActivateKey();

        if ($this->save()) {
            \Yii::$app->authManager->assign(\Yii::$app->authManager->getRole(self::ROLE_USER), $this->id);
            return $this;
        } else {
            return false;
        }
    }

    /**
     * @param \nodge\eauth\ServiceBase $service
     * @return User
     * @throws ErrorException
     */
    public static  function signupByOAuth( $service ){
        /** @var User $user */
        $user = new User;
        $user->setScenario('signupEAuth');
        $user->username = $service->getAttribute('name');
        $user->auth_key = md5($service->getServiceName().'-'.$service->getId());

        $user->generateExpireDate();
        $user->generateActivateKey();
        $user->save();

        if ($user->save()) {
            \Yii::$app->authManager->assign(\Yii::$app->authManager->getRole(self::ROLE_USER), $user->id);
            return $user;
        } else {
            return null;
        }
    }

    public function generateExpireDate(){
        $this->expire_date = time()+self::TEST_PERIOD;
    }

    /**
     * @param $key
     *
     * @return array|null|ActiveRecord
     */
    public static function findByAuthKey($key){
        return static::find()->where(['auth_key'=>$key])->one();
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    /**
     * @param \nodge\eauth\ServiceBase $service
     *
     * @return User
     * @throws \ErrorException
     */
    public static function findIdentityByEAuth( $service) {

        if (!$service->getIsAuthenticated()) {
            throw new \ErrorException('EAuth user should be authenticated before creating identity.');
        }

        $key = md5($service->getServiceName().'-'.$service->getId());

        $identity = static::findByAuthKey( $key );


        if( !$identity ){

            $identity = static::signupByOAuth($service);
        }

        return $identity;
    }

    /**
     * Finds user by username
     *
     * @param  string      $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by username
     *
     * @param  string      $username
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email, 'status' => self::STATUS_ACTIVE]);
    }


    /**
     * Finds user by password reset token
     *
     * @param  string      $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        $expire = \Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        if ($timestamp + $expire < time()) {
            // token expired
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return \Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = \Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = md5(time());
    }

    public function generateActivateKey()
    {
        $this->activate_key = md5(time());
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token =  md5(time());
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * Активация учетки
     *
     * @param $token
     *
     * @return int
     */
    public static function activate($token){

        $oUser = User::find()->andWhere(['activate_key' => $token])->one();

        if( $oUser !== null ){

            $oUser->setScenario('activate');

            $oUser->activate_key = '';
            $oUser->status = 10;

            return $oUser->save();
        }

        return false;
    }

    /**
     * @return null|IdentityInterface|User
     */
    public static function u(){
        return \Yii::$app->user->identity;
    }

    public function sendActivateEmail()
    {
        return \Yii::$app->mail->compose('passwordActivateToken', ['user' => User::u()])
            ->setFrom('no-reply@myres.pro')
            ->setTo(User::u()->email)
            ->setSubject('Активация учетной записи. ' . \Yii::$app->name)
            ->send();
    }

    public function sendResetPasswordRequestEmail()
    {
        /** @var User $user */

        $user = User::find()->andWhere([
            'status' => User::STATUS_ACTIVE,
            'email' => $this->email,
        ])->one();

        if ($user) {

            $user->setScenario('requestPasswordResetToken');

            $user->generatePasswordResetToken();

            if ($user->save()) {
                return \Yii::$app->mail->compose('passwordResetToken', ['user' => $user])
                    ->setFrom('no-reply@myres.pro')
                    ->setTo($this->email)
                    ->setSubject('Сброс пароля. ' . \Yii::$app->name)
                    ->send();
            }
        }

        return false;
    }

    public function beforeSave($insert){
        if(parent::beforeSave($insert)){
            if( \Yii::$app->user->can(User::ROLE_ADMINISTRATOR)){
                \Yii::$app->authManager->revokeAll($this->id);

                \Yii::$app->authManager->assign(\Yii::$app->authManager->getRole($this->role), $this->id);
            }

            if(!empty($this->password)){
                $this->setPassword($this->password);
            }
            return true;
        }else{
            return false;
        }
    }

    /**
     * Сохраняет изображения на сервер.
     * На данном этаме еще нету привязки к моделе, просто сохранение на серваке.
     * Привязка будет произведена при сохранении товара.
     *
     * @return bool - результат сохранения изображения
     */
    public function saveImg(){

        $sUploadDirectory = Yii::getAlias('@frontend/web/upload/original');

        $sSmallImageDirectory = Yii::getAlias('@frontend/web/upload/150x150');

        $this->image = UploadedFile::getInstanceByName( 'Filedata[0]' );

        $fileInfo = getimagesize($this->image->tempName);

        if( !$fileInfo || !in_array(strtolower($fileInfo['mime']), ['image/png', 'image/jpeg', 'image/gif'])){
            return [
                'name'=>'',
                'status'=>'error',
                'message'=>'Ошибка! Допустимые расширения картинок - *.png, *.gif, *.jpg'
            ];
        }

        $aSourcePath = pathinfo($this->image->name);

        $this->avatar = md5(microtime().rand(1,10)) .'.'. $aSourcePath['extension'];

        $this->image->saveAs($sUploadDirectory.DIRECTORY_SEPARATOR.$this->avatar);



        $picture = new  \frontend\components\SimpleImage();

        $picture->load($sUploadDirectory.DIRECTORY_SEPARATOR.$this->avatar);

        $imageHeight = $picture->getHeight();

        $imageWidth = $picture->getWidth();

        if( $imageHeight < 300 || $imageWidth < 300 ){
            return [
                'name'=>'',
                'status'=>'error',
                'message'=>'Ошибка! Изображение должно быть не меньше 300x300 пикселей'
            ];
        }

        if($imageWidth > 600 && $imageWidth == $imageHeight)    {
            $picture->resizeToHeight(600);
        }

        if($imageWidth > 630 && $imageWidth> $imageHeight){
            $picture->resizeToHeight(300);
        }else if($imageHeight>1110 && $imageHeight >$imageWidth){
            $picture->resizeToWidth(300);
        }
        $picture->save($sSmallImageDirectory.DIRECTORY_SEPARATOR.$this->avatar);

        return [
            'name'=>$this->avatar,
            'status'=>'success',
            'message'=>''
        ];

    }

    /**
     * Url аватарки
     *
     * @return string
     */
    public function getUrlAvatar(){

        $sSmallImageDirectory = Yii::getAlias('@frontend/web/upload/150x150');

        if( is_file($sSmallImageDirectory.DIRECTORY_SEPARATOR.$this->avatar))
            return '/upload/150x150/'.$this->avatar;
        else
            return '/themes/app/img/profile.jpg';
    }

    /**
     * Списывание денежных средств
     * @param $sum
     * @return bool
     */
    public function debitFunds($sum){

        $this->setScenario('buy');

        if( $this->balance >= (float)$sum){

            $this->balance =  $this->balance - (float)$sum;

            $this->save();

            return $this->save();

        }else{
            return false;
        }
    }

    public function getUserThemeCurrent(){
        return $this->hasOne(UserTheme::className(), ['user_id' => 'id'])->andOnCondition(['is_current'=>1]);
    }

    public function getCurrentTheme(){
        return $this->hasOne(Themes::className(), ['id' => 'theme_id'])
            ->via('userThemeCurrent');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEducations()
    {
        return $this->hasMany(Education::className(), ['user_id' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIsPublicEducations()
    {
        return $this->hasMany(Education::className(), ['user_id' => 'id'])->andOnCondition(['is_public'=>1]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExperiences()
    {
        return $this->hasMany(Experience::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIsPublicExperiences()
    {
        return $this->hasMany(Experience::className(), ['user_id' => 'id'])->andOnCondition(['is_public'=>1]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInterests()
    {
        return $this->hasMany(Interests::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIsPublicInterests()
    {
        return $this->hasMany(Interests::className(), ['user_id' => 'id'])->andOnCondition(['is_public'=>1]);
    }

//    /**
//     * @return \yii\db\ActiveQuery
//     */
//    public function getMessages()
//    {
//        return $this->hasMany(Message::className(), ['user_id' => 'id']);
//    }
//
//    /**
//     * @return \yii\db\ActiveQuery
//     */
//    public function getPaymentHistories()
//    {
//        return $this->hasMany(PaymentHistory::className(), ['user_id' => 'id']);
//    }
//
//    /**
//     * @return \yii\db\ActiveQuery
//     */
//    public function getPortfolioCategories()
//    {
//        return $this->hasMany(PortfolioCategory::className(), ['user_id' => 'id']);
//    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSkills()
    {
        return $this->hasMany(Skills::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIsPublicSkills()
    {
        return $this->hasMany(Skills::className(), ['user_id' => 'id'])->andOnCondition(['is_public'=>1]);
    }

    /**
 * @return \yii\db\ActiveQuery
 */
    public function getIsPublicPortfolio()
    {
        return $this->hasMany(Portfolio::className(), ['user_id' => 'id'])->andOnCondition(['is_public'=>1]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPortfolio()
    {
        return $this->hasMany(Portfolio::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(DictionaryLanguage::className(), ['id' => 'language_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserThemes()
    {
        return $this->hasMany(UserTheme::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['country_id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['city_id' => 'city_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id']);
    }

    public function incBalance($sum)
    {
        $this->setScenario('buy');

        $user = User::u();

        $user->balance += (float)$sum;
        return $user->save();
    }

    public function decBalance($sum)
    {
        $this->setScenario('buy');

        $user = User::u();

        if ($user->balance >= (float)$sum) {
            $user->balance -= (float)$sum;
            return $user->save();
        }

        return false;
    }

    public function buyPeriod($sum, $period){
        $this->setScenario('period');
        $this->expire_date += $period;
        $this->status = self::STATUS_ACTIVE;
        $this->save();
        return Invoice::addInvoice($sum, 'Покупка подписки', Invoice::STATUS_SUCCESS, Invoice::TYPE_DECREMENT );
    }

    public static function sendCommingMessageEmail($message)
    {
        return \Yii::$app->mail->compose('commingMessage', ['message' => $message])
            ->setFrom('no-reply@myres.pro')
            ->setTo(User::findOne(['id' => $message->to_user_id])->email)
            ->setSubject(Yii::t('app', 'Вам пришло сообщение'))
            ->send();
    }

    public function beforeDelete(){

        $sSmallImageDirectory = Yii::getAlias('@frontend/web/upload/150x150');
        $originalImageDirectory = Yii::getAlias('@frontend/web/upload/original');
        $sSmallProfileImageDirectory = Yii::getAlias('@frontend/web/upload/200x200');

        if($this->avatar) {
            $small = $sSmallImageDirectory . DIRECTORY_SEPARATOR . $this->avatar;
            $original = $originalImageDirectory . DIRECTORY_SEPARATOR . $this->avatar;

            if (is_file($small) && is_file($original)) {
                unlink($small);
                unlink($original);
            }
        }

        if($this->portfolio){

            foreach($this->portfolio as $item) {
                $portfolioSmall = $sSmallProfileImageDirectory . DIRECTORY_SEPARATOR . $item->img;
                $portfolioOriginal = $originalImageDirectory . DIRECTORY_SEPARATOR . $item->img;

                if (is_file($portfolioOriginal) && is_file($portfolioSmall)) {
                    unlink($portfolioOriginal);
                    unlink($portfolioSmall);
                }
            }
        }
        return true;
    }
}