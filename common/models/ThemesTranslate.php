<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_themes_translate".
 *
 * @property integer $id
 * @property integer $object_id
 * @property string $title
 * @property integer $language_id
 *
 * @property DictionaryLanguage $language
 * @property Themes $object
 */
class ThemesTranslate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_themes_translate';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['object_id', 'title', 'language_id'], 'required'],
            [['object_id', 'language_id'], 'integer'],
            [['title'], 'string', 'max' => 255],
            ['title', 'filter', 'filter' => function ($value) {
                    return \common\filters\HtmlPurifier::escape( $value );
                }],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(DictionaryLanguage::className(), ['id' => 'language_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObject()
    {
        return $this->hasOne(Themes::className(), ['id' => 'object_id']);
    }
}
