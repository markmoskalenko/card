<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_dictionary_language".
 *
 * @property string $id
 * @property string $name
 * @property string $abbr
 * @property string $local
 * @property string $url
 * @property integer $default
 *
 * @property DictionaryCompany[] $dictionaryCompanies
 * @property DictionaryInterests[] $dictionaryInterests
 * @property DictionarySchools[] $dictionarySchools
 * @property DictionarySkills[] $dictionarySkills
 * @property User[] $users
 */
class DictionaryLanguage extends \yii\db\ActiveRecord
{
    //Переменная, для хранения текущего объекта языка
    static $current = null;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_dictionary_language';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name',  'local', 'url'], 'required'],
            [['default'], 'integer'],
            [['name', 'local', 'url'], 'string', 'max' => 255],
            ['name', 'filter', 'filter' => function ($value) {
                    return \common\filters\HtmlPurifier::escape( $value );
                }],
            ['local', 'filter', 'filter' => function ($value) {
                    return \common\filters\HtmlPurifier::escape( $value );
                }],
            ['url', 'filter', 'filter' => function ($value) {
                    return \common\filters\HtmlPurifier::escape( $value );
                }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'abbr' => Yii::t('app', 'Abbr'),
            'local' => Yii::t('app', 'Local'),
            'url' => Yii::t('app', 'Url'),
            'default' => Yii::t('app', 'Default'),
        ];
    }

    /**
     * Получение текущего объекта языка
     *
     * @return null
     */
    static function getCurrent()
    {
        if( self::$current === null ){
            self::$current = self::getDefaultLang();
        }
        return self::$current;
    }

    /**
     * Установка текущего объекта языка и локаль пользователя
     *
     * @param null $url
     */
    static function setCurrent($url = null)
    {
        $language = self::getLangByUrl($url);

        if($language){
            self::$current = $language;

        }elseif(Yii::$app->session->get('lang', false)){
            self::$current = Yii::$app->session->get('lang');

        }else{
            self::$current = self::getDefaultLang();
        }

        Yii::$app->language = self::$current->local;

        Yii::$app->session->set('lang',self::$current);
    }

    /**
     * Получения объекта языка по умолчанию
     *
     * @return mixed
     */
    static function getDefaultLang()
    {
        return static::find()->where(['default' => 1])->one();
    }

    /**
     * Получения объекта языка по буквенному идентификатору
     *
     * @param null $url
     * @return null
     */
    static function getLangByUrl($url = null)
    {
        if ($url === null) {
            return null;
        } else {
            $language = static::find()->where(['url' => $url])->one();
            if ( $language === null ) {
                return null;
            }else{
                return $language;
            }
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDictionaryCompanies()
    {
        return $this->hasMany(DictionaryCompany::className(), ['language_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDictionaryInterests()
    {
        return $this->hasMany(DictionaryInterests::className(), ['language_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDictionarySchools()
    {
        return $this->hasMany(DictionarySchools::className(), ['language_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDictionarySkills()
    {
        return $this->hasMany(DictionarySkills::className(), ['language_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['language_id' => 'id']);
    }

    /**
     * Список языков для селекта
     * @return array
     */
    public static function getTreeMap()
    {
        $oLanguage  = self::find()->orderBy('name')->all();

        $result = [];
        foreach ($oLanguage as $c){

            $result[$c->id] = $c->name;
        }
        return $result;
    }
}
