<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use Yii;

/**
 * InvoiceSearch represents the model behind the search form about `common\models\Invoice`.
 */
class InvoiceSearch extends Invoice
{
    public $user;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $userId = null)
    {
        if ($userId) {
            $query = Invoice::find()->where(['user_id' => $userId, 'status' => 1]);
        } else {
            $query = Invoice::find();
        }

        $query->joinWith(
            [
                'user' => function ($q) {
                        $q->from(User::tableName().' u');
                    },
            ]
        );

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'id' => [
                    'asc' => [
                        'id' => SORT_ASC,
                    ],
                    'desc' => [
                        'id' => SORT_DESC,
                    ],
                ],
                'user' => [
                    'asc' => [
                        'u.username' => SORT_ASC,
                    ],
                    'desc' => [
                        'u.username' => SORT_DESC,
                    ],
                ],
                'type' => [
                    'asc' => [
                        'type' => SORT_ASC,
                    ],
                    'desc' => [
                        'type' => SORT_DESC,
                    ],
                ],
                'sum' => [
                    'asc' => [
                        'sum' => SORT_ASC,
                    ],
                    'desc' => [
                        'sum' => SORT_DESC,
                    ],
                ],
            ],
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'u.username', $this->user]);

        return $dataProvider;
    }
}
