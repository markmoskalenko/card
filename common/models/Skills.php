<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_skills".
 *
 * @property integer $id
 * @property integer $skills_id
 * @property integer $user_id
 * @property integer $percent
 *
 * @property User $user
 * @property DictionarySkills $skills
 */
class Skills extends \yii\db\ActiveRecord
{
    public $skill_name;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_skills';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'skill_name', 'percent'], 'required'],
            [['skills_id', 'user_id', 'percent', 'is_public'], 'integer'],
            ['skill_name', 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'skill_name'=> Yii::t('app', 'Название навыка'),
            'skills_id' => Yii::t('app', 'ID навыка'),
            'user_id' => Yii::t('app', 'ID пользователя'),
            'percent' => Yii::t('app', 'Оценка навыка'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSkill()
    {
        return $this->hasOne(DictionarySkills::className(), ['id' => 'skills_id']);
    }

    public function afterFind(){

        $this->skill_name = $this->skill->name;

        parent::afterFind();
    }


    public function beforeValidate(){

        $this->user_id = User::u()->id;

        $this->skills_id = DictionarySkills::getIdByName( $this->skill_name );

        return parent::beforeValidate();
    }
}