<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_pages".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $is_public
 * @property string $slug
 *
 * @property PagesTranslate[] $pagesTranslates
 */
class Pages extends \yii\db\ActiveRecord
{
    public $title;

    public $description;

    public $content;

    public $meta_k;

    public $meta_d;

    public $translateModelName = '\common\models\PagesTranslate';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_pages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'slug'], 'required'],
            [['slug', 'title', 'meta_k'], 'string', 'max' => 255],
            [['description', 'content', 'meta_d'], 'string'],
            [['created_at', 'is_public'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'description' => 'Описание',
            'content' => 'Контент',
            'meta_k' => 'МЕТА Ключевые слова',
            'meta_d' => 'МЕТА Описание',
            'created_at' => 'Создана',
            'is_public' => 'Опубликована',
            'slug' => 'URL',
        ];
    }

    public function behaviors()
    {
        return [
            'translate' => [
                'class' => \common\components\behaviors\TranslateBehavior::className(),
                'translateAttributes'=>array(
                    'title',
                    'description',
                    'content',
                    'meta_k',
                    'meta_d',
                ),
            ],
            'slug' => [
                'class' => 'Zelenin\yii\behaviors\Slug',
                'source_attribute' => 'title',
                'slug_attribute' => 'slug',

                // optional params
                'translit' => true,
                'replacement' => '-',
                'lowercase' => true,
                'unique' => true
            ]
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPagesTranslates()
    {
        return $this->hasMany(PagesTranslate::className(), ['object_id' => 'id']);
    }

    public function getTranslate()
    {
        return $this->hasOne(PagesTranslate::className(), ['object_id' => 'id'])
            ->andOnCondition(['language_id'=>DictionaryLanguage::getCurrent()->id]);
    }
}
