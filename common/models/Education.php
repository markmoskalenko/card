<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_education".
 *
 * @property integer $id
 * @property string $start_date
 * @property string $end_date
 * @property integer $company_id
 * @property integer $country_id
 * @property integer $city_id
 * @property string $comment
 * @property integer $user_id
 *
 * @property DictionarySchools $company
 * @property User $user
 */
class Education extends \yii\db\ActiveRecord
{
    public $company_name;
    public $position_name;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_education';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['start_date', 'end_date',  'position_name', 'company_name', 'comment', 'company_id', 'country_id', 'city_id', 'user_id'], 'required'],
            [['company_id', 'country_id', 'city_id', 'user_id', 'is_public'], 'integer'],
            [['position_name', 'company_name', 'comment'], 'string'],
            ['comment', 'filter', 'filter' => function ($value) {
                    return nl2br( \common\filters\HtmlPurifier::escape( $value, 'br') );
                }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'start_date' => Yii::t('app', 'Дата начала'),
            'dates'=> Yii::t('app', 'Дата'),
            'end_date' => Yii::t('app', 'Дата окончания'),
            'company_id' => Yii::t('app', 'Учебное заведение'),
            'company_name' => Yii::t('app', 'Учебное заведение'),
            'country_id' => Yii::t('app', 'Страна'),
            'city_id' => Yii::t('app', 'Город'),
            'comment' => Yii::t('app', 'Комментарий'),
            'user_id' => Yii::t('app', 'ID пользователя'),
            'position_name' => Yii::t('app', 'Факультет'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(DictionarySchools::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['country_id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosition()
    {
        return $this->hasOne(DictionaryFaculties::className(), ['id' => 'position_id']);
    }

    public function afterFind(){

        $this->company_name = $this->company->name;

        $this->position_name = $this->position->name;


        parent::afterFind();
    }


    public function beforeValidate(){

        $this->user_id = User::u()->id;

        $this->company_id = DictionarySchools::getIdByName( $this->company_name );

        $this->position_id = DictionaryFaculties::getIdByName( $this->position_name );

        return parent::beforeValidate();
    }
}