<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_dictionary_theme_category".
 *
 * @property integer $id
 * @property integer $sort
 *
 * @property DictionaryThemeCategoryTranslate[] $dictionaryThemeCategoryTranslates
 */
class DictionaryThemeCategory extends \yii\db\ActiveRecord
{
    public $title;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_dictionary_theme_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sort'], 'required'],
            [['sort'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'sort' => Yii::t('app', 'Sort'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDictionaryThemeCategoryTranslates()
    {
        return $this->hasMany(DictionaryThemeCategoryTranslate::className(), ['object_id' => 'id']);
    }
}
