<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_interests".
 *
 * @property integer $id
 * @property integer $interest_id
 * @property integer $user_id
 * @property string $description
 *
 * @property DictionaryInterests $interest
 * @property User $user
 */
class Interests extends \yii\db\ActiveRecord
{
    public $interest_name;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_interests';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['interest_id', 'user_id', 'interest_name', 'description'], 'required'],
            [['interest_id', 'user_id', 'is_public'], 'integer'],
            [['description', 'interest_name'], 'string'],
            ['description', 'filter', 'filter' => function ($value) {
                    return nl2br( \common\filters\HtmlPurifier::escape( $value, 'br') );
                }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'interest_name' => Yii::t('app', 'Название интереса'),
            'interest_id' => Yii::t('app', 'ID интереса'),
            'user_id' => Yii::t('app', 'ID пользователя'),
            'description' => Yii::t('app', 'Описание'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInterest()
    {
        return $this->hasOne(DictionaryInterests::className(), ['id' => 'interest_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function afterFind(){

        $this->interest_name = $this->interest->name;

        parent::afterFind();
    }

    public function beforeValidate(){

        $this->user_id = User::u()->id;

        $this->interest_id = DictionaryInterests::getIdByName( $this->interest_name );

        return parent::beforeValidate();
    }
}
