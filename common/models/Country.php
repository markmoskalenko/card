<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "tbl_country".
 *
 * @property string $country_id
 * @property integer $city_id
 * @property string $name
 *
 * @property City[] $cities
 */
class Country extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_country';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id'], 'integer'],
            [['name'], 'string', 'max' => 128],
            ['name', 'filter', 'filter' => function ($value) {
                    return \common\filters\HtmlPurifier::escape( $value );
                }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'country_id' => 'Country ID',
            'city_id' => 'City ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasMany(City::className(), ['country_id' => 'country_id']);
    }


    /**
     * Возвращает массив key=>value
     * Нужнен для dropDown силектов
     *
     * @return array
     */
    public static function getMap(){

        $oModel = self::find()->asArray()->all();

        return ArrayHelper::map( $oModel, 'country_id', 'name' );
    }
    /**
     * Список стран для селекта
     * @return array
     */
    public static function getTreeMap()
    {
        $oCountry  = self::find()->orderBy('name')->all();

        $result = [];
        foreach ($oCountry as $c){

            $result[$c->country_id] = $c->name;
        }
        return $result;
    }

}
