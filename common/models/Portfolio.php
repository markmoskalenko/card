<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_portfolio".
 *
 * @property integer $id
 * @property string $img
 * @property integer $category_id
 * @property integer $user_id
 *
 * @property User $user
 * @property PortfolioCategory $category
 */
class Portfolio extends \yii\db\ActiveRecord
{
    public $x;
    public $y;
    public $width;
    public $height;
    public $filename;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_portfolio';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['img', 'category_id', 'user_id'], 'required'],
            [['category_id','x','y','width', 'height', 'user_id', 'is_public'], 'integer'],
            [['img','name','filename'], 'string', 'max' => 255],
            ['img', 'filter', 'filter' => function ($value) {
                    return \common\filters\HtmlPurifier::escape( $value );
                }],
            ['name', 'filter', 'filter' => function ($value) {
                return \common\filters\HtmlPurifier::escape( $value );
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'img' => Yii::t('app', 'Изображение'),
            'file' => Yii::t('app', 'Изображение'),
            'category_id' => Yii::t('app', 'Категория'),
            'user_id' => Yii::t('app', 'Пользователь'),
            'name' => Yii::t('app', 'Название'),

        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(PortfolioCategory::className(), ['id' => 'category_id']);
    }
}
