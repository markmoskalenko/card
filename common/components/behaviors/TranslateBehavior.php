<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 16.08.14
 * Time: 11:23
 */

namespace common\components\behaviors;

use common\models\DictionaryLanguage;
use yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;

class TranslateBehavior extends Behavior {

    /**
     * @var array Model attributes available for translate
     */
    public $translateAttributes = array();

    /**
     * @var string
     */
    public $relationName = 'translate';

    /**
     * @var bool Disable yii model events. When disabled, behaviour
     */
    public $disableEvents = false;

    /**
     * @var integer Language id used to load model translation data.
     * If null active language id we'll be used.
     */
    private $_translation_lang;

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_FIND => 'afterFind',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterUpdate',
            ActiveRecord::EVENT_AFTER_INSERT => 'afterInsert'
        ];
    }

    public function afterFind(){
        $this->applyTranslation();
    }

    /**
     * Apply translated attrs
     */
    public function applyTranslation()
    {
        $relation = $this->relationName;
        if($this->owner->$relation)
        {
            foreach($this->translateAttributes as $attr)
                $this->owner->$attr = $this->owner->$relation->$attr;
        }
    }

    /**
     * Create new object translation for each language.
     * Used on creating new object.
     */
    public function insertTranslations()
    {
        foreach (DictionaryLanguage::find()->all() as $lang)
            $this->createTranslation($lang->id);
    }

    public function afterInsert(){
        $this->insertTranslations();
        return true;
    }

    /**
     * Update model translations
     */
    public function afterUpdate()
    {
        $relation = $this->relationName;
        $translate = $this->owner->$relation;
        if( $translate )
            $this->updateTranslation($translate);
        else
            $this->insertTranslations();

        return true;
    }

    /**
     * Update "current" translation object
     * @param BaseModel $translate
     */
    public function updateTranslation($translate)
    {
        foreach ($this->translateAttributes as $attr)
            $translate->$attr = $this->owner->$attr;
        $translate->save(false);
    }

    /**
     * Create object translation
     * @param int $languageId Language id
     * @return boolean Translation save result
     */
    public function createTranslation($languageId)
    {
        $className = $this->owner->translateModelName;
        $translate = new $className;
        $translate->object_id = $this->owner->getPrimaryKey();
        $translate->language_id = $languageId;

        // Populate translated attributes
        foreach($this->translateAttributes as $attr)
            $translate->$attr = $this->owner->$attr;

        return $translate->save(false);
    }


} 