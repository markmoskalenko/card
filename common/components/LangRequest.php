<?php
namespace common\components;

use Yii;
use yii\web\Request;
use common\models\DictionaryLanguage;

class LangRequest extends Request
{
    protected function resolveRequestUri()
    {
        $lang_prefix = null;
        $requestUri = parent::resolveRequestUri();
        $requestUriToList = explode('/', $requestUri);
        $lang_url = isset($requestUriToList[1]) ? $requestUriToList[1] : null;

        DictionaryLanguage::setCurrent($lang_url);

        if( $lang_url !== null && $lang_url === DictionaryLanguage::getCurrent()->url && strpos($requestUri, DictionaryLanguage::getCurrent()->url) === 1 )
        {
            $requestUri = substr($requestUri, strlen(DictionaryLanguage::getCurrent()->url)+1 );
        }
        return $requestUri;
    }
}