<?php

use yii\db\Schema;

class m140807_161004_add_column_for_social_field_user_table extends \yii\db\Migration
{
    public function up()
    {
        $this->addColumn('{{%user}}', 'facebook', Schema::TYPE_STRING . '(255)');
        $this->addColumn('{{%user}}', 'twitter', Schema::TYPE_STRING . '(255)');
        $this->addColumn('{{%user}}', 'flickr', Schema::TYPE_STRING . '(255)');
        $this->addColumn('{{%user}}', 'google', Schema::TYPE_STRING . '(255)');
        $this->addColumn('{{%user}}', 'vk', Schema::TYPE_STRING . '(255)');
        $this->addColumn('{{%user}}', 'git', Schema::TYPE_STRING . '(255)');
        $this->addColumn('{{%user}}', 'linkedin', Schema::TYPE_STRING . '(255)');
        $this->addColumn('{{%user}}', 'mail', Schema::TYPE_STRING . '(255)');

    }

    public function down()
    {
        echo "m140807_161004_add_column_for_social_field_user_table cannot be reverted.\n";

        return false;
    }
}
