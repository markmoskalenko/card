<?php

use yii\db\Schema;
use yii\db\Migration;

class m140919_084105_add_column_name_for_Partfolio extends Migration
{
    public function up()
    {
        $this->addColumn('tbl_portfolio','name','varchar(255)');
    }

    public function down()
    {
       $this->dropColumn('tbl_portfolio','name','varchar(255)');
    }
}
