<?php

use yii\db\Schema;
use yii\db\Migration;

class m140813_065308_Position extends Migration
{
    public function up()
    {

        $this->createTable('{{%dictionary_position}}', [
            'id' => 'pk',
            'name'=> 'varchar(255)',
            'is_public'=> 'tinyint(3) unsigned',
            'language_id'=> 'integer(11) unsigned',
        ]);

        $this->addForeignKey('fk_dictionary_position_language', '{{%dictionary_position}}', 'language_id', '{{%dictionary_language}}', 'id');

        $this->addColumn('{{%experience}}', 'position_id', Schema::TYPE_INTEGER . '(11)');

        $this->addForeignKey('fk_experience_dictionary_position', '{{%experience}}', 'position_id', '{{%dictionary_position}}', 'id');


    }

    public function down()
    {
        echo "m140813_065308_Position cannot be reverted.\n";

        return false;
    }
}
