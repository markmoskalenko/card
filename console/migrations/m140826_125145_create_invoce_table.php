<?php

use yii\db\Schema;
use yii\db\Migration;

class m140826_125145_create_invoce_table extends Migration
{
    public function up()
    {
	$this->createTable('tbl_invoice', [
            'id' => 'pk',
            'sum' => Schema::TYPE_INTEGER . ' NOT NULL',
            'date' => Schema::TYPE_INTEGER,
            'status' => Schema::TYPE_INTEGER,
            'user_id' => Schema::TYPE_INTEGER,
            'type' => Schema::TYPE_DECIMAL,
            'description' => Schema::TYPE_STRING,
	    'balance' => Schema::TYPE_DECIMAL,
        ]);
    }

    public function down()
    {
//        echo "m140826_125145_create_invoce_table cannot be reverted.\n";

//        return false;
	$this->dropTable('tbl_invoice');
    }
}
