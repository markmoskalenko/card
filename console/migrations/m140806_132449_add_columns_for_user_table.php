<?php

use yii\db\Schema;

class m140806_132449_add_columns_for_user_table extends \yii\db\Migration
{
    public function up()
    {
        $this->addColumn('{{%user}}', 'surname', Schema::TYPE_STRING . '(255)');
        $this->addColumn('{{%user}}', 'date_of_birth_day', Schema::TYPE_INTEGER . '(4)');
        $this->addColumn('{{%user}}', 'date_of_birth_month', Schema::TYPE_INTEGER . '(4)');
        $this->addColumn('{{%user}}', 'date_of_birth_year', Schema::TYPE_INTEGER . '(4)');

    }

    public function down()
    {
        echo "m140806_132449_add_columns_for_user_table cannot be reverted.\n";

        return false;
    }
}
