<?php
/* @var $this yii\web\View */
use frontend\components\DateHelper;

?>
<table class="table table-bordered">

<tr>
    <td style="text-align: center"><strong>Описание</strong></td>
    <td style="text-align: center"><strong>Код</strong></td>
    <td style="text-align: center"><strong>Результат</strong></td>
</tr>
<tr>
    <td><strong>Данные о пользователе</strong></td>
</tr>
<tr>
    <td>Адрес сайта</td>
    <td>if( !empty( $oUser->subdomain)):
        $oUser->subdomain
        endif;
    </td>
    <td><?php if( !empty( $oUser->subdomain)): ?>
            <?= $oUser->subdomain?>
        <?php endif;?>
    </td>
</tr>
<tr>

    <td>Имя</td>
    <td>if( !empty( $oUser->username ) ) :
        $oUser->username
        endif;
    </td>
    <td><?php if( !empty( $oUser->username ) ) : ?>
            <?=$oUser->username?>
        <?php endif;?>
    </td>

</tr>
<tr>
    <td>Фамилия</td>
    <td>if( !empty( $oUser->surname ) ) :
        $oUser->surname
        endif;
    </td>
    <td><?php if( !empty( $oUser->surname ) ) : ?>
            <?=$oUser->surname?>
        <?php endif;?>
    </td>
</tr>
<tr>
    <td>E-mail</td>
    <td>if( !empty( $oUser->email ) ) :
        $oUser->email
        endif;
    </td>
    <td><?php if( !empty( $oUser->email ) ) : ?>
            <?=$oUser->email?>
        <?php endif;?>
    </td>
</tr>
<tr>
    <td>Мое кредо</td>
    <td>if( !empty( $oUser->s_status_title ) ) :
        $oUser->s_status_title
        endif;
    </td>
    <td><?php if( !empty( $oUser->s_status_title ) ) : ?>
            <?=$oUser->s_status_title?>
        <?php endif;?>
    </td>
</tr>
<tr>
    <td>Аватар</td>
    <td>$oUser->getUrlAvatar()</td>
    <td><?= $oUser->getUrlAvatar() ?></td>
</tr>
<tr>
    <td>День рождения</td>
    <td>if( !empty( $oUser->date_of_birth_day )){DateHelper::getDayByCod($oUser->date_of_birth_day)}</td>
    <td><?php if( !empty( $oUser->date_of_birth_day )){
            echo DateHelper::getDayByCod($oUser->date_of_birth_day);
        }?>
    </td>
</tr>
<tr>
    <td>Месяц рождения</td>
    <td>if( !empty( $oUser->date_of_birth_month )){DateHelper::getMonthByCod($oUser->date_of_birth_month)}</td>
    <td><?php if( !empty( $oUser->date_of_birth_month )){
            echo DateHelper::getMonthByCod($oUser->date_of_birth_month);
        } ?></td>
</tr>
<tr>
    <td>Год рождения</td>
    <td>if( !empty( $oUser->date_of_birth_year )){DateHelper::getYearsByCod($oUser->date_of_birth_year);}</td>
    <td><?php if( !empty( $oUser->date_of_birth_year )){
            echo DateHelper::getYearsByCod($oUser->date_of_birth_year);
        } ?></td>
</tr>
<tr>
    <td><strong>Опыт работы</strong></td>
</tr>
<tr>
    <td>Дата с</td>
    <td> if( !empty( $oUser->isPublicExperiences )){ foreach( $oUser->isPublicExperiences as $oItemExperience ){$oItemExperience->start_date;}}</td>
    <td><?php if( !empty( $oUser->isPublicExperiences )){
            foreach( $oUser->isPublicExperiences as $oItemExperience ){
                echo $oItemExperience->start_date.'<br/>';
            }
        }?>
    </td>
</tr>
<tr>
    <td>Дата по</td>
    <td>if( !empty( $oUser->isPublicExperiences )){ foreach( $oUser->isPublicExperiences as $oItemExperience ){$oItemExperience->end_date;}}</td>
    <td><?php if( !empty( $oUser->isPublicExperiences )){
            foreach( $oUser->isPublicExperiences as $oItemExperience ){
                echo $oItemExperience->end_date.'<br/>';
            }
        }?>
    </td>
</tr>
<tr>
    <td>Компания</td>
    <td>if( !empty( $oUser->isPublicExperiences )){ foreach( $oUser->isPublicExperiences as $oItemExperience ){$oItemExperience->company->name;}}</td>
    <td><?php if( !empty( $oUser->isPublicExperiences )){
            foreach( $oUser->isPublicExperiences as $oItemExperience ){
                echo $oItemExperience->company->name.'<br/>';
            }
        }
        ?>
    </td>
</tr>
<tr>
    <td>Должность</td>
    <td>if( !empty( $oUser->isPublicExperiences )){ foreach( $oUser->isPublicExperiences as $oItemExperience ){$oItemExperience->position->name;}}</td>
    <td><?php if( !empty( $oUser->isPublicExperiences )){
            foreach( $oUser->isPublicExperiences as $oItemExperience ){
                echo $oItemExperience->position->name.'<br/>';
            }
        }
        ?>
    </td>
</tr>
<tr>
    <td>Страна</td>
    <td>if( !empty( $oUser->isPublicExperiences )){foreach( $oUser->isPublicExperiences as $oItemExperience ){$oItemExperience->country->name;}} </td>
    <td><?php if( !empty( $oUser->isPublicExperiences )){
            foreach( $oUser->isPublicExperiences as $oItemExperience ){
                echo $oItemExperience->country->name.'<br/>';
            }
        }?>
    </td>
</tr>
<tr>
    <td>Город</td>
    <td>if( !empty( $oUser->isPublicExperiences )){ foreach( $oUser->isPublicExperiences as $oItemExperience ){$oItemExperience->city->name;}}</td>
    <td><?php if( !empty( $oUser->isPublicExperiences )){
            foreach( $oUser->isPublicExperiences as $oItemExperience ){
                echo $oItemExperience->city->name.'<br/>';
            }
        }?>
    </td>
</tr>
<tr>
    <td>Коментарий</td>
    <td>if( !empty( $oUser->isPublicExperiences )){ foreach( $oUser->isPublicExperiences as $oItemExperience ){$oItemExperience->comment;}}</td>
    <td><?php if( !empty( $oUser->isPublicExperiences )){
            foreach( $oUser->isPublicExperiences as $oItemExperience ){
                echo $oItemExperience->comment.'<br/>';
            }
        }?>
    </td>
</tr>
<tr>
    <td><strong>Образование</strong></td>
</tr>
<tr>
    <td>Дата с</td>
    <td>if( !empty( $oUser->isPublicEducations )){ foreach( $oUser->isPublicEducations as $oItemEducation ){$oItemEducation->start_date;}}</td>
    <td><?php if( !empty( $oUser->isPublicEducations )){
            foreach( $oUser->isPublicEducations as $oItemEducation ){
                echo $oItemEducation->start_date.'<br/>';
            }
        }?>
    </td>
</tr>
<tr>
    <td>Дата по</td>
    <td>if( !empty( $oUser->isPublicEducations )){ foreach( $oUser->isPublicEducations as $oItemEducation ){$oItemEducation->end_date;}}</td>
    <td><?php if( !empty( $oUser->isPublicEducations )){
            foreach( $oUser->isPublicEducations as $oItemEducation ){
                echo $oItemEducation->end_date.'<br/>';
            }
        }?>
    </td>
</tr>
<tr>
    <td>Учебное заведение</td>
    <td>if( !empty( $oUser->isPublicEducations )){ foreach( $oUser->isPublicEducations as $oItemEducation ){$oItemEducation->company->name;}}</td>
    <td><?php if( !empty( $oUser->isPublicEducations )){
            foreach( $oUser->isPublicEducations as $oItemEducation ){
                echo $oItemEducation->company->name.'<br/>';
            }
        }
        ?>
    </td>
</tr>
<tr>
    <td>Факультет</td>
    <td>if( !empty( $oUser->isPublicEducations )){ foreach( $oUser->isPublicEducations as $oItemEducation ){$oItemEducation->position->name;}}</td>
    <td><?php if( !empty( $oUser->isPublicEducations )){
            foreach( $oUser->isPublicEducations as $oItemEducation ){
                echo $oItemEducation->position->name.'<br/>';
            }
        }
        ?>
    </td>
</tr>
<tr>
    <td>Страна</td>
    <td>if( !empty( $oUser->isPublicEducations )){ foreach( $oUser->isPublicEducations as $oItemEducation ){$oItemEducation->country->name;}} </td>
    <td><?php if( !empty( $oUser->isPublicEducations )){
            foreach( $oUser->isPublicEducations as $oItemEducation ){
                echo $oItemEducation->country->name.'<br/>';
            }
        }?>
    </td>
</tr>
<tr>
    <td>Город</td>
    <td>if( !empty( $oUser->isPublicEducations )){ foreach( $oUser->isPublicEducations as $oItemEducation ){$oItemEducation->city->name;}}</td>
    <td><?php if( !empty( $oUser->isPublicEducations )){
            foreach( $oUser->isPublicEducations as $oItemEducation ){
                echo $oItemEducation->city->name.'<br/>';
            }
        }?>
    </td>
</tr>
<tr>
    <td>Коментарий</td>
    <td>if( !empty( $oUser->isPublicSkills )){foreach( $oUser->isPublicEducations as $oItemEducation ){$oItemEducation->comment;}}</td>
    <td><?php if( !empty( $oUser->isPublicEducations )){
            foreach( $oUser->isPublicEducations as $oItemEducation ){
                echo $oItemEducation->comment.'<br/>';
            }
        }?>
    </td>
</tr>
<tr>
    <td><strong>Профессиональные навыки</strong></td>
</tr>
<tr>
    <td>Навык</td>
    <td>if( !empty( $oUser->isPublicSkills )){foreach( $oUser->isPublicSkills as $oItemSkill ){$oItemSkill->skill_name;}}</td>
    <td><?php if( !empty( $oUser->isPublicSkills )){
            foreach( $oUser->isPublicSkills as $oItemEducation ){
                echo $oItemEducation->skill_name.'<br/>';
            }
        }?>
    </td>
</tr>
<tr>
    <td>Оценка навыка</td>
    <td>if( !empty( $oUser->isPublicSkills )){foreach( $oUser->isPublicSkills as $oItemSkill ){$oItemSkill->percent;}}</td>
    <td><?php if( !empty( $oUser->isPublicSkills )){
            foreach( $oUser->isPublicSkills as $oItemEducation ){
                echo $oItemEducation->percent.'<br/>';
            }
        }?>
    </td>
</tr>
<tr>
    <td><strong>Интересы</strong></td>
</tr>
<tr>
    <td>Название интереса</td>
    <td>if( !empty( $oUser->isPublicInterests )){foreach( $oUser->isPublicInterests as $oItemInteres ){$oItemInteres->skill_name;}}</td>
    <td><?php if( !empty( $oUser->isPublicInterests )){
            foreach( $oUser->isPublicInterests as $oItemInteres ){
                echo $oItemInteres->interest_name.'<br/>';
            }
        }?>
    </td>
</tr>
<tr>
    <td>Описание</td>
    <td>if( !empty( $oUser->isPublicInterests )){ foreach( $oUser->isPublicInterests as $oItemInteres ){$oItemInteres->skill_name;}}</td>
    <td><?php if( !empty( $oUser->isPublicInterests )){
            foreach( $oUser->isPublicInterests as $oItemInteres ){
                echo $oItemInteres->description.'<br/>';
            }
        }?>
    </td>
</tr>
<tr>
    <td><strong>Контакты</strong></td>
</tr>
<tr>
    <td>Страна</td>
    <td>if( isset( $oUser->country ) ) :
        $oUser->country->name
        endif;
    </td>
    <td><?php if( isset( $oUser->country ) ) : ?>
            <?=$oUser->country->name?>
        <?php endif;?>
    </td>
</tr>
<tr>
    <td>Город</td>
    <td>if( isset( $oUser->city)):
        $oUser->city->name
        endif;
    </td>
    <td><?php if( isset( $oUser->city)):?>
            <?=$oUser->city->name?>
        <?php endif;?>
    </td>
</tr>
<tr>
    <td>Телефон</td>
    <td>if( !empty( $oUser->phone ) ) :
        $oUser->phone
        endif;
    </td>
    <td><?php if( !empty( $oUser->phone ) ) : ?>
            <?=$oUser->phone?>
        <?php endif;?>
    </td>
</tr>
<tr>
    <td>Мобильный</td>
    <td>if( !empty( $oUser->phone_mob ) ) :
        $oUser->phone_mob
        endif;
    </td>
    <td><?php if( !empty( $oUser->phone_mob ) ) : ?>
            <?=$oUser->phone_mob?>
        <?php endif;?>
    </td>
</tr>
<tr>
    <?php if( !empty( $oUser->my_site ) ) : ?>
        <td>Мой сайт</td>
        <td>if( !empty( $oUser->my_site ) ) :
            $oUser->my_site
            endif;
        </td>
        <td>
            <?=$oUser->my_site?>
        </td>
    <?php endif;?>
</tr>
<tr>
    <td><strong>Сициальные сети</strong></td>
</tr>
<tr>
    <td>Facebook</td>
    <td>if( !empty( $oUser->s_fb ) ) :
        $oUser->s_fb
        endif;
    </td>
    <td><?php if( !empty( $oUser->s_fb ) ) : ?>
            <?=$oUser->s_fb?>
        <?php endif;?>
    </td>
</tr>
<tr>
    <td>Вконтакте</td>
    <td>if( !empty( $oUser->s_vk ) ) :
        $oUser->s_vk
        endif;
    </td>
    <td><?php if( !empty( $oUser->s_vk ) ) : ?>
            <?=$oUser->s_vk?>
        <?php endif;?>
    </td>
</tr>
<tr>
    <td>Google+</td>
    <td>if( !empty( $oUser->s_gplus ) ) :
        $oUser->s_gplus
        endif;
    </td>
    <td><?php if( !empty( $oUser->s_gplus ) ) : ?>
            <?=$oUser->s_gplus?>
        <?php endif;?>
    </td>
</tr>
<tr>
    <td>Twitter</td>
    <td>if( !empty( $oUser->s_tw ) ) :
        $oUser->s_tw
        endif;
    </td>
    <td><?php if( !empty( $oUser->s_tw ) ) : ?>
            <?=$oUser->s_tw?>
        <?php endif;?>
    </td>
</tr>
<tr>
    <td>GitHub</td>
    <td>if( !empty( $oUser->s_git ) ) :
        $oUser->s_git
        endif;
    </td>
    <td><?php if( !empty( $oUser->s_git ) ) : ?>
            <?=$oUser->s_git?>
        <?php endif;?>
    </td>
</tr>
<tr>
    <td>LinkedIn</td>
    <td>if( !empty( $oUser->linkedin ) ) :
        $oUser->linkedin
        endif;
    </td>
    <td><?php if( !empty( $oUser->linkedin ) ) : ?>
            <?=$oUser->linkedin?>
        <?php endif;?>
    </td>
</tr>

<tr>
    <td>Flickr</td>
    <td>if( !empty( $oUser->flickr ) ) :
        $oUser->flickr
        endif;
    </td>
    <td><?php if( !empty( $oUser->flickr ) ) : ?>
            <?=$oUser->flickr?>
        <?php endif;?>
    </td>
</tr>
<tr>
    <td>МойКруг@mail.ru</td>
    <td>if( !empty( $oUser->s_mail ) ) :
        $oUser->s_mail
        endif;
    </td>
    <td><?php if( !empty( $oUser->s_mail ) ) : ?>
            <?=$oUser->s_mail?>
        <?php endif;?>
    </td>
</tr>

</table>

<a  class="btn btn-primary pull-right"  href="<?='vcard.vcf'?>"><?=\Yii::t('app','Сгенерировать Vcard')?></a>

