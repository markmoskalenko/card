<?php
/* @var $this yii\web\View */
use frontend\components\DateHelper;

?>
<header id="header"> 
  
      <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
          <div class="container">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navTop">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
              <a class="navbar-brand" href="#">Myres</a>
            <div class="collapse navbar-collapse" id="navTop">
              <ul class="nav navbar-nav navbar-right text-center">
                <li><a href="#header">Профиль</a></li>
                <li><a href="#work-experience">Опыт работы</a></li>
                <li><a href="#education">Образование</a></li>
                <li><a href="#professional-skills">Навыки</a></li>
                <li><a href="#interests">Интересы</a></li>
                <li><a href="#portfolio">Портфолио</a></li>
                <li><a href="#contact">Контакты</a></li>
              </ul>
            </div>
          </div>
        </nav>
        
        <div  class="container main">
          <div class="row">
            <div class="col-md-3 text-center">
                <div class="userPic">
                   <img src="img/profile.jpg">
                   </div>
            </div> 
            <div class="col-md-4">
               <h1 class="text-center">Александр Артёмов</h1>
               <p class="text-center profilHobby">Моя профессия</p>
               <div class="kredo text-center">
               	  <em>&ldquo;&nbsp;Хороший дизайн виден сразу. Отличный дизайн незаметен.&rdquo;</em>
               </div>
            </div>
            <div class="col-md-2 socseti text-center">
                   <span><a href=""><img src="/img/yan-main.png"/></a></span>
                   <span><a href=""><img src="/img/google-main.png"/></a></span>
                   <span><a href=""><img src="/img/vk-main.png"/></a></span>
                   <span><a href=""><img src="/img/fb-main.png"/></a></span>
                   <span><a href=""><img src="/img/od-main.png"/></a></span>
                   <span><a href=""><img src="/img/tw-main.png"/></a></span>
                   <span><a href=""><img src="/img/mail_main.png"/></a></span>
                   <span><a href=""><img src="/img/git_main.png"/></a></span>
                   <span><a href=""><img src="/img/in-main.png"/></a></span>
                   <span><a href=""><img src="/img/flickr_main.png"/></a></span>  
            </div>
            <div class="col-md-3 infoTop">
            	<p><i class="fa fa-map-marker"></i>&nbsp; Россия, Липецк</p>
                <p><i class="fa fa-phone"></i>&nbsp; 98688909</p> 
                <p><i class="fa fa-mobile"></i>&nbsp; 04568899</p>
                <p><i class="fa fa-skype"></i>&nbsp; skype</p>
                <p><i class="fa fa-envelope"></i>&nbsp;info@3dlip.ru</p>
                <p><i class="fa fa-globe"></i>&nbsp; <a href="http://www.example.com"> www.example.com</a></p>
                <p><i class="fa fa-download"></i>&nbsp; <a href="index.html#"> Скачать резюме</a></p>
                <p><i class="fa fa-th"></i>&nbsp; <a href="index.html#"> Скачать vCard</a></p>
            </div>
         </div>
      </div>

  </header>
  
  <br>
  
  <div class="container main">
    <div id="work-experience" class="row info">
      <div class="col-md-4">
        <h2><i class="fa fa-table"></i>Опыт работы</h2>
      </div>
      <div class="col-md-8 title">
        <ul>
          <li>
               <span class="note">Июнь 2012 - Настоящее время</span>
                <p>
                <strong>Дизайн студия</strong>
                  г.Липецк
                </p>
                <p>
                <strong>WEB дизайнер</strong>
                </p>
                <p class="description">Нормальная работа. Хорошо платят. И беготни меньше.</p>
          </li>
          <li>
              <span class="note">Сентябрь 2010 - Май 2012</span>
              <p><strong>Freelance</strong></p>
              <p><strong>WEB дизайнер</strong>
              г.Липецк</p>
              <p class="description">Был в свободном плавании. Зарплата очень хорошая, но пришлось побегать.</p>
          </li>
        </ul>
      </div>
    </div>
    
    <div id="education" class="row info">
      <div class="col-md-4">
        <h2><i class="fa fa-graduation-cap"></i>Образование</h2>
      </div>
      <div class="col-md-8 title">
        <ul>
          <li>
             <span class="note">Январь 2006 - Апрель 2008</span>
             <p><strong>Курсы художественной графики</strong></p>
             <p><strong>Педагогический институт</strong>&nbsp;&nbsp;г.Липецк</p>
             <p class="description">Приобрёл необходимые знания для дальнейшего развития</p>
           </li>
           <li>
              <span class="note">Сентябрь 2002 - Май 2005</span>
              <p><strong>WEB дизайн</strong></p>
              <p><strong>Обучался самостоятельно</strong></p>
              <p class="description">Наибольшее количество знаний я получил именно в этот период.</p>
          </li>
        </ul>
      </div>
    </div>
    
    <div id="professional-skills" class="row info">
      <div class="col-md-4">
        <h2><i class="fa fa-pencil-square-o"></i>Навыки</h2>
      </div>
      <div class="col-md-8 title">
        <ul>
          <li>
             <span class="note">3ds max</span>
                 <div class="progress progress-striped">
                      <div class="progress-bar progress-bar-success" role="progressbar" aria-valuetransitiongoal="60"></div>
                 </div>             
          </li>
          <li>
             <span class="note">ZBrush</span>
                 <div class="progress progress-striped">
                      <div class="progress-bar progress-bar-info" role="progressbar" aria-valuetransitiongoal="80"></div>
                 </div>             
          </li>
          <li>
             <span class="note">Autodeck Maya</span>
                 <div class="progress progress-striped">
                      <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuetransitiongoal="40"></div>
                 </div>             
          </li>
        </ul>
      </div>
    </div>
    
    <div id="interests" class="row info">
      <div class="col-md-4">
        <h2><i class="fa fa-language"></i>Интересы</h2>
      </div>
      <div class="col-md-8 title">
        <ul>
          <li>
             <span class="note">Книги</span>
             <p>Vestibulum ut pharetra diam, vitae hendrerit sapien. Praesent porttitor augue nec semper suscipit. Nulla et cursus lectus. Praesent vel feugiat dolor.</p>
          </li>
          <li>
             <span class="note">Спорт</span>
             <p>Vestibulum ut pharetra diam, vitae hendrerit sapien. Praesent porttitor augue nec semper suscipit. Nulla et cursus lectus. Praesent vel feugiat dolor.</p>
          </li>
          <li>
             <span class="note">Рисование</span>
             <p>Vestibulum ut pharetra diam, vitae hendrerit sapien. Praesent porttitor augue nec semper suscipit. Nulla et cursus lectus. Praesent vel feugiat dolor.</p>
          </li>
        </ul>
      </div>
    </div>
    
    <div id="portfolio" class="row info">
      <div class="col-md-4">
        <h2><i class="fa fa-picture-o"></i>Портфолио</h2>
      </div>
      <div class="col-md-8 title">
       
        	<div class="col-md-12">
                <div class="portfolioFilter">
                    <a href="#" data-filter="*" class="current">Все работы</a>
                    <a href="#" data-filter=".люди">Люди</a>
                    <a href="#" data-filter=".пейзажи">Пейзажи</a>
                    <a href="#" data-filter=".еда">Еда</a>
                    <a href="#" data-filter=".объекты">Объекты</a>
                </div>
            </div>
            
            <div class="col-md-12">
                    <ul class="portfolioContainer">
                        
                        <li class="объекты">
                            <a class="fancybox" href="uploads/images/portfolio/object1.jpg" title="Название работы">
                                <span class="link-icon"><img src="img/magnify.png"></span> 
                                <span class="overlay"></span>
                                <img src="uploads/images/portfolio/object1_sm.jpg" alt="Portfolio Image"/>
                            </a>   
                        </li>
                        
                        <li class="люди">
                            <a class="fancybox" href="uploads/images/portfolio/people2.jpg" title="Название работы">
                                <span class="link-icon"><img src="img/magnify.png"></span> 
                                <span class="overlay"></span>
                                <img src="uploads/images/portfolio/people2_sm.jpg" alt="Portfolio Image"/>   
                            </a>
                        </li>
                        
                        <li class="еда">
                            <a class="fancybox" href="uploads/images/portfolio/food1.jpg" title="Название работы">
                                <span class="link-icon"><img src="img/magnify.png"></span> 
                                <span class="overlay"></span>
                                <img src="uploads/images/portfolio/food1_sm.jpg" alt="Portfolio Image"/>
                            </a>
                        </li>
                        
                        <li class="пейзажи">
                            <a class="fancybox" href="uploads/images/portfolio/place2.jpg" title="Название работы">
                                <span class="link-icon"><img src="img/magnify.png"></span> 
                                <span class="overlay"></span>
                                <img src="uploads/images/portfolio/place2_sm.jpg" alt="Portfolio Image"/>
                            </a>
                        </li>
                    
                        <li class="пейзажи">
                            <a class="fancybox" href="uploads/images/portfolio/place3.jpg" title="Название работы">
                                <span class="link-icon"><img src="img/magnify.png"></span> 
                                <span class="overlay"></span>
                                <img src="uploads/images/portfolio/place3_sm.jpg" alt="Portfolio Image"/>
                            </a>
                        </li>
                    
                        <li class="объекты">
                            <a class="fancybox" href="uploads/images/portfolio/object2.jpg" title="Название работы">
                                <span class="link-icon"><img src="img/magnify.png"></span> 
                                <span class="overlay"></span>
                                <img src="uploads/images/portfolio/object2_sm.jpg" alt="Portfolio Image"/>
                            </a>
                        </li>
                    
                        <li class="люди">
                            <a class="fancybox" href="uploads/images/portfolio/people1.jpg" title="Название работы">
                                <span class="link-icon"><img src="img/magnify.png"></span> 
                                <span class="overlay"></span>
                                <img src="uploads/images/portfolio/people1_sm.jpg" alt="Portfolio Image"/>
                            </a>
                        </li>
                    
                        <li class="еда">
                            <a class="fancybox" href="uploads/images/portfolio/food2.jpg" title="Название работы">
                                <span class="link-icon"><img src="img/magnify.png"></span> 
                                <span class="overlay"></span>
                                <img src="uploads/images/portfolio/food2_sm.jpg" alt="Portfolio Image"/>
                            </a>
                        </li>
                        
                        <li class="пейзажи">
                            <a class="fancybox" href="uploads/images/portfolio/place1.jpg" title="Название работы">
                                <span class="link-icon"><img src="img/magnify.png"></span> 
                                <span class="overlay"></span>
                                <img src="uploads/images/portfolio/place1_sm.jpg" alt="Portfolio Image"/>
                            </a>
                        </li>
                   </ul>      
                </div>
                
      </div>
    </div>
    
    <div id="contact" class="row info">
      <div class="col-md-4">
        <h2><i class="fa fa-envelope"></i>Контакты</h2>
      </div>
      <div class="col-md-8 title">
          <form class="form-horizontal" role="form">
              <div class="form-group">
                <label for="inputName" class="col-sm-2 control-label">Ваше имя</label>
                <div class="col-sm-10">
                  <input type="name" class="form-control" id="inputName" placeholder="Имя">
                </div>
              </div>
                  
              <div class="form-group">
                <label for="inputEmail" class="col-sm-2 control-label">Email</label>
                <div class="col-sm-10">
                  <input type="email" class="form-control" id="inputEmail" placeholder="Email">
                </div>
              </div>
              
              <div class="form-group">
              	<label for="message" class="col-sm-2 control-label">Сообщение</label>
                <div class="col-sm-offset-2 col-sm-10">
                  <textarea class="form-control" id="message" rows="3"></textarea>
                </div>
              </div>
             
              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <button type="submit" class="btn btn-primary ">Отправить</button>
                </div>
              </div>
            </form>
      </div>
    </div>
    
    <div class="row infoMap">
      <div class="col-md-12">
            <div class="map">
                <div class="row mapArea">
                    <iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/?ie=UTF8&ll=-37.817682,144.957595&spn=0.01134,0.026157&t=m&z=16&output=embed"></iframe>
                </div>
            </div>
      </div>
    </div>  
  </div>
  
  <br>
  
  <div class="crossing">
    <a href="">
        <i class="fa fa-gears gear"></i>
    </a>
  </div>
  
  <footer>
      <div class="container foot">
        <div class="row">
            <div class="col-md-12">
                <div class="text-center">
                  <p>Myres &copy; 2014</p>
                </div>
            </div>
        </div>
        </div>
  </footer>