<?php
use yii\helpers\Html;
use frontend\assets\TestAsset;

/**
 * @var \yii\web\View $this
 * @var string $content
 */
TestAsset::register($this);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <script type='text/javascript'>
        $(document).ready(function() {
          $(".collapse ul li").click(function(e) {
        $(".collapse").removeClass('in');
        })
       });
	</script>
</head>
<body>
    <?php $this->beginBody() ?>
    <?= $content ?>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
