<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="row">
    <div class="col-lg-5">
        <h2><?= \Yii::t('app', 'Смена пароля') ?></h2>
        <hr/>
        <p style="color:red;">
            <?= Yii::$app->session->getFlash('error'); ?>
        </p>
        <p style="color:green;">
            <?= Yii::$app->session->getFlash('success'); ?>
        </p>
        <?php $form = ActiveForm::begin(['id' => 'change-password-form']); ?>
        <?= $form->field($model, 'oldPassword')->passwordInput() ?>
        <?= $form->field($model, 'password')->passwordInput() ?>
        <?= $form->field($model, 'passwordRepeat')->passwordInput() ?>
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', '  Сохранить'), ['class' => 'btn btn-primary']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>