<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<style type="text/css">
    body{
        background-color: #fff;
    }
    h2{
        text-align: center;
        margin: 20px 0;
        color: #3d8ac4;
    }
</style>

<h2>Введите Ваш E-mail адрес</h2>

<?php $form = ActiveForm::begin(['id' => 'change-email']); ?>
<?= $form->field($oUser, 'email') ?>
<?= $form->field($oUser, 'password') ?>
    <?= Html::submitButton(\Yii::t('app', '  Сохранить'), ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
    <a href="javascript:parent.jQuery.fancybox.close();" class="btn btn-danger">Закрыть окно</a>
<?php ActiveForm::end();?>
