<?php if($isActivate): ?>
    <h2 style="text-align: center"><?=\Yii::t('app', 'Ваша учетная запись активирована!')?></h2>
<?php else: ?>
    <h2  style="text-align: center"><?=\Yii::t('app', 'Ошибка активации. Обратитесь к админестрации сайта.')?></h2>
<?php endif; ?>