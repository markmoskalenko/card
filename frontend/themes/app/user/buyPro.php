<section class="accound">
    <div class="accoundContent">
        <div id="accountDetails" class="container">
            <div id="sidebar" class="userNavleft">
                <?= $this->render('//partials/_profileMenu'); ?>
            </div>

            <div id="accound_form" class="row" style="height: 400px">
                <div class="col-md-10 col-md-offset-2">
                    <div class="accoundForm_1 accound_register">
                        <div class="row">
                            <h3><?=Yii::t('app', 'Стоимость подписки на 1 год:')?> <?= \common\models\User::PAY_PERIOD_1_PRICE ?> </h3>

                            <?php if( \common\models\User::u()->balance < \common\models\User::PAY_PERIOD_1_PRICE ): ?>
                                <?=Yii::t('app', 'Недостаточно средств на балансе.')?>
                                 <a href="<?= \yii\helpers\Url::to(['payment/invoice']) ?>"><?=Yii::t('app', 'Пополнить баланс')?></a>
                            <?php else:?>
                                <a href="<?= \yii\helpers\Url::to(['user/buy-pro', 'period'=>\common\models\User::PAY_PERIOD_1]) ?>"><?=Yii::t('app', 'Купить')?></a>
                            <?php endif;?>
                        </div>
                    </div> <!-- accound_register -->
                </div>
            </div> <!-- accound_form -->
        </div> <!-- accountDetails -->
    </div> <!-- accoundContent -->
</section>
