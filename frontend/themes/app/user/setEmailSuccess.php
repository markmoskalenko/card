<?php
?>
<style type="text/css">
    body{
        background-color: #fff;
    }
    h2{
        text-align: center;
        margin: 20px 0;
        color: #3d8ac4;
    }
</style>

<h2><?= !empty($sMessage) ? $sMessage : \Yii::t('app','На Ваш почтовый ящик выслано письмо с инструкциями по активации.'); ?></h2>

<script>
   parent.location.reload();
</script>