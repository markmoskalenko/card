<?php
use frontend\assets\ProfileAsset;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

ProfileAsset::register($this);

/**
 * @var frontend\controllers\ProfileController $this
 * @var yii\widgets\ActiveForm $form
 * @var \common\models\User $model
 */

$this->title = \Yii::t('app','Статистика расходов');

?>

<section class="invoice">
    <div class="accoundContent" ng-app="profileApp">
        <div id="accountDetails" class="container">
            <div id="sidebar" class="userNavleft"><?= $this->render('//partials/_profileMenu'); ?></div>

            <div id="accound_form" class="row">

                <div class="col-md-10 col-md-offset-2">

                    <div class="accoundForm_1 accound_register">
                        <p style="color:red;">
                            <?= Yii::$app->session->getFlash('error'); ?>
                        </p>

                        <?php $form = ActiveForm::begin(['id' => 'invoice-form']); ?>
                        <div id="input_sum" class="col-md-12">
                            <?= $form->field($model, 'sum')->textInput(['placeholder' => \Yii::t('app', 'Сумма пополнения')]) ?>
                        </div>
                        <div id="form_register_btn" class="text-center">
                            <?= Html::submitButton(\Yii::t('app','Пополнить'), ['class' => 'btn btn-primary btn-lg', 'name' => 'login-button']) ?>
                        </div>
                        <?php ActiveForm::end(); ?>

                    </div>
                </div>
            </div>


        </div>
    </div>



</section>
