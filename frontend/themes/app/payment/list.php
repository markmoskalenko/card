<?php

use yii\helpers\Html;
use yii\grid\GridView;

//ProfileAsset::register($this);
$this->title = 'Статистика расходов';

/* @var $this yii\web\View */
/* @var $searchModel common\models\Invoice */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>


<section class="invoice">
    <div class="accoundContent" ng-app="profileApp">
        <div id="accountDetails" class="container">
            <div id="sidebar" class="userNavleft"><?= $this->render('//partials/_profileMenu'); ?></div>
            <div id="accound_form" class="row">

                <div class="col-md-10 col-md-offset-2">
                    <h1><?= Html::encode(\Yii::t('app',$this->title)) ?></h1>

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'columns' => [
                            [
                                'attribute' => 'date',
                                'value' => function($value){
                                        return date('d.m.Y H:i', $value->date);
                                    }
                            ],
                            [
                                'attribute' => 'id',
                            ],
                            [
                                'attribute' => 'type',
                                'value' => function($value){
                                        if (!is_array(\common\models\Invoice::getType($value->type))) {
                                            return \common\models\Invoice::getType($value->type);
                                        } else {
                                            return '';
                                        }
                                    }
                            ],
                            [
                                'attribute' =>  'description',
                                'value' =>function($value){
                                       return Yii::t('app', $value->description);
                                    }
                            ],
                            [
                                'attribute' => 'sum',
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</section>
