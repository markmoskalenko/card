<?php
/* @var $this yii\web\View */
$this->title = 'Установка шаблона'
?>

<section class="accound">
    <div class="accoundContent">
        <div id="accountDetails" class="container">
            <div id="sidebar" class="userNavleft">
                <?= $this->render('//partials/_profileMenu'); ?>
            </div>
            <div id="accound_form" class="row" style="height: 400px">
                <div class="col-md-10 col-md-offset-2">
                    <div class="accoundForm_1 accound_register">
                        <div class="row">
                                <?= \frontend\widgets\Alert::widget();?>
                        </div>
                    </div> <!-- accound_register -->
                </div>
            </div> <!-- accound_form -->
        </div> <!-- accountDetails -->
    </div> <!-- accoundContent -->
</section>
