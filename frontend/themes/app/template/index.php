<?php
/* @var $this yii\web\View */
$this->title = 'Дизайн сайта';
?>

<section class="accound">
<div class="accoundContent">
<div id="accountDetails" class="container">
<div id="sidebar" class="userNavleft">
    <?= $this->render('//partials/_profileMenu'); ?>
</div>

<div id="accound_form" class="row">
<div class="col-md-10 col-md-offset-2">
<div class="accoundForm_1 accound_register">


    <?= \frontend\widgets\Alert::widget();?>

<div class="row">
    <div class="col-md-12 text-center">
        <h3><?=\Yii::t('app','Активный шаблон')?></h3>
    </div>
</div>

<div class="row">
    <?php if( $oTemplateCurrent ): ?>
    <div class="col-md-6 col-md-offset-3 current"> <!------ текущий шаблон ------->
        <div class="thumbnail">
            <?php if($oTemplateCurrent->img != NULL){
                echo '<img src="/upload/theme/200x200/'.$oTemplateCurrent->img.'">';
            }else{
                echo'<img src="http://placehold.it/401x188">' ;
            }?>
        </div>
    </div>
    <?php else:?>
        <?=\Yii::t('app','Вы не выбрали еще шаблон.')?>
    <?php endif;?>
</div>

<div class="row">
    <div class="col-md-12 text-center">
        <h3><?=\Yii::t('app','Вы можете выбрать любой другой')?><br>
            <i class="fa fa-arrow-down"></i>
        </h3>

    </div>
</div>

<div class="col-md-12">
    <div class="dividerLine"></div>
</div>

<!------------------ Бесплатные шаблоны ------------------->

<div class="row">
    <div class="col-md-12">
        <h3><?=\Yii::t('app','Мои шаблоны')?></h3>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?php if(count($oTemplatesUser)):?>
            <ul>
                <?php foreach( $oTemplatesUser as $oTemplate ):?>
                <?php if(!$oTemplate->userThemes->is_current):?>

                    <?= $this->render('_my_templates', ['oTemplate'=>$oTemplate]) ?>

                <?php endif;?>
                <?php endforeach;?>
            </ul>
        <?php else:?>
            <p><?=\Yii::t('app','Вы еще не купили ни одного шаблона.')?></p>
        <?php endif;?>
    </div>
</div>

<div class="col-md-12">
    <div class="dividerLine"></div>
</div>

<!------------------ Платные шаблоны ------------------->

<div class="row">
    <div class="col-md-12">
        <h3><?=\Yii::t('app','PRO шаблоны')?></h3>
    </div>
</div>

<!--<div class="row proTemplList">-->
<!--    <div class="col-md-12">-->
<!--        <ul>-->
<!--            <li>-->
<!--                <button class="btn btn-primary btn-xs" type="button">-->
<!--                    --><?//=\Yii::t('app','Фотограф')?>
<!--                    <span class="badge">4</span>-->
<!--                </button>-->
<!--            </li>-->
<!--            <li>-->
<!--                <button class="btn btn-primary btn-xs" type="button">-->
<!--                    --><?//=\Yii::t('app','Дизайнер инерьеров')?>
<!--                    <span class="badge">12</span>-->
<!--                </button>-->
<!--            </li>-->
<!--            <li>-->
<!--                <button class="btn btn-primary btn-xs" type="button">-->
<!--                    --><?//=\Yii::t('app','Веб-дизайнер')?>
<!--                    <span class="badge">30</span>-->
<!--                </button>-->
<!--            </li>-->
<!--            <li>-->
<!--                <button class="btn btn-primary btn-xs" type="button">-->
<!--                    --><?//=\Yii::t('app','3d моделлер')?>
<!--                    <span class="badge">4</span>-->
<!--                </button>-->
<!--            </li>-->
<!--            <li>-->
<!--                <button class="btn btn-primary btn-xs" type="button">-->
<!--                    --><?//=\Yii::t('app','Фотограф')?>
<!--                    <span class="badge">4</span>-->
<!--                </button>-->
<!--            </li>-->
<!--            <li>-->
<!--                <button class="btn btn-primary btn-xs" type="button">-->
<!--                    --><?//=\Yii::t('app','Дизайнер инерьеров')?>
<!--                    <span class="badge">12</span>-->
<!--                </button>-->
<!--            </li>-->
<!--            <li>-->
<!--                <button class="btn btn-primary btn-xs" type="button">-->
<!--                    --><?//=\Yii::t('app','Веб-дизайнер')?>
<!--                    <span class="badge">30</span>-->
<!--                </button>-->
<!--            </li>-->
<!--            <li>-->
<!--                <button class="btn btn-primary btn-xs" type="button">-->
<!--                    --><?//=\Yii::t('app','3d моделлер')?>
<!--                    <span class="badge">4</span>-->
<!--                </button>-->
<!--            </li>-->
<!--            <li>-->
<!--                <button class="btn btn-primary btn-xs" type="button">-->
<!--                    --><?//=\Yii::t('app','Дизайнер инерьеров')?>
<!--                    <span class="badge">12</span>-->
<!--                </button>-->
<!--            </li>-->
<!--        </ul>-->
<!--    </div>-->
<!--</div>-->


<div class="row">
    <div class="col-md-12 proTempl">
        <?php if(count($oTemplates)):?>
        <ul>
            <?php foreach( $oTemplates as $oTemplate ):?>
                <?= $this->render('_pro_templates', ['oTemplate'=>$oTemplate]) ?>
            <?php endforeach;?>
        </ul>
        <?php else:?>
        <p><?=\Yii::t('app','Нет шаблонов для покупки.')?></p>
        <?php endif;?>
    </div>
</div>

</div> <!-- accound_register -->
</div>
</div> <!-- accound_form -->
</div> <!-- accountDetails -->
</div> <!-- accoundContent -->
</section>
