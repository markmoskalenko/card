<?php
use yii\helpers\Url;

?>

<li class="col-sm-6 col-md-6">
    <div class="thumbnail">
        <?php if($oTemplate->img != NULL){
            echo '<img src="/upload/theme/200x200/'.$oTemplate->img.'">';
        }else{
            echo'<img src="http://placehold.it/401x188">' ;
        }?>
        <div class="btnWrap">
            <a target="_blank" href="<?= Url::to(['go/view', 'template'=>$oTemplate->id]) ?>" class="btn btn-success btn-sm"><i class="fa fa-eye"></i> <?=\Yii::t('app','Просмотр')?></a>
            <div class="btn btn-danger btn-sm">
                <?php if($oTemplate->cost !=0):?>
                <i class="fa fa-check-square-o"></i>  <a href="<?= Url::to(['template/buy', 'id'=>$oTemplate->id]) ?>" class="buy"><?=\Yii::t('app','Купить')?> <?= $oTemplate->cost ?></a><i class="fa fa-rub"></i>
                <?php else:?>
                <i class="fa fa-check-square-o"></i>  <a href="<?= Url::to(['template/add', 'id'=>$oTemplate->id]) ?>" class="buy"><?=\Yii::t('app','Добавить в мои шаблоны')?></a></i>
                <?php endif;?>
            </div>
        </div>
    </div>
</li>


