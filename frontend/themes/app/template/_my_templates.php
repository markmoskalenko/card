<?php
/**
 * Created by PhpStorm.
 * User: asus
 * Date: 18.08.14
 * Time: 9:35
 */

?>


<li class="col-sm-6 col-md-6">
    <div class="thumbnail">
        <div class="badges">   <!-- бейджик для купленных шаблонов -->
            <div class="badg style2 badge-orange"></div>
        </div>
        <?php if($oTemplate->img != NULL){
            echo '<img src="/upload/theme/200x200/'.$oTemplate->img.'">';
        }else{
           echo'<img src="http://placehold.it/401x188">' ;
        }?>
        <div class="btnWrap">
            <a target="_blank" href="<?= \yii\helpers\Url::to(['go/view', 'template'=>$oTemplate->id]) ?>" class="btn btn-success btn-sm"><i class="fa fa-eye"></i> <?=\Yii::t('app','Просмотр')?></a>
            <?php if($oTemplate->userThemes->is_current):?>
                <a href="javascript:void(0)" class="btn btn-black btn-sm">
                    <i class="fa fa-check-square-o"></i> <?=\Yii::t('app','Тема уже установлена')?>
                </a>
            <?php else:?>
                <a href="<?= \yii\helpers\Url::to(['template/set-current', 'id'=>$oTemplate->id]) ?>" class="btn btn-black btn-sm">
                    <i class="fa fa-check-square-o"></i> <?=\Yii::t('app','Применить')?>
                </a>
            <?php endif;?>
        </div>
    </div>
</li>