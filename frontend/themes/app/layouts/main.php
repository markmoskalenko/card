<?php
use yii\helpers\Html;

use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use frontend\widgets\Alert;
use frontend\widgets\Lang;

/**
 * @var \yii\web\View $this
 * @var string $content
 */
AppAsset::register($this);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>

    <div class="wrap">

        <?=$this->render('//partials/_menu'); ?>

        <?php if( !Yii::$app->user->isGuest && $this->isVisibleHeader3 ): ?>
            <?= $this->render('//partials/_header', compact('oUser')); ?>
        <?php endif;?>

        <?php if( !Yii::$app->user->isGuest && !empty( $this->message ) ): ?>
           <p style="text-align: center"> <?= $this->message; ?> </p>
        <?php endif;?>

        <?= $content ?>
    </div>

    <footer>
        <div class="container fixed-bottom">
            <div id="intro_page_footer" class="row">
                <div class="col-sm-12 text-center">
                    <div id="intro_footer_copy">
                        <p>
                            Myres &copy; <?= date('Y'); ?>
                            <br/>
                            <a href="http://it-yes.com" target="_blank" style="color: #7a7a7a">Разработка сайтов - it-yes.com</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
