<?php
use frontend\assets\ProfileAsset;
use yii\helpers\Html;

ProfileAsset::register($this);

/**
 * @var frontend\controllers\ProfileController $this
 * @var yii\widgets\ActiveForm $form
 * @var \common\models\User $model
 */

$this->title = $model->title;
$this->registerMetaTag([
    'name' => 'description',
    'content' => $model->meta_d,
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $model->meta_k,
]);

?>

<div class="site-<?= $model->slug ?> container">
<div class="row">
    <div class="col-lg-12">
        <h1><?= Html::encode($this->title) ?></h1>
        <hr/>
        <?= $model->content ?>

    </div>
</div>

</div>
