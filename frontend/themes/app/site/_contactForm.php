<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var \common\models\User $oUserContact
 * @var \common\models\Country $oCountry
 * @var \common\models\City $oCity
 */
?>


<div class="row">
    <div id="contact" class="col-md-12">
        <div class="ajax">
<?php $form = ActiveForm::begin([
    'id' => 'contact-form',
    'class' =>'row',
    'enableClientValidation' => false,
    'enableAjaxValidation' => true
]); ?>

        <h3><?=\Yii::t('app','Контакты')?></h3>
        <div class="col-md-12 formGray">
            <div class="col-md-6">
                <?= $form->field($oUserContact, 'country_id')->dropDownList( $oCountry, ['prompt'=> \Yii::t('app','Выберите страну'), 'class'=>'sl2', 'style'=>'width: 100%;'] ) ?>
                <?= $form->field($oUserContact, 'city_id')->dropDownList( $oCity, ['prompt'=>\Yii::t('app','Выберите город'),  'class'=>'sl2', 'style'=>'width: 100%;'] ) ?>
                <?= $form->field($oUserContact, 'my_site')->textInput( ['placeholder'=> \Yii::t('app','Мой сайт (при наличии)')] ) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($oUserContact, 'phone')->textInput( ['placeholder'=>\Yii::t('app','Телефон')] ) ?>
                <?= $form->field($oUserContact, 'phone_mob')->textInput( ['placeholder'=> \Yii::t('app','Мобильный')] ) ?>
                <?= $form->field($oUserContact, 'validateScenario')->hiddenInput(['value'=>'contact']);  ?>
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-2 pull-right">
                <div class="btnAddRemove">
                    <?= Html::button(\Yii::t('app','  Сохранить'), ['class' => 'btn btn-primary fa fa-check-square-o submit', 'name' => 'save-button']) ?>
                </div>
            </div>
        </div>
<?php ActiveForm::end(); ?>
</div>
</div>
</div>
