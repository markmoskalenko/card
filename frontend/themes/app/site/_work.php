<div class="row" id="work-experience">
    <div class="col-md-12 accoundNameSection">
        <h3><?=\Yii::t('app','Опыт работы')?></h3>
    </div>
    <div class="col-md-12" ng-controller="WorkCtrl">
        <button type="submit" class="btn btn-primary" name="save-button" ng-click="add()"><?=\Yii::t('app','Добавить')?></button>
        <br/>
        <br/>
        <table class="table table-bordered">
            <tbody>
            <tr ng-repeat="work in works">
                <td class="firstColum text-center">
                    <div class="checklist inline">
                        <div rel="tooltip" title="Опубликовать" data-placement="top" class="rowCheckbox">
                            <input  ng-checked="{{work.is_public}}" data-id="{{work.id}}" data-value="{{work.is_public}}" id="" value="" ng-click="pub($event)" type="checkbox">
                        </div>
                    </div>
                    <div class="editDate">
                        <a href="javascript:void(0)" rel="tooltip" title="Редактировать" data-placement="top" data-id="{{work.id}}" ng-click="edit($event)"><i class="fa fa-pencil"></i></a>
                    </div>
                    <div class="editDate">
                        <a href="javascript:void(0)" rel="tooltip" title="Удалить" data-placement="top" data-id="{{work.id}}" ng-click="delete($event)"><i class="fa fa-trash-o"></i></a>
                    </div>
                </td>
                <td>
                    <ul>
                        <li>
                            <span>{{work.start_date}} - {{work.end_date}}</span>
                            <p>{{work.country}} г.{{work.city}}</p>
                            <p><strong>{{work.company}}</strong></p>
                            <p><strong>{{work.position}}</strong></p>
                            <p>{{work.comment}}</p>
                        </li>
                    </ul>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
