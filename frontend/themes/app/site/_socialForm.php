<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var \common\models\User $oUserSocial
 */
?>
<div class="row">
    <div class="col-md-12">

<div class="ajax">
<?php $form = ActiveForm::begin([
    'id' => 'social-form',
    'class' =>'row ',
    'enableClientValidation' => false,
    'enableAjaxValidation' => true,
]); ?>
        <h3><?=\Yii::t('app','Мои социальные сети')?></h3>

        <div class="col-md-12 formGray">
            <div class="col-md-6">
                <?= $form->field($oUserSocial, 's_fb')->textInput(array('placeholder' => 'facebook.com')) ?>
                <?= $form->field($oUserSocial, 's_tw')->textInput(array('placeholder' => 'twitter.com')) ?>
                <?= $form->field($oUserSocial, 'flickr')->textInput(array('placeholder' => 'flickr.com')) ?>
                <?= $form->field($oUserSocial, 's_gplus')->textInput(array('placeholder' => 'plus.google.com')) ?>
            </div>

            <div class="col-md-6">
                <?= $form->field($oUserSocial, 's_vk')->textInput(array('placeholder' => 'vk.com')) ?>
                <?= $form->field($oUserSocial, 's_git')->textInput(array('placeholder' => 'github.com')) ?>
                <?= $form->field($oUserSocial, 'linkedin')->textInput(array('placeholder' => 'linkedin.com')) ?>
                <?= $form->field($oUserSocial, 's_mail')->textInput(array('placeholder' => 'moikrug.ru')) ?>
                <?= $form->field($oUserSocial, 'validateScenario')->hiddenInput(['value'=>'social']);  ?>
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-2 pull-right">
                <div class="btnAddRemove">
                    <?= Html::button(\Yii::t('app','  Сохранить'), ['class' => 'submit btn btn-primary fa fa-check-square-o', 'name' => 'save-button']) ?>
                </div>
            </div>
        </div>
<?php ActiveForm::end(); ?>
    </div>
</div>
</div>