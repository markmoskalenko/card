<div class="row" id="interests">
    <div class="col-md-12 accoundNameSection">
        <h3><?=\Yii::t('app','Интересы')?></h3>
    </div>
    <div class="col-md-12" ng-controller="InterestsCtrl">
        <button type="submit" class="btn btn-primary" name="save-button" ng-click="add()"><?=\Yii::t('app','Добавить')?></button>
        <br/>
        <br/>
        <table class="table table-bordered">
            <tbody>
            <tr ng-repeat="interest in interests">
                <td class="firstColum text-center">
                    <div class="checklist inline">
                        <div rel="tooltip" title="Опубликовать" data-placement="top" class="rowCheckbox">
                            <input  ng-checked="{{interest.is_public}}" data-id="{{interest.id}}" data-value="{{interest.is_public}}" id="" value="" ng-click="pub($event)" type="checkbox">
                        </div>
                    </div>
                    <div class="editDate">
                        <a href="javascript:void(0)" rel="tooltip" title="Редактировать" data-placement="top" data-id="{{interest.id}}" ng-click="edit($event)"><i class="fa fa-pencil"></i></a>
                    </div>
                    <div class="editDate">
                        <a href="javascript:void(0)" rel="tooltip" title="Удалить" data-placement="top" data-id="{{interest.id}}" ng-click="delete($event)"><i class="fa fa-trash-o"></i></a>
                    </div>
                </td>
                <td>
                    <ul>
                        <li>
                            <p><strong>{{interest.name}}</strong></p>
                            <p>{{interest.comment}}</p>
                        </li>
                    </ul>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>