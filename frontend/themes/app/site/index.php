<?php
use frontend\assets\ProfileAsset;

ProfileAsset::register($this);

/**
 * @var frontend\controllers\ProfileController $this
 * @var yii\widgets\ActiveForm $form
 * @var \common\models\User $model
 */
$this->title = \Yii::t('app','Мои данные');
?>

<section class="accound">

    <div class="accoundContent" ng-app="profileApp">
        <div id="accountDetails" class="container">
            <div id="sidebar" class="userNavleft"><?= $this->render('//partials/_profileMenu'); ?></div>

            <div id="accound_form" class="row">

                <div class="col-md-10 col-md-offset-2">

                    <div class="accoundForm_1 accound_register">

                        <?= $this->render('_aboutForm', compact('oUserAbout')); ?>

                        <div class="row">
                            <div class="dividerLine"></div>
                        </div>

                        <?= $this->render('_changePassword'); ?>

                        <div class="row">
                            <div class="dividerLine"></div>
                        </div>

                        <?= $this->render('_work'); ?>

                        <div class="row">
                            <div class="dividerLine"></div>
                        </div>

                        <?= $this->render('_education'); ?>

                        <div class="row">
                            <div class="dividerLine"></div>
                        </div>

                        <?= $this->render('_skills'); ?>
                        <div class="row">
                            <div class="dividerLine"></div>
                        </div>
                        <?= $this->render('_interests'); ?>
                        <div class="row">
                            <div class="dividerLine"></div>
                        </div>

                        <?= $this->render('_contactForm', compact('oUserContact', 'oCountry', 'oCity')); ?>

                        <?= $this->render('_socialForm', compact('oUserSocial')); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>