<div class="row" id="education">
    <div class="col-md-12 accoundNameSection">
        <h3><?=\Yii::t('app','Образование')?></h3>
    </div>

    <div class="col-md-12" ng-controller="EducationCtrl">
        <button type="submit" class="btn btn-primary" name="save-button" ng-click="add()"><?=\Yii::t('app','Добавить')?></button>
        <br/>
        <br/>
        <table class="table table-bordered">
            <tbody>
            <tr ng-repeat="education in educations">
                <td class="firstColum text-center">
                    <div class="checklist inline">
                        <div rel="tooltip" title="Опубликовать" data-placement="top" class="rowCheckbox">
                            <input  ng-checked="{{education.is_public}}" data-id="{{education.id}}" data-value="{{education.is_public}}" id="" value="" ng-click="pub($event)" type="checkbox">
                        </div>
                    </div>
                    <div class="editDate">
                        <a href="javascript:void(0)" rel="tooltip" title="Редактировать" data-placement="top" data-id="{{education.id}}" ng-click="edit($event)"><i class="fa fa-pencil"></i></a>
                    </div>
                    <div class="editDate">
                        <a href="javascript:void(0)" rel="tooltip" title="Удалить" data-placement="top" data-id="{{education.id}}" ng-click="delete($event)"><i class="fa fa-trash-o"></i></a>
                    </div>
                </td>
                <td>
                    <ul>
                        <li>
                            <span>{{education.start_date}} - {{education.end_date}}</span>
                            <p>{{education.country}} г.{{education.city}}</p>
                            <p><strong>{{education.company}}</strong></p>
                            <p><strong>{{education.position}}</strong></p>
                            <p>{{education.comment}}</p>
                        </li>
                    </ul>
                </td>
            </tr>
            </tbody>
        </table>
    </div>

</div>