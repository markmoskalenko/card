<div class="row" id="professional-skills">
    <div class="col-md-12 accoundNameSection">
        <h3><?=\Yii::t('app','Профессиональные навыки')?></h3>
    </div>

    <div class="col-md-12"  ng-controller="SkillCtrl">
        <button type="submit" class="btn btn-primary" name="save-button" ng-click="add()"><?=\Yii::t('app','Добавить')?></button>
        <br/>
        <br/>
        <table class="table table-bordered">
            <tbody>
            <tr ng-repeat="skill in skills">
                <td class="firstColum text-center">
                    <div class="checklist inline">
                        <div rel="tooltip" title="Опубликовать" data-placement="top" class="rowCheckbox">
                            <input  ng-checked="{{skill.is_public}}" data-id="{{skill.id}}" data-value="{{skill.is_public}}" id="" value="" ng-click="pub($event)" type="checkbox">
                        </div>
                    </div>
                    <div class="editDate">
                        <a href="javascript:void(0)" rel="tooltip" title="Редактировать" data-placement="top" data-id="{{skill.id}}" ng-click="edit($event)"><i class="fa fa-pencil"></i></a>
                    </div>
                    <div class="editDate">
                        <a href="javascript:void(0)" rel="tooltip" title="Удалить" data-placement="top" data-id="{{skill.id}}" ng-click="delete($event)"><i class="fa fa-trash-o"></i></a>
                    </div>
                </td>
                <td>
                    <ul>
                        <li>
                            <p><strong>{{skill.name}}</strong></p>
                            <div class="progress progress-striped active">
                                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="{{skill.percent}}" aria-valuemin="0" aria-valuemax="100" style="width: {{skill.percent}}%">
                                </div>
                            </div>
                        </li>
                    </ul>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>