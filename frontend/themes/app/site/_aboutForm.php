<?php
use frontend\components\DateHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var \common\models\User $oUserAbout
 */
?>
<div class="ajax">
<div class="row">
<?php $form = ActiveForm::begin([
    'id' => 'profile-form',
    'enableClientValidation' => false,
    'enableAjaxValidation' => true,
]); ?>
<!--    <div id="crop_avatar" style="display: none; text-align: center; width: 300px">-->
<!--        <img id="crop_avatar_image" src="" alt=""/>-->
<!--        <div class="clearfix"></div>-->
<!--        <a href="javascript:void(0)" style="margin: 20px 0" class="btn btn-primary fa fa-check-square-o" id="delselect">Сохранить</a>-->
<!--    </div>-->

    <div class="col-md-12">
        <div class="col-md-12">
            <h3><?=\Yii::t('app','Профиль')?></h3>
        </div>

        <div class="col-md-12 formGray">

            <div class="row">
                <div class="col-md-7">
                    <div class="userPic">
                        <div class="img-container">
                            <img src="<?= $oUserAbout->getUrlAvatar() ?>" id="target" alt="[Jcrop Example]" />
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="userPicText">
                        <p><em><?=\Yii::t('app','Добавьте Ваше фото. Оно так-же будет размещено и на Вашем сайте')?></em></p>
                    </div>
                    <div class="btnAddRemove">
                        <?= $this->render('_uploadForm', compact('oUserAbout')); ?>
                    </div>
                </div>
            </div>

            <div class="col-md-7 col-md-offset-2 siteName">
                <div class="input-group" style="width: 380px">
                    <?= $form->field($oUserAbout, 'subdomain')->textInput(array('placeholder' =>  \Yii::t('app','Адрес сайта')));  ?>
                </div><div class="input-subdomian">.myres.pro</div>
            </div>

            <div class="col-md-6">
                <div class="col-md-12">
                    <?= $form->field($oUserAbout, 'username')->textInput(array('placeholder' => \Yii::t('app','Имя'))) ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($oUserAbout, 'surname')->textInput(array('placeholder' => \Yii::t('app','Фамилия'))) ?>
                </div>


                <div class="col-md-12">
                    <p><?=\Yii::t('app','Дата рождения')?></p>
                </div>

                <div class="col-md-4">
                    <?= $form->field($oUserAbout, 'date_of_birth_day')->dropDownList(DateHelper::getDays()) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($oUserAbout, 'date_of_birth_month')->dropDownList(DateHelper::getMonth()) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($oUserAbout, 'date_of_birth_year')->dropDownList(DateHelper::getYears())  ?>
                </div>
                <div class="col-md-12">
                    <div class="btnAddRemove">
                        <?= Html::button( \Yii::t('app','  Сохранить') , ['class' => 'submit refresh btn btn-primary fa fa-check-square-o', 'name' => 'save-button']) ?>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div id="input_email" class="col-md-12">
                    <?= $form->field($oUserAbout, 'email')->textInput(array('disabled'=>'disabled', 'placeholder' => \Yii::t('app','Адрес электронной почты'))) ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($oUserAbout, 'profession')->textInput(array('placeholder' => \Yii::t('app','Профессия'))) ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($oUserAbout, 's_status_title')->textarea(['rows' => '3', 'placeholder' => \Yii::t('app','Моё кредо')]) ?>
                    <?= $form->field($oUserAbout, 'validateScenario')->hiddenInput(['value'=>'profile']);  ?>
                </div>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>
</div>
</div>