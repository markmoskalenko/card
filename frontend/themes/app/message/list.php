<?php
use yii\helpers\Url;
use common\models\Message;

$this->title = 'Сообщения';

/* @var $this yii\web\View */
/* @var $searchModel common\models\Invoice */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>

<section class="invoice">
    <div class="accoundContent" ng-app="profileApp">
        <div id="accountDetails" class="container">
            <div id="sidebar" class="userNavleft"><?= $this->render('//partials/_profileMenu'); ?></div>
            <div class="row">
                <div class="col-md-10 col-md-offset-2">
                    <div class="accoundForm_1 accound_register">

                        <div class="row messages">
                            <div class="col-md-12 col-sm-12 col-xs-12 countMessages">
                                <div class="mess"><a href="<?= Url::to(['message/list', 'category' => Message::CATEGORY_MAIN]) ?>"><?=\Yii::t('app','Общие')?><span><?= $counts[Message::CATEGORY_MAIN] ?></span></a></div>
                                <div class="mess"><a href="<?= Url::to(['message/list', 'category' => Message::CATEGORY_MYRES]) ?>"><?=\Yii::t('app','Myres')?><span><?= $counts[Message::CATEGORY_MYRES] ?></span></a></div>
                                <div class="mess"><a href="<?= Url::to(['message/list', 'category' => Message::CATEGORY_ARCHIVE]) ?>"><?=\Yii::t('app','Архив')?><span><?= $counts[Message::CATEGORY_ARCHIVE] ?></span></a></div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="dividerLine"></div>
                            </div>

                            <?php foreach ($messages as $message): ?>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <ul>
                                    <li class="row">
                                        <div class="messagesDate text-right">
                                            <p><?= $message->date ?></p>
                                        </div>
                                        <div class="col-md-1 col-sm-1 col-xs-2 text-center firstLetter">
                                            <p>L</p>
                                        </div>
                                        <div class="col-md-11 col-sm-11 col-xs-9">
                                            <div class="messagesName">
                                                <h3><?= $message->name ?></h3>
                                            </div>
                                            <div class="messagesMail">
                                                <p><?= $message->email ?></p>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12 messagesText">
                                            <p><?= $message->text ?></p>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">

                                <div class="btnAddRemove">
                                    <?php if ($category != \common\models\Message::CATEGORY_ARCHIVE): ?>
                                        <a href="<?= Url::to(['message/archive', 'id' => $message->id]) ?>" class="btn btn-primary" data-toggle="" data-target=""><i class="fa fa-tasks"></i>&nbsp;&nbsp;<?=\Yii::t('app','В архив')?></a>
                                    <?php endif ?>
                                        <a href="<?= Url::to(['message/delete', 'id' => $message->id]) ?>" class="btn btn-primary" data-toggle="" data-target=""><i class="fa fa-trash-o"></i>&nbsp;&nbsp;<?=\Yii::t('app','Удалить')?></a>
                                </div>
                            </div>
                            <?php endforeach;?>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</section>