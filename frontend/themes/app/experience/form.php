<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use frontend\assets\SelectAsset;

SelectAsset::register($this);
/**
 * @var \common\models\Experience $oExperience
 */
?>

<?php $form = ActiveForm::begin(['id' => 'experience-form','class' =>'row', 'enableClientValidation'=>false]); ?>
    <div class="col-md-12">

        <div class="col-md-12">
            <h2><?= \Yii::t('app', 'Опыт работы') ?></h2>
            <hr/>
            <div class="col-md-6">
                <div class="form-group <?php if($oExperience->hasErrors('start_date') || $oExperience->hasErrors('end_date') ) echo 'has-error';?>">
                    <?= Html::activeLabel($oExperience, 'dates', ['class'=>'control-label']) ?>
                    <?= kartik\widgets\DatePicker::widget([
                        'type' => \kartik\widgets\DatePicker::TYPE_RANGE,
                        'pluginOptions' => ['format' => 'yyyy-mm-dd'],
                        'model' => $oExperience,
                        'attribute' => 'start_date',
                        'attribute2' => 'end_date',
                        'separator' => '-'

                    ]) ?>
                    <?= Html::error($oExperience, 'start_date', ['class'=>'help-block']) ?>
                    <?= Html::error($oExperience, 'end_date', ['class'=>'help-block']) ?>
                </div>

                <div class="form-group <?= $oExperience->hasErrors('start_date') ? 'has-error' : '';?>">
                    <?= Html::activeLabel($oExperience, 'company_name', ['class'=>'control-label']) ?>
                    <?= \yii\jui\AutoComplete::widget([
                          'model' => $oExperience,
                          'attribute' => 'company_name',
                          'clientOptions' => [
                              'source' => $aCompany,
                          ],
                        'options'=>['class'=>'form-control']
                        ]);?>
                    <?= Html::error($oExperience, 'company_name', ['class'=>'help-block']) ?>
                </div>

                <div class="form-group <?= $oExperience->hasErrors('start_date') ? 'has-error' : '';?>">
                    <?= Html::activeLabel($oExperience, 'position_name', ['class'=>'control-label']) ?>
                    <?= \yii\jui\AutoComplete::widget([
                        'model' => $oExperience,
                        'attribute' => 'position_name',
                        'clientOptions' => [
                            'source' => $aPosition,
                        ],
                        'options'=>['class'=>'form-control']

                    ]);?>
                    <?= Html::error($oExperience, 'position_name', ['class'=>'help-block']) ?>
                </div>

                <?= $form->field($oExperience, 'country_id')->dropDownList($oCountry,['id'=>'country_id', 'class'=>'sl2', 'style'=>'width: 100%;']) ?>
                <?= $form->field($oExperience, 'city_id')->dropDownList($oCity,['id'=>'city_id', 'class'=>'sl2', 'style'=>'width: 100%;']) ?>
                <?= $form->field($oExperience, 'comment')->textarea() ?>
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-2 pull-right">
                <div class="btnAddRemove">
                    <?= Html::submitButton(\Yii::t('app','  Сохранить'), ['class' => 'btn btn-primary fa fa-check-square-o', 'name' => 'save-button']) ?>
                </div>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>