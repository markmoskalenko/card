<?php
    use \common\models\User;
    use yii\helpers\Url;
?>
<div id="profile" class="container-fluid userWide">
    <div class="row">
        <div class="col-md-2 userPhoto">
            <img src="<?= User::u()->getUrlAvatar()?>" class="img-responsive">
        </div>
        <div class="col-md-4 userEmail">
            <h1><?=User::u()->email?><!-- Имя --><br>
                <!-- Фамилия -->
            </h1>
        </div>
        <div class="col-md-2 rt">
            <div class="rttext"><?= \Yii::t('app', 'Баланс') ?>:  <a class="rticon"><i class="fa fa-question-circle"></i></a> </div>
            <div class="rtcount"><?= (float) User::u()->balance?></div>
        </div>
        <div class="col-md-2 rt">
            <div class="rttext"> <?=\Yii::t('app','Рейтинг')?>:
                <a id="go" class="rticon"><i class="fa fa-question-circle"></i></a>
                <div id="modal_form">
                    <div class="ratingsExplained">
                        <h4 class="text-center"><?=\Yii::t('app','Рейтинг')?></h4>
                        <ul>
                            <li>
                                <?=\Yii::t('app','Бонус при регистрации')?>:<div class="statusBarCount">0</div>
                            </li>
                            <li>
                                1 <?=\Yii::t('app','комментарий')?>комментарий:<div class="statusBarCount">+2</div>
                            </li>
                            <li>
                                <?=\Yii::t('app','Бонус при регистрации')?>:<div class="statusBarCount">0</div>
                            </li>
                            <li>
                                <?=\Yii::t('app','Бонус при регистрации')?>:<div class="statusBarCount">0</div>
                            </li>
                        </ul>
                        <div class="modalClose text-center">
                            <button id="modal_close" class="btn btn-primary" type="button">Ok</button>
                        </div>
                    </div>
                </div>
                <div id="overlay"></div>
            </div>

            <div class="rtcount">250</div>
        </div>
        <div class="col-md-2 statusBar">
            <ul>
                <li><a href="#"><i class="fa fa-eye"></i>&nbsp;<?=\Yii::t('app','Просмотров')?>:</a>
                    <div class="statusBarCount">0</div>
                </li>
                <li><a href="#"><i class="fa fa-heart"></i>&nbsp;<?=\Yii::t('app','Лайки')?>:</a>
                    <div class="statusBarCount">0</div>
                </li>
                <li><a href="<?= Url::to('/message/list') ?>"><i class="fa fa-comment"></i>&nbsp;<?=\Yii::t('app','Сообщения')?>:</a>
                    <div class="statusBarCount"><?= \common\models\Message::getNewCount(); ?></div>
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="container userHeader">
    <div class="row">
        <div class="col-md-4">
            <h2><?=\Yii::t('app',$this->title)?></h2>
        </div>
        <div class="col-md-8" >
            <div class="viewInSite">
                <?php if( empty( User::u()->subdomain ) || User::u()->currentTheme == null ): ?>
                    <p style="padding: 20px; color: red"><?=\Yii::t('app','Укажите адрес сайта и выберите тему для внешнего вида')?></p>
                <?php else: ?>
                    <a target="_blank" class="btn btn-primary pull-right" href="<?= Yii::$app->params['scheme'].User::u()->subdomain.'.'.Yii::$app->params['baseDomain'] ?>"><i class="fa fa-eye"></i>&nbsp;&nbsp;<?=\Yii::t('app','Просмотр сайта')?></a>
                <?php endif;?>
                <?php if( !empty( User::u()->activate_key ) && !empty( User::u()->email )): ?>
                    <p style="padding: 20px; color: red"><?=\Yii::t('app','Вам выслано электронное письмо, активируйте почту..')?></p>
                <?php endif;?>
            </div>
        </div>
    </div>
</div>