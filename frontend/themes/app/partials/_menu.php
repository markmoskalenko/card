<?php
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Html;

NavBar::begin([
    'brandLabel' => Html::img('/themes/app/img/logo_2.png'),
    'brandUrl' => Yii::$app->homeUrl,
    'options' => [
        'class' => 'navbar intro',
        'id'=>'header'
    ],
]);

$menuItems = [
    ['label' => \Yii::t('app', 'Как это работает'), 'url' => ['page/view', 'slug'=>'how-it-works']],
];

if (Yii::$app->user->isGuest) {
    $menuItems[] = ['label' => \Yii::t('app', 'Войти'), 'url' => ['/user/login']];
} else {
    $menuItems[] = ['label' => \Yii::t('app','Мой кабинет'),
        'items' => [

            ['label' => \Yii::t('app','Мои данные'), 'url' => '#'],
            ['label' => \Yii::t('app','Сообщения'), 'url' => ['message/list']],
            ['label' => \Yii::t('app','Пополнить балланс'), 'url' => ['/payment/invoice']],
            '<li class="divider"></li>',
            ['label' => \Yii::t('app','Выход'), 'url' => '/user/logout'],
        ],
        'visible' => !Yii::$app->user->isGuest,
        'class' => 'dropdown-menu'
    ];
}

$menuItems[] = ['label'=>\frontend\widgets\Lang::widget()];

echo Nav::widget([
    'options' => ['class' => 'navbar-nav intro navbar-right'],
    'items' => $menuItems,
    'encodeLabels' => false,


]);
NavBar::end();
?>