<?php use \yii\helpers\Url;?>
<nav class="hidden-sm hidden-xs">
    <ul>
        <li><a href="<?= Url::to(['template/index']) ?>"><?=\Yii::t('app','Дизайн сайта')?></a></li>
        <li class="divider"></li>
        <li><a href="<?= Url::to(['site/index#profile']) ?>"><?=\Yii::t('app','Профиль')?></a></li>
        <li><a href="<?= Url::to(['site/index#work-experience']) ?>"><?=\Yii::t('app','Опыт работы')?></a></li>
        <li><a href="<?= Url::to(['site/index#education']) ?>"><?=\Yii::t('app','Образование')?> </a></li>
        <li><a href="<?= Url::to(['site/index#professional-skills']) ?>"><?=\Yii::t('app','Навыки')?> </a></li>
        <li><a href="<?= Url::to(['site/index#interests']) ?>"><?=\Yii::t('app','Интересы')?></a></li>
        <li><a href="<?= Url::to(['portfolio/index']) ?>"><?=\Yii::t('app','Портфолио')?></a></li>
        <li><a href="<?= Url::to(['site/index#contact']) ?>"><?=\Yii::t('app','Контакты')?> </a></li>
        <li><a href="<?= Url::to(['payment/list']) ?>"><?=\Yii::t('app','Ст-ка расходов')?> </a></li>
        <li class="divider"></li>
        <li><a href=""><?=\Yii::t('app','Сообщения')?></a></li>
        <li class="divider"></li>
        <li><a href=""><?=\Yii::t('app','Удалить аккаунт')?> </a></li>
    </ul>
</nav>

<!-- Navigation for small devices -->
<nav id="mini-nav" class="visible-sm visible-xs">
    <ul>
        <li class="dropdown"> <a class="dropdown-toggle" data-toggle="dropdown">Меню пользователя<span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="<?= Url::to(['template/index']) ?>"><?=\Yii::t('app','Дизайн сайта')?></a></li>
                <li class="divider"></li>
                <li><a href="<?= Url::to(['site/index#profile']) ?>"><?=\Yii::t('app','Профиль')?></a></li>
                <li><a href="<?= Url::to(['site/index#work-experience']) ?>"><?=\Yii::t('app','Опыт работы')?></a></li>
                <li><a href="<?= Url::to(['site/index#education']) ?>"><?=\Yii::t('app','Образование')?> </a></li>
                <li><a href="<?= Url::to(['site/index#professional-skills']) ?>"><?=\Yii::t('app','Навыки')?> </a></li>
                <li><a href="<?= Url::to(['site/index#interests']) ?>"><?=\Yii::t('app','Интересы')?></a></li>
                <li><a href="<?= Url::to(['portfolio/index']) ?>"><?=\Yii::t('app','Портфолио')?></a></li>
                <li><a href="<?= Url::to(['site/index#contact']) ?>"><?=\Yii::t('app','Контакты')?> </a></li>
                <li><a href="<?= Url::to(['payment/list']) ?>"><?=\Yii::t('app','Ст-ка расходов')?> </a></li>
                <li class="divider"></li>
                <li><a href=""><?=\Yii::t('app','Сообщения')?></a></li>
                <li class="divider"></li>
                <li><a href=""><?=\Yii::t('app','Удалить аккаунт')?> </a></li>
            </ul>
        </li>
    </ul>
</nav>
<!-- end Navigation for small devices -->
