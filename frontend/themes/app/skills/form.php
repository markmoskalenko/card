<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use frontend\assets\SelectAsset;

SelectAsset::register($this);
/**
 * @var \common\models\Experience $oSkills
 */
?>

<?php $form = ActiveForm::begin(['id' => 'experience-form','class' =>'row', 'enableClientValidation'=>false]); ?>
    <div class="col-md-12">

        <div class="col-md-12">
            <h2><?= \Yii::t('app', 'Профессиональные навыки') ?></h2>
            <hr/>
            <div class="col-md-6">

                <div class="form-group <?= $oSkills->hasErrors('skill_name') ? 'has-error' : '';?>">
                    <?= Html::activeLabel($oSkills, 'skill_name', ['class'=>'control-label']) ?>
                    <?= \yii\jui\AutoComplete::widget([
                        'model' => $oSkills,
                        'attribute' => 'skill_name',
                        'clientOptions' => [
                            'source' => $aSkills,
                        ],
                        'options'=>['class'=>'form-control']
                    ]);?>
                    <?= Html::error($oSkills, 'skill_name', ['class'=>'help-block']) ?>
                </div>

                <div class="form-group <?= $oSkills->hasErrors('percent') ? 'has-error' : '';?>">
                    <p><?=\Yii::t('app', 'Оцените свои навыки по 10 бальной шкале')?></p>
                    <div class="rating clearfix">
                        <span class="star off" rel="1"></span>
                        <span class="star off" rel="2"></span>
                        <span class="star off" rel="3"></span>
                        <span class="star off" rel="4"></span>
                        <span class="star off" rel="5"></span>
                        <span class="star off" rel="6"></span>
                        <span class="star off" rel="7"></span>
                        <span class="star off" rel="8"></span>
                        <span class="star off" rel="9"></span>
                        <span class="star off" rel="10"></span>
                    </div>
                    <?= Html::error($oSkills, 'percent', ['class'=>'help-block']) ?>

                </div>


                <?= Html::activeHiddenInput($oSkills, 'percent') ?>
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-2 pull-right">
                <div class="btnAddRemove">
                    <?= Html::submitButton(\Yii::t('app','  Сохранить'), ['class' => 'btn btn-primary fa fa-check-square-o', 'name' => 'save-button']) ?>
                </div>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>