<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use frontend\assets\SelectAsset;

SelectAsset::register($this);
/**
 * @var \common\models\Experience $oEducation
 */
?>

<?php $form = ActiveForm::begin(['id' => 'experience-form','class' =>'row', 'enableClientValidation'=>false]); ?>
    <div class="col-md-12">

        <div class="col-md-12">
            <h2><?= \Yii::t('app', 'Образование') ?></h2>
            <hr/>
            <div class="col-md-6">
                <div class="form-group <?php if($oEducation->hasErrors('start_date') || $oEducation->hasErrors('end_date') ) echo 'has-error';?>">
                    <?= Html::activeLabel($oEducation, 'dates', ['class'=>'control-label']) ?>
                    <?= kartik\widgets\DatePicker::widget([
                        'type' => \kartik\widgets\DatePicker::TYPE_RANGE,
                        'pluginOptions' => ['format' => 'yyyy-mm-dd'],
                        'model' => $oEducation,
                        'attribute' => 'start_date',
                        'attribute2' => 'end_date',
                        'separator' => '-'

                    ]) ?>
                    <?= Html::error($oEducation, 'start_date', ['class'=>'help-block']) ?>
                    <?= Html::error($oEducation, 'end_date', ['class'=>'help-block']) ?>
                </div>

                <div class="form-group <?= $oEducation->hasErrors('start_date') ? 'has-error' : '';?>">
                    <?= Html::activeLabel($oEducation, 'company_name', ['class'=>'control-label']) ?>
                    <?= \yii\jui\AutoComplete::widget([
                        'model' => $oEducation,
                        'attribute' => 'company_name',
                        'clientOptions' => [
                            'source' => $aCompany,
                        ],
                        'options'=>['class'=>'form-control']
                    ]);?>
                    <?= Html::error($oEducation, 'company_name', ['class'=>'help-block']) ?>
                </div>

                <div class="form-group <?= $oEducation->hasErrors('start_date') ? 'has-error' : '';?>">
                    <?= Html::activeLabel($oEducation, 'position_name', ['class'=>'control-label']) ?>
                    <?= \yii\jui\AutoComplete::widget([
                        'model' => $oEducation,
                        'attribute' => 'position_name',
                        'clientOptions' => [
                            'source' => $aPosition,
                        ],
                        'options'=>['class'=>'form-control']

                    ]);?>
                    <?= Html::error($oEducation, 'position_name', ['class'=>'help-block']) ?>
                </div>

                <?= $form->field($oEducation, 'country_id')->dropDownList($oCountry,['id'=>'country_id', 'class'=>'sl2', 'style'=>'width: 100%;']) ?>
                <?= $form->field($oEducation, 'city_id')->dropDownList($oCity,['id'=>'city_id', 'class'=>'sl2', 'style'=>'width: 100%;']) ?>
                <?= $form->field($oEducation, 'comment')->textarea() ?>
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-2 pull-right">
                <div class="btnAddRemove">
                    <?= Html::submitButton(\Yii::t('app','  Сохранить'), ['class' => 'btn btn-primary fa fa-check-square-o', 'name' => 'save-button']) ?>
                </div>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>