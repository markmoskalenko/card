<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $oPortfolio frontend\models\Portfolio */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="accoundContent" ng-app="profileApp">
    <div id="accountDetails" class="container">
        <div id="sidebar" class="userNavleft"><?= $this->render('//partials/_profileMenu'); ?></div>

        <div id="accound_form" class="row">

            <div class="col-md-10 col-md-offset-2">

                <div class="accoundForm_1 accound_register">

                    <div class="portfolio-form">
                        <h2><?=\Yii::t('app','Новая работа в портфолио')?></h2>
                        <?php $form = ActiveForm::begin(['options'=>['enctype'=>"multipart/form-data"]]); ?>

                        <input type="file" id="s"/>
                        <?= $form->field($oPortfolio, 'file')->fileInput(['style'=>'opacity:0']) ?>

                        <div class="form-group">
                            <?= Html::submitButton(\Yii::t('app','Сохранить'), ['class' => 'btn btn-success']) ?>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>