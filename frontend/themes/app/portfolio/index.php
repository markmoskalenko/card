<?php
use frontend\assets\ProfileAsset;
use yii\widgets\ActiveForm;

ProfileAsset::register($this);

/**
 * @var frontend\controllers\ProfileController $this
 * @var yii\widgets\ActiveForm $form
 * @var \common\models\User $model
 */
$this->title = \Yii::t('app','Портфолио');

?>

<section class="accound">

    <div class="accoundContent" ng-app="profileApp">
        <div id="accountDetails" class="container">
            <div id="sidebar" class="userNavleft"><?= $this->render('//partials/_profileMenu'); ?></div>

            <div id="accound_form" class="row">

                <div class="col-md-10 col-md-offset-2">

                    <div class="accoundForm_1 accound_register">

                        <?= \yii\helpers\Html::beginForm(); ?>
                        <div class="row nameGallery">
                            <div class="col-md-12">
                                <div class="col-md-12 formGray">
                                    <div class="col-md-12">
                                        <label for="nameGallery"><?=\Yii::t('app','Разделы галереи')?></label>
                                    </div>
                                    <div class="col-md-9">
                                        <?= \yii\helpers\Html::activeTextInput($oNewCategory, 'name', ['class'=>'form-control', 'placeholder'=>\Yii::t('app','наименование раздела')]); ?>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="btnAddRemove">
                                            <button type="submit" class="btn btn-primary"><i class="fa fa-plus-square"></i>&nbsp;&nbsp;<?=\Yii::t('app','Добавить')?></button>
                                        </div>
                                    </div>
                                    <div class="row nameSection">
                                        <div class="col-md-12">
                                            <ul>
                                                <?php if(count($oCategory)):?>
                                                    <?php foreach( $oCategory as $oItem ): ?>
                                                        <li class="col-md-6"><a data-placement="top" rel="tooltip" href="<?= \yii\helpers\Url::to(['portfolio/delete-category', 'id'=>$oItem->id]) ?>"
                                                                                onclick = "return confirm('Вы уверены?')" data-original-title="Удалить"><i class="fa fa-trash-o"></i></a><?= $oItem->name ?></li>
                                                    <?php endforeach;?>
                                                <?php else:?>
                                                    <li class="col-md-6"><?=\Yii::t('app','Вы еще не создали категории')?></li>
                                                <?php endif;?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?= \yii\helpers\Html::endForm(); ?>

                        <div class="col-md-12">
                            <div class="dividerLine"></div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <h3><?=\Yii::t('app','Галерея')?></h3>
                                </div>
                                <div class="portfolio-form">
                                    <?php $form = ActiveForm::begin(['options'=>['enctype'=>"multipart/form-data", 'id'=>'loadForm']]); ?>
                                        <?= $form->errorSummary($oPortfolio); ?>

                                        <div class="hidden">
                                            <?= $form->field($oPortfolio, 'file')->fileInput(['class'=>'hidden','id'=>'file']) ?>
                                        </div>

                                        <img id="animation" src="/themes/app/img/icons/progressbar.gif" style="margin-left: 45%; display: none">
                                    <?php ActiveForm::end(); ?>
                                </div>
                                <div class="col-md-6" style="float: right">
                                    <div class="btnAddRemove pull-right">
                                        <a  id="loadFiles" class="btn btn-primary" ><i class="fa fa-plus-square"></i>&nbsp;&nbsp;<?=\Yii::t('app','Загрузить изображение')?></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row PortfolioWrap" ng-controller="PortfolioCtrl" id="portfolio">
                            <?php foreach($oPortfolioItems as $oPortfolioItem):?>
                                <div class="col-xs-10 col-sm-4 col-md-3 PortfolioImg text-center">
                                    <div class="thumbnail img-responsive imgPrev">
                                        <img src="/upload/crop/<?= $oPortfolioItem->img ?>?<?= time().rand(0,1000)?>">
                                    </div>
                                    <?php if($oPortfolioItem->name):?>
                                        <div class="text" style="height: 15px; margin-bottom: 3pxs">
                                            <?= $oPortfolioItem->name?>
                                        </div>
                                    <?php endif;?>
                                    <input type="checkbox" <?= $oPortfolioItem->is_public ? 'checked' : '' ?> data-id="<?= $oPortfolioItem->id ?>" data-value="<?= $oPortfolioItem->is_public ?>" ng-click="pub($event)" class="pub_work"/>
                                    <div class="editDate">
                                        <a href="<?= \yii\helpers\Url::to(['portfolio/update','id'=>$oPortfolioItem->id]) ?>"><i class="fa fa-pencil"></i></a>
                                    </div>
                                    <div class="editDate">
                                        <a href="<?= \yii\helpers\Url::to(['portfolio/delete','id'=>$oPortfolioItem->id]) ?>" rel="tooltip"
                                           onclick = "return confirm('Вы уверены?')" title="" data-placement="top"  data-original-title="Удалить"><i class="fa fa-trash-o"></i></a>
                                    </div>
                                </div>
                            <?php endforeach;?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--<script>-->
<!--    document.getElementById ('portfolio').cloneNode (1);-->
<!--</script>-->