<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 12.09.14
 * Time: 18:12
 */
use frontend\assets\JcropPortfolioAsset;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

JcropPortfolioAsset::register($this);
?>

<section class="accound">
    <div class="accoundContent">
        <div id="accountDetails" class="container">
            <div id="sidebar" class="userNavleft"><?= $this->render('//partials/_profileMenu'); ?></div>
            <div class="row">
                <div class="col-md-10 col-md-offset-2">
                    <div class="accoundForm_1 accound_register">

                        <?php $form = ActiveForm::begin([
                                'method' => 'post',
                                'action' => ['/portfolio/cropping', 'id'=> $oPortfolio->id],
                            ]
                        ); ?>
                        <div class="row">

                            <div class="col-md-12">

                                <div style="float: left; width: 430px">
                                    <?= $form->field($oPortfolio, 'category_id')->dropDownList($aCategory) ?>
                                </div>
                                <div style=" float: right; width: 430px; margin-bottom: 40px">
                                    <?= $form->field($oPortfolio, 'name') ?>
                                </div>
                                <div class="process_one_photo podl">
                                    <img src="/upload/200x200/<?= $oPortfolio->img ?>" id="target"/> <!------------- Исходное фото ---------------->
                                </div>

                                <div id="preview-pane">
                                    <div class="preview-container">
                                        <img src="/upload/200x200/<?= $oPortfolio->img ?>" class="jcrop-preview" alt="Preview" /> <!------------- Превью ---------------->
                                    </div>
                                </div>
                            </div>
                            <div class="hidden">
                                <?= $form->field($oPortfolio, 'x') ?>
                                <?= $form->field($oPortfolio, 'y') ?>
                                <?= $form->field($oPortfolio, 'width') ?>
                                <?= $form->field($oPortfolio, 'height') ?>
                                <?= $form->field($oPortfolio, 'filename') ?>
                            </div>
                            <?= $form->errorSummary($oPortfolio); ?>
                            <div class="col-md-6 col-md-offset-6">
                                <div class="btnAddRemove">
                                    <?php echo Html::submitButton('<i class="fa fa-tasks"></i>&nbsp;&nbsp;Сохранить и опубликовать',array('id'=>'saveImg', 'class'=>'btn btn-primary'))?>
                                    <a type="submit" class="btn btn-primary" href="<?= \yii\helpers\Url::to(['portfolio/delete', 'id'=>$oPortfolio->id]) ?>"><i class="fa fa-trash-o"></i>&nbsp;&nbsp;Удалить</a>
                                </div>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>