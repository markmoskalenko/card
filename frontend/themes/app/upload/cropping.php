<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 26.09.2014
 * Time: 4:23
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

\frontend\assets\JcropAvatarAsset::register($this);
?>

<section class="accound">
    <div class="accoundContent">
        <div id="accountDetails" class="container">
            <div id="sidebar" class="userNavleft"><?= $this->render('//partials/_profileMenu'); ?></div>
            <div class="row">
                <div class="col-md-10 col-md-offset-2">
                    <div class="accoundForm_1 accound_register">

                        <?php $form = ActiveForm::begin([
                                'method' => 'post',
                                'action' => ['/upload/cropping'],
                            ]
                        ); ?>
                        <div class="row">

                            <div class="col-md-12">
                                <div class="process_one_photo podl">
                                    <img src="/upload/150x150/<?= $oUser->avatar ?>" id="target"/> <!------------- Исходное фото ---------------->
                                </div>

                                <div id="preview-pane" style="margin-right: 59px;">
                                    <div class="preview-container" style="width: 150px !important; height: 150px !important;">
                                        <img src="/upload/150x150/<?= $oUser->avatar ?>" class="jcrop-preview" alt="Preview" /> <!------------- Превью ---------------->
                                    </div>
                                </div>
                            </div>

                            <div class="hidden">
                                <?= $form->field($oUser, 'x') ?>
                                <?= $form->field($oUser, 'y') ?>
                                <?= $form->field($oUser, 'width') ?>
                                <?= $form->field($oUser, 'height') ?>
                                <?= $form->field($oUser, 'filename') ?>
                            </div>
                            <div class="col-md-6 col-md-offset-6">
                                <div class="btnAddRemove">
                                    <?php echo Html::submitButton('<i class="fa fa-tasks"></i>&nbsp;&nbsp;Сохранить и опубликовать',array('id'=>'save', 'class'=>'btn btn-primary'))?>
                                    <a type="submit" class="btn btn-primary" href="<?= \yii\helpers\Url::to(['upload/delete']) ?>"><i class="fa fa-trash-o"></i>&nbsp;&nbsp;Удалить</a>
                                </div>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>