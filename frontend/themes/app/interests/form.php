<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
/**
 * @var \common\models\Interests $oInterests
 */
?>

<?php $form = ActiveForm::begin(['id' => 'experience-form','class' =>'row', 'enableClientValidation'=>false]); ?>
    <div class="col-md-12">

        <div class="col-md-12">
            <h2><?= \Yii::t('app', 'Интересы') ?></h2>
            <hr/>
            <div class="col-md-6">
                <div class="form-group <?= $oInterests->hasErrors('interest_name') ? 'has-error' : '';?>">
                    <?= Html::activeLabel($oInterests, 'interest_name', ['class'=>'control-label']) ?>
                    <?= \yii\jui\AutoComplete::widget([
                        'model' => $oInterests,
                        'attribute' => 'interest_name',
                        'clientOptions' => [
                            'source' => $aInterests,
                        ],
                        'options'=>['class'=>'form-control']
                    ]);?>
                    <?= Html::error($oInterests, 'interest_name', ['class'=>'help-block']) ?>
                </div>

                <?= $form->field($oInterests, 'description')->textarea() ?>
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-2 pull-right">
                <div class="btnAddRemove">
                    <?= Html::submitButton(\Yii::t('app','  Сохранить'), ['class' => 'btn btn-primary fa fa-check-square-o', 'name' => 'save-button']) ?>
                </div>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>