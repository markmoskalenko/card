<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'name' => 'Проект - визитная карточка',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'sourceLanguage' => 'ru',
    'language' => 'en-EN',
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                    'sourceLanguage' => 'en',
                    'fileMap' => [
                        'app' => 'app.php',
                        //'main' => 'main.php',
                    ],
                ],
                'eauth' => array(
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@eauth/messages',
                ),
            ],
        ],
        'mail' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => false,
            'viewPath' => '@common/mail',
        ],

        'robokassa' => [
            'class' => '\robokassa\Merchant',
            'sMerchantLogin' => 'myres',
            'sMerchantPass1' => 'Yzd31qOh',
            'sMerchantPass2' => 'bTier2Mh',
        ],

        'eauth' => array(
            'class' => 'nodge\eauth\EAuth',
            'popup' => true,
            'cache' => false,
            'cacheExpire' => 0,
            'httpClient' => array(),
            'services' => array(
                'twitter' => array(
                    // register your app here: https://dev.twitter.com/apps/new
                    'class' => 'nodge\eauth\services\TwitterOAuth1Service',
                    'key' => 'rHYLm9BM6ceHWRndh2dWpIgmS',
                    'secret' => 'lKf4SlfSTrRcSWQi6k34zhE9mLp147SFK6jFYljf5yYFAyKg4E',
                ),
                'facebook' => array(
                    // register your app here: https://developers.facebook.com/apps/
                    'class' => 'nodge\eauth\services\FacebookOAuth2Service',
                    'clientId' => '354417578047989',
                    'clientSecret' => 'b0bceb665da9ae6f426155e1efc8611a',
                ),
                'vkontakte' => array(
                    // register your app here: https://vk.com/editapp?act=create&site=1
                    'class' => 'nodge\eauth\services\VKontakteOAuth2Service',
                    'clientId' => '4588680',
                    'clientSecret' => '078vRjyirAarDdR9wwji',
                ),
            ),
        ),

        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'loginUrl' => ['user/login'],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],


        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'class' => 'common\components\LangUrlManager',
            'rules' => [

                $params['scheme'] . '<alias:(\p{L}|\p{N}|-)+>.' . $params['baseDomain'].'/vcard.vcf' => 'go/vcard',
                $params['scheme'] . '<alias:(\p{L}|\p{N}|-)+>.' . $params['baseDomain'] => 'go/index',

                '/' => 'site/index',

                'page/<slug:(\p{L}|\p{N}|-)+>/' => 'page/view',

                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ]
        ],

        'request' => [
            'class' => 'common\components\LangRequest',
            'cookieValidationKey' => 'qweqwekspods'
        ],

        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],

        'view' => [
            'class' => 'frontend\components\View',
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
    ],
    'params' => $params,
];
