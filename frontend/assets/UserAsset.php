<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class UserAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web/themes/app';
    public $css= [
        'css/jquery.vegas.css',
      ];
    public $js = [

        'js/jquery.vegas.js',
        'js/portamento.js',
        'js/site.index.js'
      ];
    public $depends = [
        'frontend\assets\AppAsset',
      ];
}