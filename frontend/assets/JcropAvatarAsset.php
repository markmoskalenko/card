<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class JcropAvatarAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web/themes/app';
    public $css= [
        'css/jquery.Jcrop.css',
        'css/style_img.css',

      ];
    public $js = [
        'js/img_js/bootstrap.min.js',
        'js/img_js/jquery.Jcrop.js',
        'js/cropAvatar.js',
    ];
    public $depends = [
        'frontend\assets\AppAsset',
      ];
}
