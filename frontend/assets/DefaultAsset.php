<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class DefaultAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web/themes/default';
    public $css= [
        'css/style.css',
        'css/jquery.fancybox.css',
        'font-awesome/css/font-awesome.min.css',
        'css/style.css',
      ];
    public $js = [
        'js/jquery.min.js',
        'js/modernizr.js',
        //'js/bootstrap.min.js',
        'js/bootstrap-progressbar.min.js'
        'js/jquery.isotope.js',
	'js/jquery.fancybox.pack.js',
	'js/allScript.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
      ];
}