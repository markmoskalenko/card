<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class SelectAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web/library/select2';
    public $css= [
        'select2.css',
        'select2-bootstrap.css',
      ];
    public $js = [
        'select2.min.js',
      ];
    public $depends = [
        'yii\bootstrap\BootstrapAsset',
        'yii\web\JqueryAsset',
    ];
}