<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class ProfileAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web/themes/app';
    public $css= [
        'select2-3.5.1/select2.css'
      ];
    public $js = [

        'select2-3.5.1/select2.js',

        'js/profile.contact.form.js',

        'js/profile.script.js',

        'js/jquery.uploadifive-v1.0.min.js',

        'js/upload.js',
        'js/load.js',
      ];
    public $depends = [
        'frontend\assets\PnotifyAsset',
        'frontend\assets\JcropAsset',
        'frontend\assets\AppAsset',
    ];
}