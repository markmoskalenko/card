<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class Test2Asset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web/themes/test2';
    public $css= [
        'css/style.css',
      ];
    public $js = [
        'js/app.js',

    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
      ];
}