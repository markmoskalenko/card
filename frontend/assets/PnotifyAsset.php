<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class PnotifyAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web/library/pnotify';
    public $css = [
        'pnotify.custom.min.css',
    ];
    public $js = [
        'pnotify.custom.min.js',
      ];
    public $depends = [
        'yii\web\JqueryAsset',
      ];
}