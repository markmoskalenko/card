<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web/themes/app';
    public $css= [
        'css/bootstrap.min.css',
        'css/normalize.css',
        'font-awesome/css/font-awesome.min.css',
        'css/style.css',
      ];
    public $js = [
        'https://ajax.googleapis.com/ajax/libs/angularjs/1.2.22/angular.min.js',
        'js/jquery.autocomplete.js',
        'js/jquery.cookie.js',
        'js/app.js',
        'js/radioButtons.js',
        'js/totalScript.js',

    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'frontend\assets\FancyBoxAsset',
        'frontend\assets\PnotifyAsset'
      ];
}