<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class JcropAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web/library/jcrop';
    public $css= [
        'css/jquery.Jcrop.css',
        'css/jquery.Jcrop.min.css'
      ];
    public $js = [
        'js/jquery.Jcrop.min.js',
      ];
    public $depends = [

      ];
}