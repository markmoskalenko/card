<?php

namespace frontend\models;

use common\models\User;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * This is the model class for table "tbl_portfolio_category".
 *
 * @property integer $id
 * @property string $name
 * @property integer $user_id
 *
 * @property Portfolio[] $portfolios
 * @property User $user
 */
class PortfolioCategory extends \common\models\PortfolioCategory
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_portfolio_category';
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        return parent::find()->where(['user_id'=>User::u()->id]);
    }

    public function beforeValidate(){

        if($this->isNewRecord){
            $this->user_id = User::u()->id;
        }

        return parent::beforeValidate();
    }

    public static function getMap(){

        return ArrayHelper::map( self::find()->all(), 'id', 'name');
    }

}
