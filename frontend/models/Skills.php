<?php

namespace frontend\models;

use common\models\DictionarySchools;
use common\models\User;
use Yii;

/**
 * This is the model class for table "tbl_education".
 *
 * @property integer $id
 * @property string $start_date
 * @property string $end_date
 * @property integer $company_id
 * @property integer $country_id
 * @property integer $city_id
 * @property string $comment
 * @property integer $user_id
 *
 * @property DictionarySchools $company
 * @property User $user
 */
class Skills extends \common\models\Skills
{
    /**
     * @inheritdoc
     */
    public static function find()
    {
        return parent::find()->where(['user_id'=>User::u()->id]);
    }

    public static function getForAngularJs(){

        $aModels = static::find()->asArray()->with(['skill'])->all();

        $aResult = [];

        foreach( $aModels as $aModel ){
            $aResult[] = [
                'id' => $aModel['id'],
                'name' => $aModel['skill']['name'],
                'percent'=>$aModel['percent'],
                'is_public' => $aModel['is_public'],

            ];

        }

        return $aResult;
    }
}