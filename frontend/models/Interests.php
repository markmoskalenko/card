<?php

namespace frontend\models;

use common\models\DictionaryInterests;
use common\models\User;
use Yii;

/**
 * This is the model class for table "tbl_interests".
 *
 * @property integer $id
 * @property integer $interest_id
 * @property integer $user_id
 * @property string $description
 *
 * @property DictionaryInterests $interest
 * @property User $user
 */
class Interests extends \common\models\Interests
{
    /**
     * @inheritdoc
     */
    public static function find()
    {
        return parent::find()->where(['user_id'=>User::u()->id]);
    }

    public static function getForAngularJs(){

        $aModels = static::find()->asArray()->with(['interest'])->all();

        $aResult = [];

        foreach( $aModels as $aModel ){
            $aResult[] = [
                'id' => $aModel['id'],
                'comment' => $aModel['description'],
                'name' => $aModel['interest']['name'],
                'is_public' => $aModel['is_public'],

            ];

        }

        return $aResult;
    }
}
