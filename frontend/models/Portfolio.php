<?php

namespace frontend\models;

use common\models\User;
use yii\web\UploadedFile;
use Yii;

/**
 * This is the model class for table "tbl_portfolio".
 *
 * @property integer $id
 * @property string $img
 * @property integer $category_id
 * @property integer $user_id
 *
 * @property User $user
 * @property PortfolioCategory $category
 */
class Portfolio extends \common\models\Portfolio
{
    public  $file;
    /**
     * @inheritdoc
     */
    public static function find()
    {
        return parent::find()->where(['user_id'=>User::u()->id]);
    }

    public function beforeValidate(){

        if($this->isNewRecord){
            $this->user_id = User::u()->id;
        }

        $sUploadDirectory = Yii::getAlias('@frontend/web/upload/original');

        $sSmallImageDirectory = Yii::getAlias('@frontend/web/upload/200x200');

        $this->file = UploadedFile::getInstance( $this, 'file' );

        if( $this->file ){

            $aSourcePath = pathinfo($this->file->name);
//            var_dump(!in_array($aSourcePath['extension'], ['png', 'jpg', 'gif']));
//            var_dump($aSourcePath['extension']);
            if(!in_array(strtolower($aSourcePath['extension']), ['png', 'jpg', 'jpeg','gif'])) {
               return $this->addError('file', \Yii::t('app', 'Ошибка! Допустимые расширения картинок - *.png, *.gif, *.jpg'));
            }

            $filename = md5(microtime().rand(1,10)) .'.'. $aSourcePath['extension'];

            $this->file->saveAs($sUploadDirectory.DIRECTORY_SEPARATOR.$filename);

            $picture = new  \frontend\components\SimpleImage();

            $picture->load($sUploadDirectory.DIRECTORY_SEPARATOR.$filename);

            $imageHeight = $picture->getHeight();

            $imageWidth = $picture->getWidth();

            if( $imageHeight < 400 || $imageWidth < 400 ){
                $this->addError('file',\Yii::t('app','Ошибка! Изображение должно быть не меньше 400x400 пикселей'));
            }


            if($imageWidth > 600 && $imageWidth == $imageHeight)    {
                $picture->resizeToHeight(600);
            }
            if($imageWidth > 630 && $imageWidth> $imageHeight){
                $picture->resizeToHeight(400);
            }else if($imageHeight>1110 && $imageHeight >$imageWidth){
                $picture->resizeToWidth(400);
            }

            $picture->save($sSmallImageDirectory.DIRECTORY_SEPARATOR.$filename);

            $this->img = $filename;
        }

        if(empty($this->img)){
            $this->addError('file',\Yii::t('app','Выберите изображение для загрузки'));
        }

        return parent::beforeValidate();
    }

    public function beforeDelete(){

        if($this->img) {
            $sUploadDirectory = Yii::getAlias('@frontend/web/upload/original');
            $sSmallImageDirectory = Yii::getAlias('@frontend/web/upload/200x200');

            $small = $sSmallImageDirectory.DIRECTORY_SEPARATOR.$this->img;
            $original = $sUploadDirectory.DIRECTORY_SEPARATOR.$this->img;

            if (is_file($small) && is_file($original)) {
                unlink($small);
                unlink($original);
            }
        }
        return true;
    }
}
