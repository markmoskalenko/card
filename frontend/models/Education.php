<?php

namespace frontend\models;

use common\models\DictionarySchools;
use common\models\User;
use Yii;

/**
 * This is the model class for table "tbl_education".
 *
 * @property integer $id
 * @property string $start_date
 * @property string $end_date
 * @property integer $company_id
 * @property integer $country_id
 * @property integer $city_id
 * @property string $comment
 * @property integer $user_id
 *
 * @property DictionarySchools $company
 * @property User $user
 */
class Education extends \common\models\Education
{
    /**
     * @inheritdoc
     */
    public static function find()
    {
        return parent::find()->where(['user_id'=>User::u()->id]);
    }

    public static function getForAngularJs(){

        $aModels = static::find()->asArray()->with(['company', 'country', 'city', 'position'])->all();

        $aResult = [];

        foreach( $aModels as $aModel ){
            $aResult[] = [
                'id' => $aModel['id'],
                'start_date' => date('d-m-Y',strtotime($aModel['start_date'])),
                'end_date' => date('d-m-Y',strtotime($aModel['end_date'])),
                'comment' => $aModel['comment'],
                'company' => $aModel['company']['name'],
                'country' => $aModel['country']['name'],
                'city' => $aModel['city']['name'],
                'position' => $aModel['position']['name'],
                'is_public' => $aModel['is_public'],
            ];

        }

        return $aResult;
    }
}
