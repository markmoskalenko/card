$(function(){
// звёзды рейтинга
    $(".rating span.star").hover(
        function() {
            $('.rating span.star').removeClass('on').addClass('off');
            $(this).prevAll().addClass('over');
        }
        , function() {
            $(this).removeClass('over');
        }
    );

    $(".rating").mouseleave(function(){
        $(this).parent().find('.over').removeClass('over');
    });

    $( ".rating span.star" ).click( function() {
        $(this).prevAll().removeClass('off').addClass('on');
        $('#skills-percent').val($(this).attr('rel')*10);
        $(this).removeClass('off').addClass('on');
    });
});

function setEmail(){}

var position_name;

$('#country_id').change(function(){
    var country_id = $(this).val();
    $.ajax({
        url : '/city/get-city-by-country',
        data : { id : country_id },
        type: 'GET'
    }).done(function(r){
        $("#city_id").select2("val", "");

        $("#city_id").empty();

        $.each( r, function(i, item) {
            $("#city_id").append( $('<option value="'+i+'">'+r[i]+'</option>'))
        });
    });
});

$(".sl2").select2();
