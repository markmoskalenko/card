/**
 * Сохранение форм в профайле
 * @param form
 * @returns {boolean}
 */
function submitForm(form, reload){
    jQuery.ajax({
        url: "/site/index",
        type: "POST",
        dataType: "json",
        data: form.serialize(),
        success : function( r ){
            if(r['status'] == 'success'){
                PNotify.desktop.permission();
                (new PNotify({
                    title: 'Сохранение данных',
                    text: 'Данные успешно сохранены!',
                    type: 'info',
                    desktop: {
                        desktop: true
                    }
                }));
            }else{
                PNotify.desktop.permission();
                (new PNotify({
                    title: 'Сохранение данных',
                    text: 'Ошибка сохранения данных!',
                    type: 'error',
                    desktop: {
                        desktop: true
                    }
                }));
            }

            if( reload == true ){
                location.reload();
            }
        }
    });
    return false;
}

function noticeSave(){
    PNotify.desktop.permission();
    (new PNotify({
        title: 'Сохранение данных',
        text: 'Данные успешно сохранены!',
        type: 'info',
        desktop: {
            desktop: true
        }
    }));
}

'use strict';
/* Controllers */

var profileApp = angular.module('profileApp', []);

profileApp.controller('WorkCtrl', function($scope,$http) {

    $http.get('/site/get-works').success(function(data) {
        $scope.works = data;
    });

    $scope.add = function(){
        $.fancybox({
            width     : 700,
            height    : 400,
            minWidth  : 400,
            maxWidth  : 700,
            autoScale: true,
            transitionIn: 'fade',
            transitionOut: 'fade',
            closeEffect : 'none',
            overlayShow:	true,
            hideOnOverlayClick:false,
            closeClick: false,
            type: 'iframe',
            href : '/experience/create',
            helpers     : {
                overlay : {closeClick: false} // prevents closing when clicking OUTSIDE fancybox
            },
            afterClose : function(){
                $http.get('/site/get-works').success(function(data) {
                    $scope.works = data;
                });
            }
        });

    }

    $scope.edit = function(obj){
        var id = obj.currentTarget.getAttribute('data-id');

        $.fancybox({
            width     : 700,
            height    : 400,
            minWidth  : 400,
            maxWidth  : 700,
            autoScale: false,
            closeClick: false,
            closeEffect : 'none',
            hideOnOverlayClick:false,
            helpers     : {
                overlay : {closeClick: false}},
            overlayShow:	true,
            transitionIn: 'fade',
            transitionOut: 'fade',
            type: 'iframe',
            href : '/experience/update?id='+id,
            afterClose : function(){
                $http.get('/site/get-works').success(function(data) {
                    $scope.works = data;
                });
            }
        });
    }

    $scope.delete = function(obj){
        if (confirm('Вы уверены?')) {
            var id = obj.currentTarget.getAttribute('data-id');
            $.ajax({
                url : '/experience/delete?id='+id
            }).done(function(){
                $http.get('/site/get-works').success(function(data) {
                    $scope.works = data;
                });
            });
        }
    }

    $scope.pub = function(obj){

        var id = obj.currentTarget.getAttribute('id');

        var value = obj.currentTarget.getAttribute('value') == 1 ? 0 : 1;

        $.ajax({
            url : '/experience/pub',
            data: {
                id : id,
                val : value
            }
        }).done(function(){
            $http.get('/site/get-works').success(function(data) {
                $scope.works = data;
            });
        });
    }
});

profileApp.controller('EducationCtrl', function($scope,$http) {

    $http.get('/site/get-educations').success(function(data) {
        $scope.educations = data;
    });

    $scope.add = function(){
        $.fancybox({
            width     : 700,
            height    : 400,
            minWidth  : 400,
            maxWidth  : 700,
            autoScale: false,
            hideOnOverlayClick : false,
            helpers     : {
                overlay : {closeClick: false}},
            transitionIn: 'fade',
            transitionOut: 'fade',
            type: 'iframe',
            href : '/education/create',
            afterClose : function(){
                $http.get('/site/get-educations').success(function(data) {
                    $scope.educations = data;
                });
            }
        });

    }

    $scope.edit = function(obj){
        var id = obj.currentTarget.getAttribute('data-id');

        $.fancybox({
            width     : 700,
            height    : 400,
            minWidth  : 400,
            maxWidth  : 700,
            autoScale: false,
            hideOnOverlayClick : false,
            helpers     : {
                overlay : {closeClick: false}},
            transitionIn: 'fade',
            transitionOut: 'fade',
            type: 'iframe',
            href : '/education/update?id='+id,
            afterClose : function(){
                $http.get('/site/get-educations').success(function(data) {
                    $scope.educations = data;
                });
            }
        });
    }

    $scope.delete = function(obj){
        if (confirm('Вы уверены?')) {
            var id = obj.currentTarget.getAttribute('data-id');
            $.ajax({
                url : '/education/delete?id='+id
            }).done(function(){
                $http.get('/site/get-educations').success(function(data) {
                    $scope.educations = data;
                });
            });
        }

    }

    $scope.pub = function(obj){

        var id = obj.currentTarget.getAttribute('id');

        var value = obj.currentTarget.getAttribute('value') == 1 ? 0 : 1;

        $.ajax({
            url : '/education/pub',
            data: {
                id : id,
                val : value
            }
        }).done(function(){
            $http.get('/site/get-educations').success(function(data) {
                $scope.educations = data;
            });
        });
    }
});


profileApp.controller('SkillCtrl', function($scope,$http) {

    $http.get('/site/get-skill').success(function(data) {
        $scope.skills = data;
    });

    $scope.add = function(){
        $.fancybox({
            width     : 700,
            height    : 400,
            minWidth  : 400,
            maxWidth  : 700,
            autoScale: false,
            hideOnOverlayClick : false,
            helpers     : {
                overlay : {closeClick: false}},
            transitionIn: 'fade',
            transitionOut: 'fade',
            type: 'iframe',
            href : '/skills/create',
            afterClose : function(){
                $http.get('/site/get-skill').success(function(data) {
                    $scope.skills = data;
                });
            }
        });

    }

    $scope.edit = function(obj){
        var id = obj.currentTarget.getAttribute('data-id');

        $.fancybox({
            width     : 700,
            height    : 400,
            minWidth  : 400,
            maxWidth  : 700,
            autoScale: false,
            hideOnOverlayClick : false,
            helpers     : {
                overlay : {closeClick: false}},
            transitionIn: 'fade',
            transitionOut: 'fade',
            type: 'iframe',
            href : '/skills/update?id='+id,
            afterClose : function(){
                $http.get('/site/get-skill').success(function(data) {
                    $scope.skills = data;
                });
            }
        });
    }

    $scope.delete = function(obj){
        if (confirm('Вы уверены?')) {
            var id = obj.currentTarget.getAttribute('data-id');
            $.ajax({
                url : '/skills/delete?id='+id
            }).done(function(){
                $http.get('/site/get-skill').success(function(data) {
                    $scope.skills = data;
                });
            });
        }

    }

    $scope.pub = function(obj){

        var id = obj.currentTarget.getAttribute('id');

        var value = obj.currentTarget.getAttribute('value') == 1 ? 0 : 1;

        $.ajax({
            url : '/skills/pub',
            data: {
                id : id,
                val : value
            }
        }).done(function(){
            $http.get('/site/get-skill').success(function(data) {
                $scope.skills = data;
            });
        });
    }
});


profileApp.controller('PortfolioCtrl', function($scope) {

    $scope.pub = function(obj){

        var id = obj.currentTarget.getAttribute('data-id');

        var value = $(obj.currentTarget).is(':checked') ? 1 : 0;

        $.ajax({
            url : '/portfolio/published',
            data: {
                id : id,
                val : value
            }
        });
    }
});

profileApp.controller('InterestsCtrl', function($scope,$http) {

    $http.get('/site/get-interest').success(function(data) {
        $scope.interests = data;
    });

    $scope.add = function(){
        $.fancybox({
            width     : 700,
            height    : 400,
            minWidth  : 400,
            maxWidth  : 700,
            autoScale: false,
            hideOnOverlayClick : false,
            helpers     : {
                overlay : {closeClick: false}},
            transitionIn: 'fade',
            transitionOut: 'fade',
            type: 'iframe',
            href : '/interests/create',
            afterClose : function(){
                $http.get('/site/get-interest').success(function(data) {
                    $scope.interests = data;
                });
            }
        });

    }

    $scope.edit = function(obj){
        var id = obj.currentTarget.getAttribute('data-id');

        $.fancybox({
            width     : 700,
            height    : 400,
            minWidth  : 400,
            maxWidth  : 700,
            autoScale: false,
            hideOnOverlayClick : false,
            helpers     : {
                overlay : {closeClick: false}},
            transitionIn: 'fade',
            transitionOut: 'fade',
            type: 'iframe',
            href : '/interests/update?id='+id,
            afterClose : function(){
                $http.get('/site/get-interest').success(function(data) {
                    $scope.interests = data;
                });
            }
        });
    }

    $scope.delete = function(obj){
        if (confirm('Вы уверены?')) {
            var id = obj.currentTarget.getAttribute('data-id');
            $.ajax({
                url : '/interests/delete?id='+id
            }).done(function(){
                $http.get('/site/get-interest').success(function(data) {
                    $scope.interests = data;
                });
            });
        }

    }

    $scope.pub = function(obj){

        var id = obj.currentTarget.getAttribute('id');

        var value = obj.currentTarget.getAttribute('value') == 1 ? 0 : 1;

        $.ajax({
            url : '/interests/pub',
            data: {
                id : id,
                val : value
            }
        }).done(function(){
            $http.get('/site/get-interest').success(function(data) {
                $scope.interests = data;
            });
        });
    }
});


profileApp.controller('changePassword', function($scope,$http) {

    $scope.change = function(){
        $.fancybox({
            width     : 700,
            height    : 400,
            minWidth  : 400,
            maxWidth  : 700,
            autoScale: false,
            hideOnOverlayClick : false,
            helpers     : {
                overlay : {closeClick: false}},
            transitionIn: 'fade',
            transitionOut: 'fade',
            type: 'iframe',
            href : '/user/change-password'
        });
    }
});

$('.fb').fancybox({
    width     : 700,
    height    : 400,
    minWidth  : 400,
    maxWidth  : 700,
    autoScale: false,
    hideOnOverlayClick : false,
    helpers     : {
        overlay : {closeClick: false}},
    transitionIn: 'fade',
    transitionOut: 'fade',
    type: 'iframe',
    afterClose : function(){
        location.reload();
    }
});

$('.pub_work').click(function(){
    $.ajax({
        url:'/portfolio/published',
        type: 'get',
        data: {id:$(this).attr('data-id'),val:$(this).is(':checked')?1:0}
    });
});