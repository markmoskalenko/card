$('#file_upload').uploadifive({
    'uploadScript' : '/upload/upload',
    'auto'           : true,
    'onUploadComplete' : function(name, data, response) {
        var data = JSON.parse(data);

        if( data.status == 'error' ){

            alert( data.message );

        }else{
            window.location.href = '/upload/cropping';
        }

    }
});