jQuery(function($){

    var jcrop_api,
        boundx,
        boundy,
        rx,ry,
        ctest,

        $preview = $('#preview-pane'),
        $pcnt = $('#preview-pane .preview-container'),
        $pimg = $('#preview-pane .preview-container img'),

        xsize = $pcnt.width(),
        ysize = $pcnt.height(),

	width = $pimg.width(),
        height = $pimg.height();
	
    $(".process_one_photo .podl").css({width: width + 'px !important', height: height + 'px !important'});

    $(".jcrop-holder").css({width: width + 'px !important', height: height + 'px !important'});

    $(".jcrop-holder .jcrop-tracker").css({width: width + 'px !important', height: height + 'px !important'});
   
    console.log('init',[width ,height ]);
    $('#target').Jcrop({
        onChange: updatePreview,
        onSelect: updatePreview,
        bgFade:     true,
        bgOpacity: .5,
        aspectRatio: xsize / ysize,
        minSize:  [ 250, 250 ], //минимальный размер превью
        setSelect: [ 0, 0, 250, 250 ] //исходное состояние рамки на изображении

    },function(){
        // Use the API to get the real image size
        var bounds = this.getBounds();
        boundx = bounds[0];
        boundy = bounds[1];
        // Store the API in the jcrop_api variable
        jcrop_api = this;

        // Move the preview into the jcrop container for css positioning
        $preview.appendTo(jcrop_api.ui.holder);
    });

    function updatePreview(c)
    {
        if (parseInt(c.w) > 0)
        {

            ctest = c;
            rx = xsize / c.w;
            ry = ysize / c.h;

            $pimg.css({
                width: Math.round(rx * boundx) + 'px',
                height: Math.round(ry * boundy) + 'px',
                marginLeft: '-' + Math.round(rx * c.x) + 'px',
                marginTop: '-' + Math.round(ry * c.y) + 'px'
            });

            var imageName = document.getElementById("target");

            var height = $('.jcrop-tracker').height();
            var width = $('.jcrop-tracker').width();

            var position = $('.jcrop-holder div').position();
            var src = imageName.src;
            var li = src.lastIndexOf("/");
            var fileName;
            if (li == -1) {
                fileName = src;
            } else {
                fileName = src.substring(li + 1);
            }
            document.getElementById('portfolio-x').value = position.left;
            document.getElementById('portfolio-y').value = position.top;
            document.getElementById('portfolio-width').value = width;
            document.getElementById('portfolio-height').value = height;
            document.getElementById('portfolio-filename').value = fileName;

        }
    }
    function GetCssStyle(e){
        if (e.currentStyle) return e.currentStyle;
        else if (window.getComputedStyle) return window.getComputedStyle(e,null)
    }
});


