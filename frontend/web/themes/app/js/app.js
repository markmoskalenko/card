
function setEmail(){
    $.fancybox({
        width     : 700,
        height    : 400,
        minWidth  : 400,
        minHeight : 400,
        maxWidth  : 700,
        maxHeight : 400,
        autoScale: false,
        'transitionIn': 'fade',
        'transitionOut': 'fade',
        'type': 'iframe',
        'href': '/user/set-email'
    });
}


$('#user-country_id').change(function(){
    var country_id = $(this).val();
    $.ajax({
        url : '/city/get-city-by-country',
        data : { id : country_id },
        type: 'GET'
    }).done(function(r){
        $("#user-city_id").select2("val", "");
        $("#user-city_id").empty();
        $.each( r, function(i, item) {
            $("#user-city_id").append( $('<option value="'+i+'">'+r[i]+'</option>'))
        });
    });
});

$('.ajax .submit').click(function(){
    var form = $(this).parents('form');

    submitForm(form, $(this).hasClass('refresh'));

    return false;
});