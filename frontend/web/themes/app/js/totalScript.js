jQuery.fn.customInput = function(){
	return $(this).each(function(){	
		if($(this).is('[type=checkbox],[type=radio]')){
			var input = $(this);
			
			// get the associated label using the input's id
			var label = $('label[for='+input.attr('id')+']');
			
			// wrap the input + label in a div 
			input.add(label).wrapAll('<div class="custom-'+ input.attr('type') +'"></div>');
			
			// necessary for browsers that don't support the :hover pseudo class on labels
			label.hover(
				function(){ $(this).addClass('hover'); },
				function(){ $(this).removeClass('hover'); }
			);
			
			//bind custom event, trigger it, bind click,focus,blur events					
			input.bind('updateState', function(){	
				input.is(':checked') ? label.addClass('checked') : label.removeClass('checked checkedHover checkedFocus'); 
			})
			.trigger('updateState')
			.click(function(){ 
				$('input[name='+ $(this).attr('name') +']').trigger('updateState'); 
			})
			.focus(function(){ 
				label.addClass('focus'); 
				if(input.is(':checked')){  $(this).addClass('checkedFocus'); } 
			})
			.blur(function(){ label.removeClass('focus checkedFocus'); });
		}
	});
};

$(document).ready(function() { 
    $('a#go').click( function(event){ 
        event.preventDefault(); 
        $('#overlay').fadeIn(300, 
            function(){ 
                $('#modal_form')
                    .css('display', 'block') 
                    .animate({opacity: 1, top: '50%'}, 200); 
        });
    });
    $('#modal_close, #overlay').click( function(){ 
        $('#modal_form')
            .animate({opacity: 0, top: '45%'}, 200, 
                function(){ 
                    $(this).css('display', 'none'); 
                    $('#overlay').fadeOut(300); 
                }
            );
    });
});

$(function() {  //всплывающие подсказки
	   $("[rel='tooltip']") .tooltip();
     });
	 
	 
jQuery(document).ready(function() {
 	var $ = jQuery;
    var screenRes = $(window).width(),
        screenHeight = $(window).height(),
        html = $('html');
		
// Checkbox
    if ($("select").hasClass("select_styled")) {
        cuSel({changedEl: ".select_styled", visRows: 8, scrollArrows: true});
    }
    if ($("div,p").hasClass("input_styled")) {
        $(".input_styled input").customInput();
    }
	
	// звёзды рейтинга
    $(".rating span.star").hover(
        function() {
            $('.rating span.star').removeClass('on').addClass('off');
            $(this).prevAll().addClass('over');
        }
        , function() {
            $(this).removeClass('over');
        }
    );

    $(".rating").mouseleave(function(){
        $(this).parent().find('.over').removeClass('over');
    });

    $( ".rating span.star" ).click( function() {
        $(this).prevAll().removeClass('off').addClass('on');
        $(this).removeClass('off').addClass('on');
    });


});


$(document).ready(function(){
    var sidebartop = $('#sidebar').offset().top;
	var minibartop = $('#mini-nav').offset().top;
    $(window).scroll(function(){
        if( $(window).scrollTop() > sidebartop ) {
            $('#sidebar').css({position: 'fixed', top: '20px'});
        } else {
            $('#sidebar').css({position: 'static'});
        }
    });
	 
	$(window).scroll(function(){
        if( $(window).scrollTop() > 20 && minibartop ) {
            $('#mini-nav').css({position: 'fixed', top: '0'});
			$('#mini-nav .dropdown').css({padding: '5px 0 10px 0'});
			                   
        } else {
            $('#mini-nav').css({position: 'static'});
			
        }
    });
    });
	
	 
	$(document).ready(function() { 
    $('a#go').click( function(event){ 
        event.preventDefault(); 
        $('#overlay').fadeIn(300, 
            function(){ 
                $('#modal_form')
                    .css('display', 'block') 
                    .animate({opacity: 1, top: '50%'}, 200); 
        });
    });
    $('#modal_close, #overlay').click( function(){ 
        $('#modal_form')
            .animate({opacity: 0, top: '45%'}, 200, 
                function(){ 
                    $(this).css('display', 'none'); 
                    $('#overlay').fadeOut(300); 
                }
            );
    });
});


(function() { // Кнопка вверх
function $(id){
    return document.getElementById(id);
}

function goTop(acceleration, time) {
    acceleration = acceleration || 0.1;
    time = time || 12;

    var dx = 0;
    var dy = 0;
    var bx = 0;
    var by = 0;
    var wx = 0;
    var wy = 0;

    if (document.documentElement) {
        dx = document.documentElement.scrollLeft || 0;
        dy = document.documentElement.scrollTop || 0;
    }
    if (document.body) {
        bx = document.body.scrollLeft || 0;
        by = document.body.scrollTop || 0;
    }
    var wx = window.scrollX || 0;
    var wy = window.scrollY || 0;

    var x = Math.max(wx, Math.max(bx, dx));
    var y = Math.max(wy, Math.max(by, dy));

    var speed = 1 + acceleration;
    window.scrollTo(Math.floor(x / speed), Math.floor(y / speed));
    if(x > 0 || y > 0) {
        var invokeFunction = "top.goTop(" + acceleration + ", " + time + ")"
        window.setTimeout(invokeFunction, time);
    }
    return false;
}

//*
function scrollTop(){
    var el = $('gotop');
    var stop = (document.body.scrollTop || document.documentElement.scrollTop);
    if(stop>400){
        if(el.style.display!='block'){
            el.style.display='block';
            smoothopaque(el, 0, 100, 1);
        }
    }
    else
        el.style.display='none';

    return false;
}
// Плавная смена прозрачности
function smoothopaque(el, startop, endop, inc){
    op = startop;
    // Устанавливаем прозрачность
    setopacity(el, op);
    // Начинаем изменение прозрачности
    setTimeout(slowopacity, 1);
    function slowopacity(){
        if(startop < endop){
            op = op + inc;
            if(op < endop){
                setTimeout(slowopacity, 1);
            }
        }else{
            op = op - inc;
            if(op > endop){
                setTimeout(slowopacity, 1);
            }
        }
        setopacity(el, op);
    };
};
// установка opacity
function setopacity(el, opacity){
    el.style.opacity = (opacity/100);
    el.style.filter = 'alpha(opacity=' + opacity + ')';
};

if (window.addEventListener){
    window.addEventListener("scroll", scrollTop, false);
    window.addEventListener("load", scrollTop, false);
}
else if (window.attachEvent){
    window.attachEvent("onscroll", scrollTop);
    window.attachEvent("onload", scrollTop);
}
window['top'] = {};
window['top']['goTop'] = goTop;
})();