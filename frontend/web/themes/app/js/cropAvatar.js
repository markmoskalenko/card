jQuery(function($){

    var jcrop_api,
        boundx,
        boundy,
        rx,ry,
        ctest,

        $preview = $('#preview-pane'),
        $pcnt = $('#preview-pane .preview-container'),
        $pimg = $('#preview-pane .preview-container img'),

        xsize = $pcnt.width(),
        ysize = $pcnt.height(),

        width = $pimg.width(),
        height = $pimg.height();



console.log(width, height);

    $(".process_one_photo").css({width: width + 'px !important', height: height + 'px !important'});
    $(".podl").css({width: width + 'px !important', height: height + 'px !important'});
    $(".process_one_photo .podl img").css({width: width + 'px !important', height: height + 'px !important'});

    $(".jcrop-holder").css({width: width + 'px !important', height: height + 'px !important'});
    $(".jcrop-holder div div img").css({width: width + 'px !important', height: height + 'px !important'});

    $(".jcrop-holder .jcrop-tracker").css({width: width + 'px !important', height: height + 'px !important'});

    $(".jcrop-holder img, img.jcrop-preview").css({width: width + 'px !important', height: height + 'px !important'});

    $(".jcrop-preview").css({width: width + 'px !important', height: height + 'px !important'});
    $("#preview-pane .preview-container").css({width: 150 + 'px !important', height: 150 + 'px !important'});



    $('#target').Jcrop({
        onChange: updatePreview,
        onSelect: updatePreview,
        bgFade:     true,
        bgOpacity: .5,
        //boxWidth: 300,
        //boxHeight: 300,
        aspectRatio: xsize / ysize,
        minSize:  [ 150, 150 ], //минимальный размер превью
        setSelect: [ 0, 0, 150, 150 ] //исходное состояние рамки на изображении

    },function(){

        // Use the API to get the real image size
        var bounds = this.getBounds();
        boundx = bounds[0];
        boundy = bounds[1];
        // Store the API in the jcrop_api variable
        jcrop_api = this;

        // Move the preview into the jcrop container for css positioning
        $preview.appendTo(jcrop_api.ui.holder);
    });

    function updatePreview(c)
    {




        //$(".jcrop-holder img, img.jcrop-preview").css({width: width + 'px !important', height: height + 'px !important'});
        if (parseInt(c.w) > 0)
        {

            ctest = c;
            rx = xsize / c.w;
            ry = ysize / c.h;

            $pimg.css({
                width: Math.round(rx * boundx) + 'px',
                height: Math.round(ry * boundy) + 'px',
                marginLeft: '-' + Math.round(rx * c.x) + 'px',
                marginTop: '-' + Math.round(ry * c.y) + 'px'
            });

            var imageName = document.getElementById("target");

            var height = $('.jcrop-tracker').height();
            var width = $('.jcrop-tracker').width();

            var position = $('.jcrop-holder div').position();
            var src = imageName.src;
            var li = src.lastIndexOf("/");
            var fileName;
            if (li == -1) {
                fileName = src;
            } else {
                fileName = src.substring(li + 1);
            }
            document.getElementById('user-x').value = position.left;
            document.getElementById('user-y').value = position.top;
            document.getElementById('user-width').value = width;
            document.getElementById('user-height').value = height;
            document.getElementById('user-filename').value = fileName;
        }
    }
});


