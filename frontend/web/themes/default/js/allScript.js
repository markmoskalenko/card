          $(document).ready(function() {
			  $(".fancybox").fancybox({
				  
				  beforeShow: function () {
					if (this.title) {
						// New line
					this.title += '<span/>';
						
						// Add Like button
					this.title += '<a href="" class="btn btnLike pull-right" data-toggle="buttons-checkbox"><i class="fa fa-thumbs-o-up"></i>Like</a> ';
					}
				},
				afterShow: function() {
					// Render Like button
					twttr.widgets.load();
				},
				helpers : {
					title : {
						type: 'inside'
					}
				},  
			  wrapCSS    : 'fancybox-custom',
			  closeClick : true,
			  openEffect : 'elastic',
			  openSpeed  : 200,

			  closeEffect : 'elastic',
			  closeSpeed  : 200,
			  
				 helpers : {
				  title : {
					  type : 'inside'
				  },
				  overlay : {
					  css : {
						  'background' : 'rgba(238,238,238,0.85)'
					  }
				  }  
			  }
			  });			
		  });
		  

		  $(window).load(function(){
			  var $container = $('.portfolioContainer');
			  $container.isotope({
				  filter: '*',
				  animationOptions: {
					  duration: 750,
					  easing: 'linear',
					  queue: false
				  }
			  });
		   
			  $('.portfolioFilter a').click(function(){
				  $('.portfolioFilter .current').removeClass('current');
				  $(this).addClass('current');
		   
				  var selector = $(this).attr('data-filter');
				  $container.isotope({
					  filter: selector,
					  animationOptions: {
						  duration: 750,
						  easing: 'linear',
						  queue: false
					  }
				   });
				   return false;
			  }); 
		  });
		  
		  $(document).ready(function() {
              $('.progress-bar').progressbar();
           });