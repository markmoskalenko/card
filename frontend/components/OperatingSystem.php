<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 18.09.2014
 * Time: 10:02
 */

namespace frontend\components;


class OperatingSystem {

    public static function detect() {
        $userAgent = strtolower($_SERVER['HTTP_USER_AGENT']);

        if (preg_match('/linux/', $userAgent)) {
            $platform = 'linux';
        }
        elseif (preg_match('/macintosh|mac os x/', $userAgent)) {
            $platform = 'mac';
        }
        elseif (preg_match('/windows|win32/', $userAgent)) {
            $platform = 'windows';
        }
        else {
            $platform = 'unrecognized';
        }

        return array(
            'platform'  => $platform,
            'userAgent' => $userAgent
        );
    }
}