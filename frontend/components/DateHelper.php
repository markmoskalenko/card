<?php
/**
 * Created by PhpStorm.
 * User: asus
 * Date: 06.08.14
 * Time: 13:27
 */

namespace frontend\components;


class DateHelper {

    private static $_years = [
        '1'=> '1975',
        '2'=> '1976',
        '3'=> '1977',
        '4'=> '1978',
        '5'=> '1979',
        '6'=> '1980',
        '7'=> '1981',
        '8'=> '1982',
        '9'=> '1983',
        '10'=> '1984',
        '11'=> '1985',
        '12'=> '1986',
        '13'=> '1987',
        '14'=> '1988',
        '15'=> '1989',
        '16'=> '1990',
        '17'=> '1991',
        '18'=> '1992',
        '19'=> '1993',
        '20'=> '1994',
        '21'=> '1995',
        '22'=> '1996',
        '23'=> '1997',
        '24'=> '1998',
        '25'=> '1999',
        '26'=> '2001',
        '27'=> '2002',
        '28'=> '2003',
        '29'=> '2004',
    ];

    private static $_month = [
        '1'=> 'Январь',
        '2'=> 'Февраль',
        '3'=> 'Март',
        '4'=> 'Апрель',
        '5'=> 'Май',
        '6'=> 'Июнь',
        '7'=> 'Июль',
        '8'=> 'Август',
        '9'=> 'Сентябрь',
        '10'=> 'Октябрь',
        '11'=> 'Ноябрь',
        '12'=> 'Декабрь',];

    private static $_days= [
        '1'=> '1',
        '2'=> '2',
        '3'=> '3',
        '4'=> '4',
        '5'=> '5',
        '6'=> '6',
        '7'=> '7',
        '8'=> '8',
        '9'=> '9',
        '10'=> '10',
        '11'=> '11',
        '12'=> '12',
        '13'=> '13',
        '14'=> '14',
        '15'=> '15',
        '16'=> '16',
        '17'=> '17',
        '18'=> '18',
        '19'=> '19',
        '20'=> '20',
        '21'=> '21',
        '22'=> '22',
        '23'=> '23',
        '24'=> '24',
        '25'=> '25',
        '26'=> '26',
        '27'=> '27',
        '28'=> '28',
        '29'=> '29',
        '30'=> '30',
        '31'=> '31',
    ];


    public static function getDays(){
        return static::$_days;
    }

    public static function getMonth(){
        return static::$_month;
    }
    public static function getYears(){
        return static::$_years;
    }

    public static function getDayByCod($code){
        return static::$_days[$code];
    }

    public static function getMonthByCod($code){
        return static::$_month[$code];
    }
    public static function getYearsByCod($code){
        return static::$_years[$code];
    }
} 