<?php
namespace frontend\components;


use common\models\User;

class View extends \yii\web\View {

    public $isVisibleHeader3 = true;

    public $message;

    public function init(){

       $this->setTheme();

       parent::init();
   }

    public function beforeRender(){

        if( !\Yii::$app->user->isGuest && empty(User::u()->email)){
            $this->registerJs('setEmail();');
        }

        return parent::beforeRender();
    }

    public function setTheme($name = 'app'){
        $this->theme = [
            'class'   => 'yii\base\Theme',
            'pathMap' => ['@app/views' => '@app/themes/'.$name],
            'baseUrl' => '@web/themes/'.$name,
        ];

        $this->theme = \Yii::createObject($this->theme);
    }
}