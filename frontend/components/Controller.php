<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 28.08.14
 * Time: 14:22
 */

namespace frontend\components;


use common\models\User;
use yii\helpers\Html;

class Controller extends \yii\web\Controller{

    public $enableCsrfValidation = false;

    public function init(){
        if( ! \Yii::$app->user->isGuest ){

            if( User::u()->status == User::STATUS_DELETED )
                $this->getView()->message = \Yii::t('app', 'Вы заблокированы админестрацией сайта');
            elseif( User::u()->status == User::STATUS_TEST )
                $this->getView()->message = \Yii::t('app', 'Тестовый режим до ').date('d-m-Y', User::u()->expire_date).'. '.Html::a(\Yii::t('app', 'Купить PRO аккаунт'), '/user/buy-pro');
            elseif( User::u()->status == User::STATUS_ACTIVE )
                $this->getView()->message = \Yii::t('app', 'Оплачено до ').date('d-m-Y', User::u()->expire_date);
            elseif( User::u()->status == User::STATUS_NOT_PAID )
                $this->getView()->message = \Yii::t('app', 'Аккаунт заблокирован').'. '.Html::a(\Yii::t('app', 'Купить PRO аккаунт'), '/user/buy-pro');

        }
        parent::init();
    }

}