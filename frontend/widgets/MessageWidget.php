<?php
namespace frontend\widgets;
use common\models\Message;

class MessageWidget extends \yii\bootstrap\Widget
{
    public $customId;

    public function init(){}

    public function run() {
        $model = new Message();

        $model->to_user_id = $this->customId;

        return $this->render('message/form', [
            'model' => $model,
        ]);
    }
}