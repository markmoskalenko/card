<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

?>
<div class="col-sm-5">

    <h2><?=Yii::t('app', 'Отправить сообщение пользователю')?></h2>
    <p style="color: green;">
        <?= Yii::$app->session->getFlash('messageSent'); ?>
    </p>

    <div class="message-form">

        <?php $form = ActiveForm::begin(['action' => Url::to('message/send'), 'options' => ['enctype'=>'multipart/form-data']]);?>

        <?= $form->field($model, 'to_user_id', array('template' => "{input}"))->hiddenInput() ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>

        <?= $form->field($model, 'email')->textInput(['maxlength' => 255]) ?>

        <?= $form->field($model, 'text')->textarea(['maxlength' => 1024]) ?>

        <br />

        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Отправить'), ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>

