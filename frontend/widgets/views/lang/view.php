<?php
use yii\helpers\Html;
?>
<?php foreach ($langs as $lang):?>
    <a href="/<?= $lang->url.Yii::$app->getRequest()->getUrl() ?>">
        <img src="/themes/app/img/flag/<?= $lang->url ?>.jpg" alt=""/>
    </a>
<?php endforeach;?>
