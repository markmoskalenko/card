<?php

namespace frontend\controllers;

use common\models\Pages;
use yii\web\NotFoundHttpException;
use Yii;

class PageController extends \frontend\components\Controller
{
    public function actionView($slug)
    {
        $this->view->isVisibleHeader3 = false;

        $page = Pages::find()->andWhere(['slug'=>$slug])->one();

        if( !$page )
            throw new NotFoundHttpException(\Yii::t('app', 'Запрошенная страница не существует.'));

//        Yii::$app->clientScript->registerMetaTag($page->meta_d, 'description');
//        Yii::$app->clientScript->registerMetaTag($page->meta_k, 'keywords');

        return $this->render('view', [
            'model' => $page,
        ]);
    }

}
