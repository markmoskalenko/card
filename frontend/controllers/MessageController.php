<?php

namespace frontend\controllers;

use common\models\Message;
use common\models\User;
use Yii;

class MessageController extends \frontend\components\Controller
{
    /**
     * @param int $category
     * @return string
     */
    public function actionList($category = Message::CATEGORY_MAIN)
    {
        $model = new Message();

        Message::updateAll(['is_read'=>1], ['to_user_id' => Yii::$app->user->id, 'category' => $category]);

        $messages = Message::find()
            ->where(['to_user_id' => Yii::$app->user->id, 'category' => $category])
            ->orderBy(['date' => SORT_DESC])
            ->all();

        $counts = [
            Message::CATEGORY_MAIN => Message::find()
                      ->where(['to_user_id' => Yii::$app->user->id, 'category' => Message::CATEGORY_MAIN])
                      ->asArray()
                      ->count(),
            Message::CATEGORY_MYRES => Message::find()
                      ->where(['to_user_id' => Yii::$app->user->id, 'category' => Message::CATEGORY_MYRES])
                      ->asArray()
                      ->count(),

            Message::CATEGORY_ARCHIVE => Message::find()
                      ->where(['to_user_id' => Yii::$app->user->id, 'category' => Message::CATEGORY_ARCHIVE])
                      ->asArray()
                      ->count(),

        ];

        return $this->render('list', [
            'model' => $model,
            'messages' => $messages,
            'counts' => $counts,
            'category' => $category,
        ]);
    }

    /**
     * Lists all Invoice models.
     * @return mixed
     */
    public function actionArchive($id)
    {
        $model = new Message();
        $message = $model->findOne($id);

        $category = $message->category;
        $message->category = Message::CATEGORY_ARCHIVE;
        $message->save();

        return $this->redirect('/message/list?category='.$category);
    }

    /**
     * Lists all Invoice models.
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = new Message();
        $message = $model->findOne($id);

        $category = $message->category;
        $message->delete();

        return $this->redirect('/message/list?category='.$category);
    }

    /**
     * Lists all Invoice models.
     * @return mixed
     */
    public function actionSend()
    {
        $model = new Message();
        $model->date = date('Y-m-d H:i:s');
        $model->is_read = 0;
        $model->category = 1;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            User::sendCommingMessageEmail($model);
            Yii::$app->getSession()->setFlash('messageSent', \Yii::t('app','Ваше сообщение отправлено.'));
        }
        return $this->redirect(Yii::$app->params['scheme'].User::u()->subdomain.'.'.Yii::$app->params['baseDomain']);
    }
}
