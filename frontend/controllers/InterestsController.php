<?php

namespace frontend\controllers;

use common\models\DictionaryInterests;
use frontend\models\Interests;
use Yii;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

class InterestsController extends \frontend\components\Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['create', 'update', 'delete', 'pub'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionCreate(){

        $this->layout = 'iframe';

        $oInterests = new \frontend\models\Interests();

        $aInterests = DictionaryInterests::getByAutocomlete();

        if( $oInterests->load($_POST) && $oInterests->save() ){

            return $this->render('success');
        }

        return $this->render('form', compact('oInterests', 'aInterests'));

    }

    public function actionUpdate( $id ){

        $this->layout = 'iframe';

        $oInterests = $this->findModel($id);

        $aInterests = DictionaryInterests::getByAutocomlete();

        if( $oInterests->load($_POST) && $oInterests->save() ){

            return $this->render('success');

        }

        return $this->render('form', compact('oInterests', 'aInterests'));

    }

    public function actionDelete($id){
        $this->findModel($id)->delete();
    }

    public function actionPub($id, $val){
        $oModel = $this->findModel($id);
        $oModel->is_public = (int) $val;
        $oModel->save();
        Yii::$app->end();
    }
    /**
     * Finds the DictionaryCompany model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return DictionaryCompany the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Interests::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(\Yii::t('app', 'Запрошенная страница не существует.'));
        }
    }
}