<?php

namespace frontend\controllers;

use common\models\DictionarySkills;
use frontend\models\Skills;
use Yii;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

class SkillsController extends \frontend\components\Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['create', 'update', 'delete', 'pub'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }


    public function actionCreate(){

        $this->layout = 'iframe';

        $oSkills = new \frontend\models\Skills();

        $aSkills = DictionarySkills::getByAutocomlete();

        if( $oSkills->load($_POST) && $oSkills->save() ){

            return $this->render('success');
        }

        return $this->render('form', compact('oSkills', 'aSkills'));

    }

    public function actionUpdate( $id ){

        $this->layout = 'iframe';

        $oSkills = $this->findModel($id);

        $aSkills = DictionarySkills::getByAutocomlete();

        if( $oSkills->load($_POST) && $oSkills->save() ){

            return $this->render('success');

        }

        return $this->render('form', compact('oSkills', 'aSkills'));

    }

    public function actionDelete($id){
        $this->findModel($id)->delete();
    }

    public function actionPub($id, $val){
        $oModel = $this->findModel($id);
        $oModel->is_public = (int) $val;
        $oModel->save();
        Yii::$app->end();
    }

    /**
     * Finds the DictionaryCompany model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return DictionaryCompany the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Skills::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(\Yii::t('app', 'Запрошенная страница не существует.'));
        }
    }
}
