<?php

namespace frontend\controllers;

use common\models\City;
use yii\web\NotFoundHttpException;
use Yii;
use yii\web\Response;

class CityController extends \frontend\components\Controller
{
    /**
     * Экшен для Ajax запроса.
     * Результат массив в JSon формате содержащий мануфактуры
     *
     * @param $id
     *
     * @return array
     *
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionGetCityByCountry($id){

        if( !\Yii::$app->request->isAjax )
            throw new NotFoundHttpException(\Yii::t('app', 'Страница не найдена.'));

        Yii::$app->response->format = Response::FORMAT_JSON;

        return City::getMapByCountryId( $id );
    }
}
