<?php
/**
 * Created by PhpStorm.
 * User: asus
 * Date: 13.08.14
 * Time: 10:33
 */

namespace frontend\controllers;
use common\models\User;
use yii\imagine\Image;
use liao0007\yii2\cropimage;
use Imagine\Image\Point;
use Yii;
use yii\web\Response;


class UploadController extends \frontend\components\Controller {

    public function actionUpload(){

        $oUser = User::u();

        $oUser->setScenario('upload');

        if( isset($_FILES['Filedata']) ){
            Yii::$app->response->format = Response::FORMAT_JSON;
            $result = $oUser->saveImg();
            $oUser->save();
            return $result;
        }
    }

    /**
     * Кроп аватарки
     * @return array
     */
    public function actionCropping(){

        $oUser = User::u();
        $oUser->setScenario('upload');

        $sSmallImageDirectory = Yii::getAlias('@frontend/web/upload/150x150');

        if(Yii::$app->request->isPost){
            if (isset($_POST['User']) && $_POST['User'] != '') {

                $oUser->x= intval($_POST['User']['x']);
                $oUser->y= intval($_POST['User']['y']);
                $oUser->width= intval($_POST['User']['width']);
                $oUser->height= intval($_POST['User']['height']);
                $oUser->filename= $_POST['User']['filename'];

                if (!is_file($sSmallImageDirectory.DIRECTORY_SEPARATOR.$oUser->filename)) {

                    return ['status' => 0];

                } else {

                    Image::crop($sSmallImageDirectory.DIRECTORY_SEPARATOR.$oUser->filename,  $oUser->width,  $oUser->height, [$oUser->x, $oUser->y])->save(
                        $sSmallImageDirectory.DIRECTORY_SEPARATOR.$oUser->filename);

                    if ($oUser->save()) {
                        return $this->redirect('/site/index');
                    }
                }
            }
        }

        Yii::$app->response->format = Response::FORMAT_HTML;
        return $this->render('cropping', compact('oUser'));
    }

    public function actionDelete(){
        $oUser = User::u();
        $oUser->setScenario('upload');
        if($oUser->avatar){
            $sSmallImageDirectory = Yii::getAlias('@frontend/web/upload/150x150');
            $originalImageDirectory = Yii::getAlias('@frontend/web/upload/original');

            $small = $sSmallImageDirectory.DIRECTORY_SEPARATOR.$oUser->avatar;
            $original = $originalImageDirectory . DIRECTORY_SEPARATOR . $oUser->avatar;

            if( is_file($small) && is_file($original)) {
                unlink($small);
                unlink($original);
            }
        }
        $oUser->avatar='';
        $oUser->save();
        $this->redirect(['/site/index']);
    }
}