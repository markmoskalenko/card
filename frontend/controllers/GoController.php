<?php

namespace frontend\controllers;

use common\models\Themes;
use common\models\User;
use frontend\components\OperatingSystem;
use Heartsentwined\Vcard\Service\Vcard;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use Yii;

class GoController extends \frontend\components\Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only'=>['view'],
                'rules' => [
                    [
                        'actions' => ['view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex( $alias )
    {
        $oUser = User::find()->andWhere(['subdomain' => $alias])->with(['currentTheme'])->one();

        if(!$oUser || !$oUser->currentTheme) throw new NotFoundHttpException(\Yii::t('app', 'Страница не найдена.'));

        $this->view->setTheme( $oUser->currentTheme->system_name );

        if($oUser->status == User::STATUS_DELETED) return $this->render('message', ['message'=>\Yii::t('app', 'Страница пользователя заблокирована админестрацией сайта.')]);

        if($oUser->status == User::STATUS_NOT_PAID) return $this->render('message', ['message'=>\Yii::t('app', 'Для разблокировки страницы оплатите PRO аккаунт.')]);

        if($oUser->subdomain){
            return $this->render('index', compact('oUser'));
        }else{
            throw new NotFoundHttpException(\Yii::t('app', 'Запрошенная страница не существует.'));
        }

    }

    public function actionVcard($alias)
    {
        $vcard = new \frontend\components\VCard();

        $oUser = User::find()->andWhere(['subdomain' => $alias])->with(['currentTheme'])->one();

        if(!$oUser || !$oUser->currentTheme) throw new NotFoundHttpException(\Yii::t('app', 'Страница не найдена.'));

        $this->view->setTheme( $oUser->currentTheme->system_name );

        if($oUser->status == User::STATUS_DELETED) return $this->render('message', ['message'=>\Yii::t('app', 'Страница пользователя заблокирована админестрацией сайта.')]);

        if($oUser->status == User::STATUS_NOT_PAID) return $this->render('message', ['message'=>\Yii::t('app', 'Для разблокировки страницы оплатите PRO аккаунт.')]);

        if( !empty( $oUser->username ) || !empty( $oUser->surname )){
            $vcard->setFormattedName($oUser->username);
        }

        if( !empty( $oUser->username ) || !empty( $oUser->surname )){
            $vcard->setName($oUser->surname,$oUser->username,"","");
        }
        if( !empty( $oUser->phone_mob ) ) {
            $vcard->setPhoneNumber($oUser->phone_mob, "PREF;WORK;VOICE");
        }

        if( !empty( $oUser->phone ) ) {
            $vcard->setPhoneNumber($oUser->phone, "PREF;HOME;VOICE");
        }
        if( !empty( $oUser->profession ) ) {
            $vcard->setTitle($oUser->profession);
        }

        if( !empty( $oUser->email ) ) {
            $vcard->setEmail($oUser->email);
        }

        if( !empty( $oUser->subdomain)){
            $vcard->setURL('http://'.$oUser->subdomain.'.myres.pro', "WORK");
        }

        $sSmallImageDirectory = Yii::getAlias('@frontend/web/upload/150x150');

        if( is_file($sSmallImageDirectory.DIRECTORY_SEPARATOR.$oUser->avatar)){
            $image =$sSmallImageDirectory.DIRECTORY_SEPARATOR.$oUser->avatar;
        }
        else{
            $image = Yii::getAlias('@frontend/web/themes/app/img/profile.jpg');
        }

        $imagetypes = array
        (
            IMAGETYPE_JPEG => 'JPEG',
            IMAGETYPE_GIF  => 'GIF',
            IMAGETYPE_PNG  => 'PNG',
            IMAGETYPE_BMP  => 'BMP'
        );
        if (is_file($image))
        {
            $imageinfo = @getimagesize($image);
            $type  = $imagetypes[$imageinfo[2]];
            $vcard->setPhoto($type,file_get_contents($image));
        }

        if($oUser->subdomain){
            $output = $vcard->getVCard();

            $os = OperatingSystem::detect();

            if($os['platform'] == 'windows'){
                $output = iconv("UTF-8", "cp1251", $output);
            }
            $filename = $vcard->getFileName();
            Header("Content-Disposition: attachment; filename=$filename");
            Header("Content-Length: ".strlen($output));
            Header("Connection: close");
            Header("Content-Type: text/x-vCard; name=$filename");

            echo $output;
            exit;
        }else{
            throw new NotFoundHttpException(\Yii::t('app', 'Запрошенная страница не существует.'));
        }

    }

    public function actionView( $template )
    {

        $oTheme = Themes::findOne((int)$template);

        if( !$oTheme )
            throw new NotFoundHttpException(\Yii::t('app', 'Тема не найдена.'));

        $this->view->setTheme( $oTheme->system_name );

        $oUser = User::u();

        return $this->render('index', compact('oUser'));
    }
}
