<?php

namespace frontend\controllers;

use Yii;
use yii\filters\AccessControl;

class CompanyController extends \frontend\components\Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['search'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

//    public function actionSearch($query){
//
//        if( !\Yii::$app->request->isAjax )
//            throw new NotFoundHttpException('Страница не найдена.');
//
//        Yii::$app->response->format = Response::FORMAT_JSON;
//
//        return DictionaryCompany::findByTerm($query);
//    }
}
