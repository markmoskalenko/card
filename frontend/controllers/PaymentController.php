<?php
namespace frontend\controllers;

use common\models\Invoice;
use yii\filters\VerbFilter;
use Yii;
use yii\web\BadRequestHttpException;

class PaymentController extends \frontend\components\Controller
{
    public $enableCsrfValidation = false;

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'success' => ['post'],
                    'result' => ['post'],
                    'fail' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Invoice models.
     * @return mixed
     */
    public function actionList()
    {
        $model = new Invoice();
        $dataProvider = $model->search(Yii::$app->request->queryParams, Yii::$app->user->id);

        return $this->render('list', [
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionInvoice()
    {
        $model = new Invoice();
        $model->user_id = Yii::$app->user->id;
        $model->status = Invoice::STATUS_PENDING;
        $model->type = Invoice::TYPE_INCREMTNT;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            /** @var \robokassa\Merchant $merchant */
            $merchant = Yii::$app->get('robokassa');
            return $merchant->payment($model->sum, $model->id, 'Пополнение баланса', null, Yii::$app->user->identity->email);
        } else {
            return $this->render('invoice', [
                'model' => $model,
            ]);
        }
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'result' => [
                'class' => '\robokassa\ResultAction',
                'callback' => [$this, 'resultCallback'],
            ],
            'success' => [
                'class' => '\robokassa\SuccessAction',
                'callback' => [$this, 'successCallback'],
            ],
            'fail' => [
                'class' => '\robokassa\FailAction',
                'callback' => [$this, 'failCallback'],
            ],
        ];
    }

    /**
     * Callback.
     * @param \robokassa\Merchant $merchant merchant.
     * @param integer $nInvId invoice ID.
     * @param float $nOutSum sum.
     * @param array $shp user attributes.
     */
    public function successCallback($merchant, $nInvId, $nOutSum, $shp)
    {
//        $this->loadModel($nInvId)->updateAttributes(['status' => Invoice::STATUS_ACCEPTED]);
        if (Invoice::addInvoice($nOutSum, Yii::t('app', 'Пополнение баланса'), Invoice::STATUS_SUCCESS, Invoice::TYPE_INCREMTNT, $nInvId)){
            return $this->goBack();
        } else {
            $model = Invoice::findOne(['id' => $nInvId]);
            $model->updateAttributes(['status' => Invoice::STATUS_SITEERROR]);
        }
        return $this->redirect('/payment/list');
    }
    public function resultCallback($merchant, $nInvId, $nOutSum, $shp)
    {
//        $this->loadModel($nInvId)->updateAttributes(['status' => Invoice::STATUS_SUCCESS]);
        Invoice::addInvoice($nOutSum, Yii::t('app', 'Пополнение баланса'), Invoice::STATUS_SUCCESS, Invoice::TYPE_INCREMTNT, $nInvId);
        return 'Ok';
    }
    public function failCallback($merchant, $nInvId, $nOutSum, $shp)
    {
//        $model = $this->loadModel($nInvId);
        $model = Invoice::findOne(['id' => $nInvId]);
        if ($model->status == Invoice::STATUS_PENDING) {
//            $model->updateAttributes(['status' => Invoice::STATUS_FAIL]);
            Invoice::addInvoice($nOutSum, Yii::t('app', 'Пополнение баланса'), Invoice::STATUS_FAIL, Invoice::TYPE_INCREMTNT, $nInvId);
            Yii::$app->getSession()->setFlash('error', \Yii::t('app','К сожалению, вы не смогли пополнить баланс.'));
            return $this->redirect('/payment/invoice');
        } else {
            return \Yii::t('app','Статус не изменился') ;
        }
    }

    /**
     * @param integer $id
     * @return Invoice
     * @throws \yii\web\BadRequestHttpException
     */
    protected function loadModel($id) {
        $model = Invoice::find($id);
        if ($model === null) {
            throw new BadRequestHttpException;
        }
        return $model;
    }
}