<?php
namespace frontend\controllers;

use common\models\City;
use common\models\Country;
use common\models\User;
use frontend\models\Education;
use frontend\models\Experience;
use frontend\models\Interests;
use frontend\models\Skills;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;
use Yii;

/**
 * Site controller
 */
class SiteController extends \frontend\components\Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['test', 'signup', 'request-password-reset', 'reset-password', 'error', 'get-skill', 'get-interest'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['test', 'logout', 'index', 'get-works', 'get-educations', 'error', 'get-skill', 'get-interest'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        if (Yii::$app->user->isGuest){
            $this->redirect('/user/login');
        }

        $oUser = clone User::u();
        $oUser->setScenario('head');

        $oUserAbout = clone User::u();
        $oUserAbout->setScenario('profile');

        $oUserContact = clone User::u();
        $oUserContact->setScenario('contact');

        $oUserSocial = clone User::u();
        $oUserSocial->setScenario('social');

        $oCity =[];
        $oCountry = Country::getTreeMap();

        $scenario = isset($_POST['User']['validateScenario']) ? $_POST['User']['validateScenario']:false;

        if( $scenario ){
            switch ($scenario){

                case 'profile' :
                    return $this->saveProfile($oUserAbout);
                    break;

                case 'contact' :
                    return $this->saveProfile($oUserContact);
                    break;

                case 'social' :
                    return $this->saveProfile($oUserSocial);
                    break;

                default :
                    throw new NotFoundHttpException(\Yii::t('app', 'Страница не найдена.'));

            }
        }

        if( !is_null( $oUserContact->country_id ) ){
            $oCity = City::getMapByCountryId($oUserContact->country_id);
        }

        return $this->render('index', compact('oUserAbout', 'oUserContact', 'oUserSocial', 'oCountry', 'oCity', 'oUser'));
    }

    /**
     * @param User $oModel
     * @return array
     */
    protected function saveProfile( $oModel ){

        if( $oModel->load($_POST)){

            Yii::$app->response->format = Response::FORMAT_JSON;

            if( $oModel->save() ){
                return ['status'=>'success'];

            }else{
                return ActiveForm::validate($oModel);
            }
        }

        return ActiveForm::validate($oModel);

    }

//    public function actionError(){
//        $this->layout = 'error';
//        return $this->render('error');
//    }

    public function actionGetWorks(){

        Yii::$app->response->format = Response::FORMAT_JSON;

        return Experience::getForAngularJs();
    }

    public function actionGetEducations(){

        Yii::$app->response->format = Response::FORMAT_JSON;

        return Education::getForAngularJs();
    }

    public function actionGetSkill(){

        Yii::$app->response->format = Response::FORMAT_JSON;

        return Skills::getForAngularJs();
    }

    public function actionGetInterest(){

        Yii::$app->response->format = Response::FORMAT_JSON;

        return Interests::getForAngularJs();
    }

    public function actionAbout()
    {
        return $this->render('about');
    }
}


//$auth = \Yii::$app->getAuthManager();
//
//$rule = new \common\rbac\UserRule();
//$auth->add($rule);
//$user = $auth->createRole('user');
//$user->ruleName = $rule->name;
//$auth->add($user);
//
//
//$rule = new \common\rbac\AdministratorRule();
//$auth->add($rule);
//$admin = $auth->createRole('administrator');
//$admin->ruleName = $rule->name;
//$auth->add($admin);
//
//$auth->addChild($admin, $user);
//
//$auth->save();