<?php
namespace frontend\controllers;

use common\models\LoginForm;
use common\models\User;
use yii\filters\AccessControl;
use Yii;

/**
 * User controller
 */
class UserController extends \frontend\components\Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup', 'request-password-reset', 'reset-password'],
                'rules' => [
                    [
                        'actions' => ['signup', 'request-password-reset', 'reset-password'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout', 'set-email', 'buy-pro'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'eauth' => array(
                'class' => \nodge\eauth\openid\ControllerBehavior::className(),
                'only' => array('login-eauth'),
            ),
        ];
    }

    public function actionBuyPro($period=null){

        if($period != null){
            if( $period == User::PAY_PERIOD_1 )
                User::u()->buyPeriod( User::PAY_PERIOD_1_PRICE, User::PAY_PERIOD_1  );
            $this->redirect('/');
        }
        return $this->render('buyPro');
    }

    /**
     * Авторизация
     * @return string|\yii\web\Response
     */
    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Авторизация СоцСети
     */
    public function actionLoginEauth()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $serviceName = Yii::$app->getRequest()->getQueryParam('service');

        if (isset($serviceName)) {

            $eAuth = Yii::$app->get('eauth')->getIdentity($serviceName);

            $eAuth->setRedirectUrl('/');

            $eAuth->setCancelUrl(Yii::$app->getUrlManager()->createAbsoluteUrl('user/login'));

            try {
                if ($eAuth->authenticate()) {

                    $identity = \common\models\User::findIdentityByEAuth($eAuth);

                    Yii::$app->getUser()->login($identity);

                    $eAuth->redirect();
                }
                else {
                    $eAuth->cancel();
                }
            }
            catch (\nodge\eauth\ErrorException $e) {
                Yii::$app->getSession()->setFlash('error', 'EAuthException: '.$e->getMessage());
                $eAuth->redirect($eAuth->getCancelUrl());
            }
        }
    }

    /**
     * Регистрация
     * @return string|\yii\web\Response
     */
    public function actionSignup()
    {
        $oModel = new User();

        $oModel->setScenario('signup');

        if ( $oModel->load($_POST) && $oModel->signup() ) {

            if (Yii::$app->getUser()->login($oModel)) {

                $oModel->sendActivateEmail();

                return $this->redirect(['site/index']);
            }
        }

        return $this->render('signup', compact('oModel'));
    }

    /**
     * Выход
     * @return \yii\web\Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Восстановление пароля
     * @return string|\yii\web\Response
     */
    public function actionRequestPasswordReset()
    {
        $model = new User();
        $model->setScenario('requestPasswordResetToken');

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendResetPasswordRequestEmail()) {
                Yii::$app->getSession()->setFlash('success', \Yii::t('app','Проверьте свою электронную почту для получения дальнейших инструкций.'));
            } else {
                Yii::$app->getSession()->setFlash('error', \Yii::t('app','К сожалению, вы не можете сбросить пароль для данной электронной почты.'));
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Сброс пароля
     * @param $token
     *
     * @return string|\yii\web\Response
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        $model = new User();

        $model->setScenario('resetPassword');

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            if( $model->save() ){
                Yii::$app->getSession()->setFlash('success', \Yii::t('app','Пароль успешно сохранен'));

                return $this->goHome();
            }else
                Yii::$app->getSession()->setFlash('error', \Yii::t('app','Не удалось сохранить пароль.'));
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * Смена почты
     * @return string
     */
    public function actionSetEmail(){

        $this->layout = 'iframe';

        $oUser = User::u();

        if(!empty($oUser->email)){

            $sMessage = \Yii::t('app','Вы уже указали почтовый ящик');

            return $this->render('setEmailSuccess', compact('oUser','sMessage'));
        }


        $oUser->setScenario('setEmail');

        if ( $oUser->load($_POST) && $oUser->save() ) {

            $oUser->sendActivateEmail();

            return $this->render('setEmailSuccess', compact('oUser'));

        }

        return $this->render('setEmail', compact('oUser'));
    }

    /**
     * Активация
     *
     * @param $token
     *
     * @return string
     */
    public function actionActivate($token){

        $isActivate = User::activate($token);

        return $this->render('activateSuccess', compact('isActivate'));
    }

    public function actionChangePassword()
    {
        $this->layout = 'iframe';
        
        $model =  User::u();
        $model->setScenario('changePassword');

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $data = Yii::$app->request->post('User');
            if ($model->validatePassword($data['oldPassword'])){
                if ($data['password'] === $data['passwordRepeat']){
                    $model->password = $data['password'];
                    $model->setPassword($data['password']);
                    if ($model->save()){
                        Yii::$app->getSession()->setFlash('success', \Yii::t('app','Пароль изменен.'));
                    } else {
                        Yii::$app->getSession()->setFlash('error', \Yii::t('app','Ошибка! Обратитесь к администратору.'));
                    }
                } else {
                    Yii::$app->getSession()->setFlash('error', \Yii::t('app','Палоли не совпадают.'));
                }
            } else {
                Yii::$app->getSession()->setFlash('error', \Yii::t('app','Старый пароль введен не верно.'));
            }
        }
        return $this->render('change_password', [
            'model' => $model,
        ]);
    }
}