<?php

namespace frontend\controllers;

use common\models\Themes;
use common\models\User;
use common\models\UserTheme;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use Yii;

class TemplateController extends \frontend\components\Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        // Шаблоны пользователя IDS
        $aTemplateUsersIds = UserTheme::find()->andWhere(['user_id'=>User::u()->id])->select('theme_id')->asArray()->column();

        // Модели шаблона пользователя
        $oTemplatesUser = Themes::find()->andWhere(['id'=>$aTemplateUsersIds])->with(['userThemes'=>function($q){
                $q->andOnCondition(['user_id'=>User::u()->id]);
            }])->all();

        $oTemplateCurrent = false;

        foreach($oTemplatesUser as $oTemplateUser){

            if($oTemplateUser->userThemes->is_current){
                $oTemplateCurrent = $oTemplateUser;
                break 1;
            }
        }

        // Все не купленные шаблоны
        $oTemplates = Themes::find();

        // Не показывать купленные шаблоны
        if( count($aTemplateUsersIds) ) $oTemplates->andWhere(['not in', 'id', $aTemplateUsersIds]);

        $oTemplates->andWhere(['is_published'=>1]);

        $oTemplates = $oTemplates->all();

        $oUser =  User::u();

        return $this->render('index',compact('oTemplates', 'oUser', 'oTemplatesUser', 'oTemplateCurrent'));
    }

    public function actionBuy($id){

        $oUser =  User::u();

        $oTemplate = $this->findModel($id);

        $buyTemplate = $oTemplate->buy();

        return  $this->redirect('index');
    }

    public function actionAdd($id){

        $oTemplate = $this->findModel($id);
        $addTemplate = $oTemplate->add();
        return $this->render('buy');
    }

    public function actionSetCurrent($id){



        $oTemplate = UserTheme::find()->andWhere(['theme_id'=>(int)$id])->andWhere(['user_id'=>User::u()->id])->one();

        if( !$oTemplate ){
            Yii::$app->session->setFlash('error', \Yii::t('app', 'Тема не найдена'));
        }else{
            if( $oTemplate->setCurrent())
                Yii::$app->session->setFlash('success', \Yii::t('app', 'Тема успешно установлена'));
            else
                Yii::$app->session->setFlash('error', \Yii::t('app', 'Ошибка применения темы'));
        }

        return  $this->redirect('index');
    }

    /**
     * Finds the DictionaryInterests model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return DictionaryInterests the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Themes::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(\Yii::t('app', 'Запрошенная страница не существует.'));
        }
    }

}