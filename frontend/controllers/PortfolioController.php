<?php

namespace frontend\controllers;

use common\models\User;
use frontend\models\Portfolio;
use frontend\models\PortfolioCategory;
use Imagine\Image\Point;
use liao0007\yii2\cropimage;
use Yii;
use yii\filters\AccessControl;
use yii\imagine\Image;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class PortfolioController extends \frontend\components\Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'update', 'delete-category', 'pub', 'load','cropping', 'delete', 'published'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }


    public function actionIndex(){

        $oCategory = PortfolioCategory::find()->all();
        $oneCategory = PortfolioCategory::find()->one();
        $oPortfolio = new Portfolio();
        $oNewCategory = new PortfolioCategory();
        $oPortfolioItems = Portfolio::find()->all();

        if( $oNewCategory->load($_POST) && $oNewCategory->save() ){
            $this->refresh();
        }

        if(!$oneCategory){
            $oPortfolio->addError('file', \Yii::t('app', 'У вас не созданы категории'));
            return $this->render('index', compact('oCategory', 'oNewCategory', 'oPortfolioItems','oPortfolio'));
        }

        if($oPortfolio->load($_POST) ){
            $oPortfolio->category_id =$oneCategory->id;
            $oPortfolio->save();
            $errors = $oPortfolio->errors;
//            var_dump($errors);
            if($errors){
                return $this->render('index', compact('oCategory', 'oNewCategory', 'oPortfolioItems','oPortfolio'));
            }
            return $this->redirect(array('cropping', 'id' => $oPortfolio->id));
        }
        return $this->render('index', compact('oCategory', 'oNewCategory', 'oPortfolioItems','oPortfolio'));
    }


//    public function actionUpdate( $id ){
//
//        $this->layout = 'iframe';
//
//        $oSkills = $this->findModel($id);
//
//        $aSkills = DictionarySkills::getByAutocomlete();
//
//        if( $oSkills->load($_POST) && $oSkills->save() ){
//
//            return $this->render('success');
//
//        }
//
//        return $this->render('form', compact('oSkills', 'aSkills'));
//
//    }

    public function actionDeleteCategory($id){
        $this->findModelCategory($id)->delete();
        $this->redirect(['index']);
    }

    public function actionUpdate($id){
        return $this->redirect(array('cropping', 'id' => $id));
    }

    public function actionLoad(){

        $oPortfolio = new Portfolio();

        $aCategory = PortfolioCategory::getMap();
        $oCategory = PortfolioCategory::find()->one();

        if($oPortfolio->load($_POST) ){
            $oPortfolio->category_id =$oCategory->id;
            $oPortfolio->save();

            $errors = $oPortfolio->errors;

            if($errors){
                return $this->render('load', compact('aCategory', 'oPortfolio'));
            }

            return $this->redirect(array('cropping', 'id' => $oPortfolio->id));
        }
        return $this->render('load', compact('aCategory', 'oPortfolio'));
    }

    public function actionCropping($id){

        $oPortfolio = Portfolio::find()->andWhere('id=:id AND user_id=:user_id', [':id'=>$id, ':user_id'=>User::u()->id])->one();

        if($oPortfolio == NULL){
            throw new NotFoundHttpException(\Yii::t('app', 'Страница не найдена.'));
        }

        $sSmallImageDirectory = Yii::getAlias('@frontend/web/upload/200x200');
        $sCropImageDirectory = Yii::getAlias('@frontend/web/upload/crop');

        $aCategory = PortfolioCategory::getMap();

        if(Yii::$app->request->isPost){
            if(isset($_POST['Portfolio']) && $_POST['Portfolio'] !=''){
                $oPortfolio->attributes= $_POST['Portfolio'];
                $oPortfolio->name= trim($oPortfolio->name);

                if(empty($oPortfolio->name)){
                    $oPortfolio->name = '&nbsp;';
                }

                $oPortfolio->save();

                if( !is_file($sSmallImageDirectory.DIRECTORY_SEPARATOR.$oPortfolio->filename) ){
                    echo 'status = ' + 0 ;
                }else{
                    Yii::$app->response->format = Response::FORMAT_JSON;

                    Image::crop( $sSmallImageDirectory.DIRECTORY_SEPARATOR.$oPortfolio->filename , $oPortfolio->width, $oPortfolio->height, [$oPortfolio->x, $oPortfolio->y])
                        ->save($sCropImageDirectory.DIRECTORY_SEPARATOR.$oPortfolio->filename);

                    return $this->redirect('index');
                }
            }
        }

        return $this->render('crop', compact('aCategory', 'oPortfolio'));
    }

    public function actionDelete($id){
        $this->findModelPortfolio($id)->delete();
        $this->redirect(['index']);
    }

    public function actionPublished($id,$val){
        $oModel = $this->findModelPortfolio($id);
        $oModel->is_public = (int)$val;
        $oModel->save();
        Yii::$app->end();
    }

    /**
     * Finds the DictionaryCompany model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return PortfolioCategory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelCategory($id)
    {
        if (($model = PortfolioCategory::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(\Yii::t('app', 'Запрошенная страница не существует.'));
        }
    }

    /**
     * Finds the DictionaryCompany model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Portfolio the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelPortfolio($id)
    {
        if (($model = Portfolio::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
