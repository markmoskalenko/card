<?php

namespace frontend\controllers;

use common\models\City;
use common\models\Country;
use common\models\DictionaryCompany;
use common\models\DictionaryFaculties;
use common\models\DictionarySchools;
use frontend\models\Education;
use Yii;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

class EducationController extends \frontend\components\Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['create', 'update', 'delete', 'pub'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }


    public function actionCreate(){

        $this->layout = 'iframe';

        $oEducation = new Education();

        $oCity =[];

        $oCountry = Country::getTreeMap();

        $aCompany = DictionarySchools::getByAutocomlete();

        $aPosition = DictionaryFaculties::getByAutocomlete();

        if( $oEducation->load($_POST) && $oEducation->save() ){

            return $this->render('success');
        }

        if( !is_null( $oEducation->country_id ) ){
            $oCity = City::getMapByCountryId($oEducation->country_id);
        }

        return $this->render('form', compact('oEducation', 'oCity', 'oCountry', 'aCompany', 'aPosition'));

    }

    public function actionUpdate( $id ){

        $this->layout = 'iframe';

        $oEducation = $this->findModel($id);

        $oCity =[];

        $oCountry = Country::getTreeMap();

        $aCompany = DictionarySchools::getByAutocomlete();

        $aPosition = DictionaryFaculties::getByAutocomlete();

        if( $oEducation->load($_POST) && $oEducation->save() ){

            return $this->render('success');

        }

        if( !is_null( $oEducation->country_id ) ){
            $oCity = City::getMapByCountryId($oEducation->country_id);
        }

        return $this->render('form', compact('oEducation', 'oCity', 'oCountry', 'aCompany', 'aPosition'));

    }

    public function actionDelete($id){
        $this->findModel($id)->delete();
    }

    public function actionPub($id, $val){
        $oModel = $this->findModel($id);
        $oModel->is_public = (int) $val;
        $oModel->save();
        Yii::$app->end();
    }
    /**
     * Finds the DictionaryCompany model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return DictionaryCompany the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Education::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(\Yii::t('app', 'Запрошенная страница не существует.'));
        }
    }
}
