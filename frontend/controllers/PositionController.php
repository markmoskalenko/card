<?php

namespace frontend\controllers;

use common\models\DictionaryPosition;
use yii\filters\AccessControl;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class PositionController extends \frontend\components\Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['search'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionSearch($term){

        if( !\Yii::$app->request->isAjax )
            throw new NotFoundHttpException(\Yii::t('app', 'Страница не найдена.'));

        Yii::$app->response->format = Response::FORMAT_JSON;

        return DictionaryPosition::findByTerm($term);
    }
}
